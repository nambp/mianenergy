<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\InformationController;
use App\Http\Controllers\Admin\IntroduceController;
use App\Http\Controllers\Admin\ContactController;
use App\Http\Controllers\Admin\AgencyController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\GalleryController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\TechnicalController;
use App\Http\Controllers\Admin\RateController;
use App\Http\Controllers\Admin\ProjectController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\CkeditorController;
use App\Http\Controllers\Client\PageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes([
    'register' => false
]);

Route::get('/',[PageController::class, 'index'])->name('home');
Route::post('/search', [PageController::class, 'search'])->name('search');
Route::get('danh-muc/{slug}',[PageController::class, 'category'])->name('category');
Route::get('/san-pham',[PageController::class, 'product'])->name('product');
Route::get('/san-pham/{slug}',[PageController::class, 'productDetail'])->name('product.detail');
Route::post('/add-advisory',[PageController::class, 'addAdvisory'])->name('add.advisory');
Route::get('/gioi-thieu',[PageController::class, 'introduce'])->name('introduce');
Route::get('/tin-tuc',[PageController::class, 'news'])->name('news');
Route::get('/tin-tuc/{slug}',[PageController::class, 'newsDetail'])->name('news.detail');
Route::get('/du-an',[PageController::class, 'project'])->name('project');
Route::get('/du-an/{slug}',[PageController::class, 'projectDetail'])->name('project.detail');
Route::get('/lien-he',[PageController::class, 'contact'])->name('contact');
Route::post('/send-contact',[PageController::class, 'sendContact'])->name('send.contact');
Route::post('/send-rating',[PageController::class, 'sendRating'])->name('send.rate');
Route::post('filter-product',[PageController::class, 'filter'])->name('filter.product');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', [ProductController::class, 'index'])->name('dash');
    // Information
    Route::group(['prefix' => 'informations'], function () {
        Route::get('/', [InformationController::class, 'index'])->name('informations');
        Route::post('/update', [InformationController::class, 'update'])->name('informations.u');
    });
    // Introduce
    Route::group(['prefix' => 'introduces'], function () {
        Route::get('/', [IntroduceController::class, 'index'])->name('introduces');
        Route::post('/update', [IntroduceController::class, 'update'])->name('introduces.u');
    });
    // Contacts
    Route::group(['prefix' => 'contacts'], function () {
        Route::get('/', [ContactController::class, 'index'])->name('contacts');
        Route::get('/{id}/show', [ContactController::class, 'show'])->name('contacts.d');
        Route::delete('/destroy/{id}', [ContactController::class, 'destroy'])->name('contacts.r');
        Route::get('/status/{id}/{value}', [ContactController::class, 'changeStatus'])
                ->where('id', '[0-9]+')->where('value', '(1|2)')->name('contacts.st');
    });
    // Agencies
    Route::group(['prefix' =>'agencies'], function () {
        Route::get('/', [AgencyController::class, 'index'])->name('agencies');
        Route::get('/create', [AgencyController::class, 'create'])->name('agencies.c');
        Route::post('/store', [AgencyController::class, 'store'])->name('agencies.s');
        Route::get('/{id}/edit', [AgencyController::class, 'edit'])->name('agencies.e');
        Route::post('/update/{id}', [AgencyController::class, 'update'])->name('agencies.u');
        Route::delete('/destroy/{id}', [AgencyController::class, 'destroy'])->name('agencies.r');
    });
    // Categories
    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', [CategoryController::class, 'index'])->name('categories');
        Route::get('/create', [CategoryController::class, 'create'])->name('categories.c');
        Route::post('/store', [CategoryController::class, 'store'])->name('categories.s');
        Route::get('/{id}/edit', [CategoryController::class, 'edit'])->name('categories.e');
        Route::post('/update/{id}', [CategoryController::class, 'update'])->name('categories.u');
        Route::delete('/destroy/{id}', [CategoryController::class, 'destroy'])->name('categories.r');
        Route::get('/status/{id}/{value}', [CategoryController::class, 'changeStatus'])
                ->where('id', '[0-9]+')->where('value', '(1|2)')->name('categories.st');
        Route::get("/position/{id}", [CategoryController::class, 'changePosition'])
                ->name("categories.p");

        // Sub categories
        Route::get('/{id}/sub-categories', [CategoryController::class, 'list'])->name('sub-categories');
        Route::get('/{id}/sub-categories/create', [CategoryController::class, 'add'])->name('sub-categories.c');
        Route::post('/{id}/sub-categories/store', [CategoryController::class, 'saveCate'])->name('sub-categories.s');
        Route::get('/{id}/sub-categories/{cate}/edit', [CategoryController::class, 'change'])->name('sub-categories.e');
        Route::post('sub-categories/update/{id}', [CategoryController::class, 'saveChange'])->name('sub-categories.u');
        Route::delete('/{id}/sub-categories/destroy/{cate}', [CategoryController::class, 'delete'])->name('sub-categories.r');
        Route::get("/{id}/sub-categories/position/{cate}", [CategoryController::class, 'updatePosition'])
                ->name("sub-categories.p");
    });
    // Product
    Route::group(['prefix' => 'products'], function () {
        Route::get('/', [ProductController::class, 'index'])->name('products');
        Route::get('/show/{id}', [ProductController::class, 'show'])->name('products.d');
        Route::get('/create', [ProductController::class, 'create'])->name('products.c');
        Route::post('/store', [ProductController::class, 'store'])->name('products.s');
        Route::get('/{id}/edit', [ProductController::class, 'edit'])->name('products.e');
        Route::post('/update/{id}', [ProductController::class, 'update'])->name('products.u');
        Route::delete('/destroy/{id}', [ProductController::class, 'destroy'])->name('products.r');
        Route::get('/status/{id}/{value}', [ProductController::class, 'changeStatus'])
                ->where('id', '[0-9]+')->where('value', '(1|2)')->name('products.st');
        // Galleries
        Route::get('/{id}/galleries', [GalleryController::class, 'index'])->name('galleries');
        Route::post('/galleries/save', [GalleryController::class, 'save'])->name('galleries.s');
        Route::delete('/galleries/destroy/{id}', [GalleryController::class, 'destroy'])->name('galleries.r');
        // Technicals
        Route::get('/{id}/technicals', [TechnicalController::class, 'index'])->name('technicals');
        Route::post('/technicals/save', [TechnicalController::class, 'save'])->name('technicals.s');
        Route::delete('/technicals/destroy/{id}', [TechnicalController::class, 'destroy'])->name('technicals.r');
    });
    // Brands
    Route::group(['prefix' =>'brands'], function () {
        Route::get('/', [BrandController::class, 'index'])->name('brands');
        Route::get('/create', [BrandController::class, 'create'])->name('brands.c');
        Route::post('/store', [BrandController::class, 'store'])->name('brands.s');
        Route::get('/{id}/edit', [BrandController::class, 'edit'])->name('brands.e');
        Route::post('/update/{id}', [BrandController::class, 'update'])->name('brands.u');
        Route::delete('/destroy/{id}', [BrandController::class, 'destroy'])->name('brands.r');
    });
    // Rates
    Route::group(['prefix' => 'rates'], function () {
        Route::get('/', [RateController::class, 'index'])->name('rates');
        Route::get('/show/{id}', [RateController::class, 'show'])->name('rates.d');
        Route::delete('/destroy/{id}', [RateController::class, 'destroy'])->name('rates.r');
        Route::get('/status/{id}/{value}', [RateController::class, 'changeStatus'])
                ->where('id', '[0-9]+')->where('value', '(1|2)')->name('rates.st');
    });
    // Projects
    Route::group(['prefix' => 'projects'], function () {
        Route::get('/', [ProjectController::class, 'index'])->name('projects');
        Route::get('/create', [ProjectController::class, 'create'])->name('projects.c');
        Route::post('/store', [ProjectController::class, 'store'])->name('projects.s');
        Route::get('/{id}/edit', [ProjectController::class, 'edit'])->name('projects.e');
        Route::post('/update/{id}', [ProjectController::class, 'update'])->name('projects.u');
        Route::delete('/destroy/{id}', [ProjectController::class, 'destroy'])->name('projects.r');
        Route::get('/status/{id}/{value}', [ProjectController::class, 'changeStatus'])
                ->where('id', '[0-9]+')->where('value', '(1|2)')->name('projects.st');
    });
    // Blogs
    Route::group(['prefix' => 'blogs'], function () {
        Route::get('/', [BlogController::class, 'index'])->name('blogs');
        Route::get('/create', [BlogController::class, 'create'])->name('blogs.c');
        Route::post('/store', [BlogController::class, 'store'])->name('blogs.s');
        Route::get('/{id}/edit', [BlogController::class, 'edit'])->name('blogs.e');
        Route::post('/update/{id}', [BlogController::class, 'update'])->name('blogs.u');
        Route::delete('/destroy/{id}', [BlogController::class, 'destroy'])->name('blogs.r');
        Route::get('/status/{id}/{value}', [BlogController::class, 'changeStatus'])
                ->where('id', '[0-9]+')->where('value', '(1|2)')->name('blogs.st');
    });
    // SLiders
    Route::group(['prefix' => 'sliders'], function () {
        Route::get('/', [SliderController::class, 'index'])->name('sliders');
        Route::get('/create', [SliderController::class, 'create'])->name('sliders.c');
        Route::post('/store', [SliderController::class, 'store'])->name('sliders.s');
        Route::get('/{id}/edit', [SliderController::class, 'edit'])->name('sliders.e');
        Route::post('/update/{id}', [SliderController::class, 'update'])->name('sliders.u');
        Route::delete('/destroy/{id}', [SliderController::class, 'destroy'])->name('sliders.r');
        Route::get('/status/{id}/{value}', [SliderController::class, 'changeStatus'])
                ->where('id', '[0-9]+')->where('value', '(1|2)')->name('sliders.st');
        Route::get("/position/{id}", [SliderController::class, 'changePosition'])
                ->name("sliders.p");
    });
    // Orders
    Route::group(['prefix' => 'orders'], function () {
        Route::get('/', [OrderController::class, 'index'])->name('orders');
        Route::get('/show/{id}', [OrderController::class, 'show'])->name('orders.d');
        Route::delete('/destroy/{id}', [OrderController::class, 'destroy'])->name('orders.r');
        
    });

    Route::post('upload', [CkeditorController::class, 'uploadPhoto'])->name('upload.file');
});

Route::get('/thumbnail', 'Controller@thumbnail')->name('image.thumbnail');
Route::get('/sitemap', [PageController::class, 'sitemap'])->name('sitemap');

Route::get('/', [PageController::class, 'index'])->name('home');
Route::post('/add-advisory', [PageController::class, 'addAdvisory'])->name('add.advisory');
Route::get('/ve-chung-toi', [PageController::class, 'introduce'])->name('introduce');
Route::get('/tin-tuc', [PageController::class, 'news'])->name('news');
Route::get('/tin-tuc/{slug}', [PageController::class, 'newsDetail'])->name('news.detail');
Route::get('/du-an', [PageController::class, 'project'])->name('project');
Route::get('/du-an/{slug}', [PageController::class, 'projectDetail'])->name('project.detail');
Route::get('/lien-he', [PageController::class, 'contact'])->name('contact');
Route::post('/send-contact', [PageController::class, 'sendContact'])->name('send.contact');
Route::post('/send-rating', [PageController::class, 'sendRating'])->name('send.rate');
Route::get('/danh-muc/{slug}', [PageController::class, 'category'])->name('category');
Route::get('/san-pham', [PageController::class, 'product'])->name('product');
Route::get('{slug}', [PageController::class, 'productDetail'])->name('product.detail');

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
