<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
            $table->string('title', 255)->nullable();
            $table->string('slug', 255)->nullable();
            $table->string('summary', 300)->nullable();
            $table->string('image', 200)->nullable();
            $table->text('content')->nullable();
            $table->string('meta_title', 150)->nullable();
            $table->string('meta_keywords', 200)->nullable();
            $table->string('meta_description', 400)->nullable();
            $table->string('seo_image', 200)->nullable();
            $table->tinyInteger('status')->nullable()->comment('1 - active, 2 - block');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
