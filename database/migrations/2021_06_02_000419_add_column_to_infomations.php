<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToInfomations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('infomations', function (Blueprint $table) {
            $table->string('shopping_guide', 200)->nullable();
            $table->string('payment_guide', 200)->nullable();
            $table->string('purchase_forms', 200)->nullable();
            $table->string('road_map', 200)->nullable();
            $table->string('delivery_policy', 200)->nullable();
            $table->string('exchange_policy', 200)->nullable();
            $table->string('warranty_policy', 200)->nullable();
            $table->string('privacy_policy', 200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('infomations', function (Blueprint $table) {
            //
        });
    }
}
