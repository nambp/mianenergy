<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informations', function (Blueprint $table) {
            $table->id();
            $table->string('name', 150)->nullable();
            $table->string('logo', 150)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('slogan', 255)->nullable();
            $table->string('zalo', 255)->nullable();
            $table->string('facebook', 255)->nullable();
            $table->string('face_id', 100)->nullable();
            $table->text('fanpage')->nullable();
            $table->string('email', 150)->nullable();
            $table->text('map')->nullable();
            $table->string('meta_title', 150)->nullable();
            $table->string('meta_keywords', 200)->nullable();
            $table->string('meta_description', 400)->nullable();
            $table->string('seo_image', 200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informations');
    }
}
