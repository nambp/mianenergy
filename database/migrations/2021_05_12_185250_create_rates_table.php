<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rates', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->nullable();
            $table->integer('star')->nullable()->default(0);
            $table->string('message', 255)->nullable();
            $table->string('name', 150)->nullable();
            $table->string('email', 150)->nullable();
            $table->tinyInteger('status')->nullable()->comment('1 - active, 2 - block');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rates');
    }
}
