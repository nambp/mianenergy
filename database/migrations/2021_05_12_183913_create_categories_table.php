<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('parent_id')->nullable();
            $table->string('title', 255)->nullable();
            $table->string('slug', 255)->nullable();
            $table->string('short_title', 255)->nullable();
            $table->string('banner', 255)->nullable();
            $table->text('content')->nullable();
            $table->integer('position')->nullable()->default(0);
            $table->tinyInteger('status')->nullable()->comment('1 - active, 2 -block');
            $table->string('meta_title', 150)->nullable();
            $table->string('meta_keywords', 200)->nullable();
            $table->string('meta_description', 400)->nullable();
            $table->string('seo_image', 200)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
