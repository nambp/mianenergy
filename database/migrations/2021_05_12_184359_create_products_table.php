<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('category_id')->nullable();
            $table->bigInteger('brand_id')->nullable();
            $table->string('code', 200)->nullable();
            $table->string('title', 255)->nullable();
            $table->string('slug', 255)->nullable();
            $table->integer('price')->nullable()->default(0);
            $table->text('content')->nullable();
            $table->string('image', 200)->nullable();
            $table->string('meta_title', 150)->nullable();
            $table->string('meta_keywords', 200)->nullable();
            $table->string('meta_description', 400)->nullable();
            $table->string('seo_image', 200)->nullable();
            $table->tinyInteger('status')->nullable()->comment('1- active, 2 - block');
            $table->tinyInteger('spec')->nullable()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
