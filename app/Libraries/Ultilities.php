<?php
    namespace App\Libraries;

    use Carbon\Carbon;
    use File;
    use DateTime;
    use Twilio\Rest\Client;
    use Twilio\Jwt\AccessToken;
    use Twilio\Exceptions\RestException;
    use App\Models\Order;
    use App\Models\Category;
    use App\Models\Product;
    use App\Models\Blog;
    use App\Models\Brand;

    class Ultilities
    {
        // Get full url image
        public static function replaceUrlImage($val)
        {
            $image = '';
            if (!empty($val)) {
                if (!filter_var($val, FILTER_VALIDATE_URL)) {
                    $image = url('/uploads/'.$val);
                } else {
                    $image = $val;
                }
            }

            return $image;
        }

        // Clear XSS
        public static function clearXSS($string)
        {
            $string = nl2br($string);
            $string = trim(strip_tags($string));
            $string = self::removeScripts($string);

            return $string;
        }

        public static function removeScripts($str)
        {
            $regex =
                '/(<link[^>]+rel="[^"]*stylesheet"[^>]*>)|'.
                '<script[^>]*>.*?<\/script>|'.
                '<style[^>]*>.*?<\/style>|'.
                '<!--.*?-->/is';

            return preg_replace($regex, '', $str);
        }

        public static function uploadFile($file)
        {
            $publicPath = public_path('uploads');
            if (!File::exists($publicPath)) {
                File::makeDirectory($publicPath, 0775, true, true);
            }
            $name = time().'-'.$file->getClientOriginalName();
            $name = preg_replace('/\s+/', '', $name);
            $file->move(public_path('uploads'), $name);
            return '/uploads/'.$name;
        }

        public static function getCodeOrder()
        {
            $check = false;
            $order = new Order();
            $str = null;
            do {
                $str = 'ZEN';
                $str .= rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9);
                $orderCheck = $order->where('code', $str)->get()->first();
                if (empty($orderCheck)) {
                    $check = true;
                }
            } while ($check = false);
            return $str;
        }

        public static function getCodeOrderByID($order_id)
        {
            //mẫu ZEN000000001
            $str = 'ZEN';
            $lenghtId = strlen($order_id);
            switch($lenghtId){
                case 1: $str .= '00000000'.$order_id; break;
                case 2: $str .= '0000000'.$order_id; break;
                case 3: $str .= '000000'.$order_id; break;
                case 4: $str .= '00000'.$order_id; break;
                case 5: $str .= '0000'.$order_id; break;
                case 6: $str .= '000'.$order_id; break;
                case 7: $str .= '00'.$order_id; break;
                case 8: $str .= '0'.$order_id; break;
                case 9: $str .= $order_id; break;
                default: $str = self::getCodeOrder(); break;
            }
            return $str;
        }

        public static  function getListCate()
        {
            $list = Category::with(['children','products','parent'])->get();
            return $list;
        }

        public static  function productNew($key = 5)
        {
            $list = Product::orderBy('id','desc')->take($key)->get();
            return $list;
        }

        public static  function News()
        {
            $list = Blog::orderBy('id','desc')->take(5)->get();
            return $list;
        }

        public static  function brands()
        {
            $list = Brand::orderBy('id','desc')->get();
            return $list;
        }

        public static  function productCategory($arr)
        {
            $list = Product::whereIn('category_id',$arr)->where('spec',2)->orderBy('id','desc')->take(8)->get();
            return $list;
        }
        public static  function productAll($arr)
        {
            $list = Product::whereIn('category_id',$arr)->orderBy('id','desc')->paginate(20);
            return $list;
        }
        public static  function productAlls($arr)
        {
            $list = Product::where('category_id',$arr)->orderBy('id','desc')->paginate(20);
            return $list;
        }
    }