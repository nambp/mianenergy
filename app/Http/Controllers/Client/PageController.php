<?php
namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\Order;
use App\Models\Rate;
use App\Models\Category;
use App\Models\Introduce;
use App\Models\Blog;
use App\Models\Project;
use App\Models\Product;
use App\Models\Slider;
use Validator;
use DB;
use Carbon\Carbon;

class PageController extends Controller
{
    /**
     * home page
     */
    public function index(Category $category ,Slider $slider,Product $product)
    {   $slider = $slider->getList();
        $categories = $category->getList();
        $products = $product->where('status',Product::ACTIVE)->orderBy('id','desc')->take(12)->get();
        return view('templates.index', ['categories'=> $categories,'slider'=>$slider,'productList'=>$products]);
    }

    /**
     * search
     */
    public function search(Request $request)
    {
        $products = Product::where('title','LIKE','%'.$request->search.'%')->orWhere('code','LIKE','%'.$request->search.'%')->get();
        return view('templates.search',['products'=>$products,'search'=>$request->search]);
        
    }

    /**
     * category page
     */
    public function category($slug, Category $category)
    {
        $category = $category->getCateBySlug($slug);
        if($category){
            return view('templates.category',['category'=>$category,'slug'=>$slug]);
        }else{
            return redirect()->route('product');
        }
    }

    /**
     * product
     */
    public function product(Product $product)
    {
        $lists = $product->getList();
        return view('templates.product',['products'=>$lists]);
    }

    /**
     * product detail
     */

    public function productDetail($slug, Product $product)
    {

        $detail = $product->getDetail($slug);
        if($detail){
            $categories = $detail->category;
            $listProduct = $product->where('category_id',$categories->id)->orderBy('id','desc')->take(8)->get();
            return view('templates.product_detail',['detail'=>$detail,'products'=>$listProduct]);
        }else{
            return \redirect()->route('product');
        }
    }

    /**
     * add advisory
     */
    public function addAdvisory(Request $request, Order $order)
    {
        $request->validate([
            'name'=>'required|max:100',
            'email'=>'required|email|max:100',
            'phone'=>'required|numeric|min:10',
            'quantity'=>'nullable|numeric|max:1000000',
            'message'=>'nullable|max:300',
        ]);

        try {
            $create = $order->saveAdvisory($request);
            return back()
                    ->with(['alert-type' => 'success', 'message' => 'Thêm tư vấn thành công']);
        } catch (\Exception $e) {
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }

    /**
     * introduce
     */
    public function introduce(Introduce $introduce)
    {
        $introduce = $introduce->first();
        return view('templates.introduce', ['introduce'=>$introduce]);
    }
    
    /**
     * news
     */
    public function news(Blog $blog)
    {
        $list = $blog->getListBlog();
        return view('templates.news', ['blogs'=>$list]);
    }
    
    /**
     * news detail
     */
    public function newsDetail($slug, Blog $blog)
    {
        $detail = $blog->where('slug',$slug)->first();
        return view('templates.news_detail', ['detail'=>$detail]);
    }

    /**
     * project
     */
    public function project(Project $project)
    {
        $project = $project->getListProject();
        return view('templates.project', ['project'=>$project]);
    }

    /**
     * project
     */
    public function projectDetail($slug , Project $project)
    {
        $detail = $project->where('slug', $slug)->first();
        return view('templates.project_detail', ['detail'=>$detail]);
    }

    /**
     * contact
     */
    public function contact()
    {
        return view('templates.contact');
    }

    /**
     * send contact
     */
    public function sendContact(Request $request, Contact $contact)
    {
        $request->validate($contact->rule());
        try {
            $contact = $contact->addContact($request);
            return redirect()->route('contact')
                    ->with(['alert-type' => 'success', 'message' => 'Thêm phản hồi thành công']);
        } catch (\Exception $e) {
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }

    public function sendRating(Request $request, Rate $rate)
    {
        $value = session()->put('rating', 'fail');
        $data = $request->validate([
            'rate'=> 'required',
            'message'=>'required|max:255',
            'name'=> 'required|max:150',
            'email'=> 'required|email|max:150',
        ]);

        try {
            $contact = $rate->saveRate($request);
            $value = session()->forget('rating');
            return back()
                    ->with(['alert-type' => 'success', 'message' => 'Thêm đánh giá thành công']);
        } catch (\Exception $e) {
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }

    public function filter(Request $request ,Product $product)
    {
        $lists = $product->filter($request);
        return view('elements.frontend.product',['products'=>$lists]);
    }

    public function sitemap()
    {
        // $sitemap = app()->make('sitemap');

        // // Cache kết quả 60 phút
        // $sitemap->setCache('bds.sitemap.index', 60);
        // if (!$sitemap->isCached()) {
        //     $sitemap->add(url('/'), Carbon::now(), 1, 'daily');
        //     $sitemap->add(url('/ve-chung-toi'), Carbon::now(), 1, 'daily');
        //     $sitemap->add(url('/lien-he'), Carbon::now(), 1, 'daily');
        //     $sitemap->add(url('/tin-tuc'), Carbon::now(), 1, 'daily');
        //     $sitemap->add(url('/tin-tuc/tim-kiem'), Carbon::now(), 1, 'daily');
        //     $sitemap->add(url('/do-noi-that'), Carbon::now(), 1, 'daily');
        //     // add bài viết
        //     $posts = \DB::table('articles')->orderBy('created_at', 'desc')->get();
        //     foreach ($posts as $post) {
        //         //$sitemap->add(url, thời gian, độ ưu tiên, thời gian quay lại)
        //         $sitemap->add(route('blog.detail', $post->slug), $post->updated_at, 1, 'daily');
        //     }
        //     // add bài viết
        //     $categories = \DB::table('categories')->orderBy('created_at', 'desc')->get();
        //     foreach ($categories as $cate) {
        //         //$sitemap->add(url, thời gian, độ ưu tiên, thời gian quay lại)
        //         $sitemap->add(route('categories', $cate->slug), $cate->updated_at, 1, 'daily');
        //     }
        //     // add bài viết
        //     $products = \DB::table('products')->orderBy('created_at', 'desc')->get();
        //     foreach ($products as $prod) {
        //         //$sitemap->add(url, thời gian, độ ưu tiên, thời gian quay lại)
        //         $sitemap->add(route('products', $prod->slug), $prod->updated_at, 1, 'daily');
        //     }
        //     // add bài viết
        //     $services = \DB::table('services')->orderBy('created_at', 'desc')->get();
        //     foreach ($services as $ser) {
        //         //$sitemap->add(url, thời gian, độ ưu tiên, thời gian quay lại)
        //         $sitemap->add(route('services', $ser->slug), $ser->updated_at, 1, 'daily');
        //     }
        //     // add bài viết
        //     $lands = \DB::table('lands')->orderBy('created_at', 'desc')->get();
        //     foreach ($lands as $land) {
        //         //$sitemap->add(url, thời gian, độ ưu tiên, thời gian quay lại)
        //         $sitemap->add(route('project', $land->slug), $land->updated_at, 1, 'daily');
        //     }

        //     $lands = Land::with(['services', 'districts'])->orderBy('created_at', 'desc')->get();
        //     foreach ($lands as $land) {
        //         if (!empty($land->services) && !empty($land->districts)) {
        //             //$sitemap->add(url, thời gian, độ ưu tiên, thời gian quay lại)
        //             $sitemap->add(route('service.list', [$land->districts->slug, $land->services->slug]), $land->updated_at, 1, 'daily');
        //         }
        //     }
        // }
        // // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
        // return $sitemap->render('xml');
    }
}
