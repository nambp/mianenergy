<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Technical;
use App\Libraries\Ultilities;
use Str;
use Carbon\Carbon;

class TechnicalController extends Controller
{
    public function save(Request $request, Technical $technical)
    {
        // dd($request->all());
        $params = [];
        $technical->where('product_id', $request->prod_id)->delete();
        for ($i = 0; $i < count($request->title); $i++) {
            $params[] = [
                'product_id' => $request->prod_id,
                'title' => Ultilities::clearXSS($request->title[$i]),
                'content' => Ultilities::clearXSS($request->content[$i]),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        }
        $technical->insert($params);
        // dd($params);
        return back()->with(['alert-type' => 'success', 'message' => __('Cập nhật thông số thành công')]);
    }
}
