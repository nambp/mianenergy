<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Rate;
use App\Models\Product;
use DataTables;

class RateController extends Controller
{
    public function index(Request $request, Rate $rate, Product $product)
    {        
        if ($request->ajax()) {
            $data = $rate->with(['products'])->orderBy('id', 'desc');
            // dd($data);
            if (!empty($request->get('search'))) {
                $keyword = $request->get('search');
                $data->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('email', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('star', $keyword);
            }
            if (!empty($request->get('product_id')) || $request->get('product_id') !="") {
                $data->where('product_id', $request->get('product_id'));
            }
            if (!empty($request->get('status')) || $request->get('status') !="") {
                $data->where('status', $request->get('status'));
            }
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status', function ($data) {
                        if ($data->status == 1) {
                            $status = 'Đang mở';
                        } else {
                            $status = 'Đang khóa';
                        }
                        return $status;
                    })
                    ->addColumn('status', function ($data) {
                        return view('elements.backend.status', [
                            'model' => $data,
                            'url' => route('rates.st', ['id' => $data->id, 'value' => $data->status])
                        ]);
                    })
                    ->addColumn('product', function ($data) {
                        $title = '';
                        if (!empty($data->products)) {
                            $title = '<h6 class="mb-1 font-weight-normal">'.$data->products->title.'</h6>'
                                    .'<p class="mb-0">'.$data->products->code.'</p>';
                        }
                        return  $title;
                    })
                    ->editColumn('star', function ($data) {
                        return '<strong>'.$data->star.'</strong>';
                    })
                    ->addColumn('action', function ($data) {
                        return view('elements.backend.action', [
                            'model' => $data,
                            'url_show' => route('rates.d', $data->id),
                            'url_edit' => null,
                            'url_destroy' => route('rates.r', $data->id)
                        ]);
                    })
                    ->rawColumns(['action', 'status', 'product', 'star'])
                    ->make(true);
        }
        return view('backend.rates.index', [
            'products' => $product->all()
        ]);
    }

    public function destroy(Rate $rate, $id)
    {
        $delete  = $rate->findOrFail($id)->delete();
        return 0 ;
    }

    public function show(Rate $rate, $id)
    {
        return view('backend.rates.show', [
            'detail' => $rate->with(['products'])->findOrFail($id)
        ]);
    }

    public function changeStatus(Request $request, Rate $rate, $id, $value)
    {
        $id = $request->id;
        $value = $request->value;
        $update = $rate->changeStatus($id, $value);
        
        if ($update) {
            return 1;
        }

        return 0;
    }
}
