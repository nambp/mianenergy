<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libraries\Ultilities;
use DataTables;
use Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use App\Models\Blog;

class BlogController extends Controller
{
    public function index(Request $request, Blog $blog)
    {
        if ($request->ajax()) {
            $data = $blog->latest();
            if (!empty($request->get('search'))) {
                $keyword = $request->get('search');
                $data->where('title', 'LIKE', '%'.$keyword.'%');
            }
            
            if (!empty($request->get('status')) || $request->get('status') !="") {
                $data->where('status', '=', $request->get('status'));
            }
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status', function ($data) {
                        return view('elements.backend.status', [
                            'model' => $data,
                            'url' => route('blogs.st', ['id' => $data->id, 'value' => $data->status])
                        ]);
                    })
                    ->addColumn('action', function ($data) {
                        return view('elements.backend.action', [
                            'model' => $data,
                            'url_show' => null,
                            'url_edit' => route('blogs.e', $data->id),
                            'url_destroy' => route('blogs.r', $data->id)
                        ]);
                    })
                    ->rawColumns(['action', 'status', 'position'])
                    ->make(true);
        }
        return view('backend.blogs.index');
    }

    public function create()
    {
        return view('backend.blogs.add');
    }

    public function store(Request $request, Blog $blog)
    {
        $request->validate([
            'title' => 'required|string|max:150',
            'summary' => 'required|string|max:300',
            'slug' => 'required|string|max:150|unique:blogs,slug',
            'meta_title' => 'required|string|max:150',
            'meta_keywords' => 'required|string|max:150',
            'meta_description' => 'required|string|max:400',
            'image_seo' => 'required|mimes:jpeg,jpg,png|max:10000',
            'banner' => 'required|mimes:jpeg,jpg,png|max:10000',
        ]);
        try {
            $params['title'] = Ultilities::clearXSS($request->title);
            $params['slug'] = Str::slug(Ultilities::clearXSS($request->slug));
            $params['summary'] = Ultilities::clearXSS($request->summary);
            $params['meta_title'] = Ultilities::clearXSS($request->meta_title);
            $params['meta_keywords'] = Ultilities::clearXSS($request->meta_keywords);
            $params['meta_description'] = Ultilities::clearXSS($request->meta_description);
            $params['status'] = Ultilities::clearXSS($request->status);
            $params['content'] = $request->content;

            $slug = $params['slug'];
            $str = date('dmHis', strtotime(now()));

            if ($request->hasFile('image_seo')) {
                $image_seo = $request->file('image_seo');
                $params['seo_image'] = $slug.'-'.$str.'.'.$image_seo->getClientOriginalExtension();
            }

            if ($request->hasFile('banner')) {
                $banner = $request->file('banner');
                $params['image'] = $slug.'-'.($str + 1).'.'.$banner->getClientOriginalExtension();
            }

            $action = $blog->create($params);

            if ($action) {
                if ($request->hasFile('image_seo')) {
                    $image_seo = $request->file('image_seo');
                    $path = public_path('uploads/blogs');
                    $image_seo->move($path, $params['seo_image']);
                }
                if ($request->hasFile('banner')) {
                    $banner = $request->file('banner');
                    $path = public_path('uploads/blogs');
                    $banner->move($path, $params['image']);
                }
                return redirect()->route('blogs')
                    ->with(['alert-type' => 'success', 'message' => 'Thêm mới thành công']);
            }
        } catch (\Exception $e) {
            // dd($e);
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }
    
    public function edit(Blog $blog, $id)
    {
        return view('backend.blogs.edit', [
            'detail' => $blog->findOrFail($id)
        ]);
    }

    public function update(Request $request, Blog $blog, $id)
    {
        $request->validate([
            'title' => 'required|string|max:150',
            'summary' => 'required|string|max:300',
            'slug' => 'required|string|max:150|unique:blogs,slug,'.$id,
            'meta_title' => 'required|string|max:150',
            'meta_keywords' => 'required|string|max:150',
            'meta_description' => 'required|string|max:400',
            'image_seo' => 'nullable|mimes:jpeg,jpg,png|max:10000',
            'banner' => 'nullable|mimes:jpeg,jpg,png|max:10000',
        ]);
        
        try {
            $model = $blog->findOrFail($id);
            $oldImage = $model->image;
            $oldSeo = $model->seo_image;
            if (empty($model)) {
                return back()
                    ->with(['alert-type' => 'error', 'message' => 'Không tìm thấy dữ liệu']);
            }

            $params['title'] = Ultilities::clearXSS($request->title);
            $params['summary'] = Ultilities::clearXSS($request->summary);
            $params['slug'] = Str::slug(Ultilities::clearXSS($request->slug));
            $params['meta_title'] = Ultilities::clearXSS($request->meta_title);
            $params['meta_keywords'] = Ultilities::clearXSS($request->meta_keywords);
            $params['meta_description'] = Ultilities::clearXSS($request->meta_description);
            $params['status'] = Ultilities::clearXSS($request->status);
            $params['content'] = $request->content;

            $slug = $params['slug'];
            $str = date('dmHis', strtotime(now()));
            
            if ($request->hasFile('image_seo')) {
                $image_seo = $request->file('image_seo');
                $params['seo_image'] = $slug.'-'.$str.'.'.$image_seo->getClientOriginalExtension();
            }
            if ($request->hasFile('banner')) {
                $banner = $request->file('banner');
                $params['image'] = $slug.'-'.($str + 1).'.'.$banner->getClientOriginalExtension();
            }

            $action = $model->update($params);

            if ($action) {
                if ($request->hasFile('image_seo')) {
                    if ($oldSeo != "") {
                        $path = public_path() . '/uploads/blogs/'. $oldSeo;
                        if (file_exists($path)) {
                            File::delete($path);
                        }
                    }
                    $image_seo = $request->file('image_seo');
                    $path = public_path('uploads/blogs');
                    $image_seo->move($path, $params['seo_image']);
                }
                if ($request->hasFile('banner')) {
                    if ($oldImage != "") {
                        $path = public_path() . '/uploads/blogs/'. $oldImage;
                        if (file_exists($path)) {
                            File::delete($path);
                        }
                    }
                    $banner = $request->file('banner');
                    $path = public_path('uploads/blogs');
                    $banner->move($path, $params['image']);
                }
                return redirect()->route('blogs')
                    ->with(['alert-type' => 'success', 'message' => 'Cập nhật thành công']);
            }
        } catch (\Exception $e) {
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }

    public function destroy(Blog $blog, $id)
    {
        $model = $blog->findOrFail($id);
    
        if ($model->seo_image != "") {
            $path = public_path() . '/uploads/blogs/'. $model->seo_image;
            if (file_exists($path)) {
                File::delete($path);
            }
        }
        if ($model->image != "") {
            $path = public_path() . '/uploads/blogs/'. $model->image;
            if (file_exists($path)) {
                File::delete($path);
            }
        }

        $model->delete();
        return 0;
    }

    public function changeStatus(Request $request, Blog $blog, $id, $value)
    {
        $id = $request->id;
        $value = $request->value;
        $update = $blog->changeStatus($id, $value);
        
        if ($update) {
            return 1;
        }

        return 0;
    }
}
