<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Introduce;
use App\Libraries\Ultilities;
use Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;

class IntroduceController extends Controller
{
    public function index(Introduce $introduce)
    {
        return view('backend.introduces.index', [
            'detail' => $introduce->first()
        ]);
    }

    public function update(Request $request, Introduce $introduce)
    {
        $request->validate([
            'content' => "required|string",
            'meta_title' => 'required|string|max:150',
            'meta_keywords' => 'required|string|max:150',
            'meta_description' => 'required|string|max:400',
            'image_seo' => 'nullable|mimes:jpeg,jpg,png|max:10000',
        ]);
        try {
            $model = $introduce->first();

            $params['content'] = $request->content;
            $params['meta_title'] = Ultilities::clearXSS($request->meta_title);
            $params['meta_keywords'] = Ultilities::clearXSS($request->meta_keywords);
            $params['meta_description'] = Ultilities::clearXSS($request->meta_description);
            
            $str = date('dmHis', strtotime(now()));
            
            if ($request->hasFile('image_seo')) {
                $image_seo = $request->file('image_seo');
                $params['seo_image'] = $str.'-'.$image_seo->getClientOriginalName();
            }

            if (empty($model)) {
                $action = $introduce->create($params);
            } else {
                $action = $model->update($params);
            }

            if ($action) {
                if ($request->hasFile('image_seo')) {
                    $image_seo = $request->file('image_seo');
                    $path = public_path('uploads');
                    $image_seo->move($path, $params['seo_image']);
                }
                
                return back()
                    ->with(['alert-type' => 'success', 'message' => 'Cập nhật thành công']);
            }
        } catch (\Exception $e) {
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }
}
