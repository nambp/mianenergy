<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;
use DataTables;

class ContactController extends Controller
{
    public function index(Request $request, Contact $contact)
    {
        if ($request->ajax()) {
            $data = $contact->orderBy('id','desc');
            // dd($data);
            if (!empty($request->get('search'))) {
                $keyword = $request->get('search');
                $data->where('name', 'LIKE', '%'.$keyword.'%')->orWhere('phone','LIKE','%'.$keyword.'%')->orWhere('email','LIKE','%'.$keyword.'%');
            }
            if (!empty($request->get('status')) || $request->get('status') !="") {
                $data->where('status', '=', $request->get('status'));
            }
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status', function ($data) {
                        if($data->status == 1){
                            $status = 'Mới';
                        }else{
                            $status = 'Đã xem';
                        }
                        return $status;
                    })
                    
                    ->addColumn('action', function ($data) {
                        return view('elements.backend.action', [
                            'model' => $data,
                            'url_show' => route('contacts.d', $data->id),
                            'url_edit' => null,
                            'url_destroy' => route('contacts.r', $data->id)
                        ]);
                    })
                    ->rawColumns(['action', 'status'])
                    ->make(true);
        }
        return view('backend.contacts.index');
    }

    public function changeStatus(Request $request, Contact $contact, $id, $value)
    {
        $id = $request->id;
        $value = $request->value;
        $update = $contact->changeStatus($id, $value);
        
        if ($update) {
            return 1;
        }

        return 0;
    }

    public function destroy(Contact $contact , $id)
    {
        $delete  = $contact->findOrFail($id)->delete();
        return 0 ;
    }

    public function show(Contact $contact ,$id)
    {
        $update = $contact->changeStatus($id, Contact::BLOCK);
        $contact = $contact->findOrFail($id);
        return view('backend.contacts.show',['contact'=>$contact]);
    }
}
