<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libraries\Ultilities;
use DataTables;
use Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use App\Models\Brand;

class BrandController extends Controller
{
    public function index(Request $request, Brand $brand)
    {
        if ($request->ajax()) {
            $data = $brand->orderBy('id', 'desc');
            if (!empty($request->get('search'))) {
                $keyword = $request->get('search');
                $data->where('title', 'LIKE', '%'.$keyword.'%');
            }
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->editColumn('logo', function ($data) {
                        return '<img src="'.asset('uploads/brands/'.$data->logo).'" width="80">';
                    })
                    ->addColumn('action', function ($data) {
                        return view('elements.backend.action', [
                            'model' => $data,
                            'url_show' => null,
                            'url_edit' => route('brands.e', $data->id),
                            'url_destroy' => route('brands.r', $data->id)
                        ]);
                    })
                    ->rawColumns(['action', 'logo'])
                    ->make(true);
        }
        return view('backend.brands.index');
    }

    public function create()
    {
        return view('backend.brands.add');
    }

    public function store(Request $request, Brand $brand)
    {
        $request->validate([
            'title' => 'required|string|max:150',
            'logo' => 'required|mimes:jpeg,jpg,png|max:10000',
        ]);
        try {
            $params['title'] = Ultilities::clearXSS($request->title);

            $slug = Str::slug($params['title']);
            $str = date('dmHis', strtotime(now()));

            if ($request->hasFile('logo')) {
                $logo = $request->file('logo');
                $params['logo'] = $slug.'-'.($str + 1).'.'.$logo->getClientOriginalExtension();
            }

            $action = $brand->create($params);

            if ($action) {
                if ($request->hasFile('logo')) {
                    $logo = $request->file('logo');
                    $path = public_path('uploads/brands');
                    $logo->move($path, $params['logo']);
                }
                return redirect()->route('brands')
                    ->with(['alert-type' => 'success', 'message' => 'Thêm mới thành công']);
            }
        } catch (\Exception $e) {
            // dd($e);
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }
    
    public function edit(Brand $brand, $id)
    {
        return view('backend.brands.edit', [
            'detail' => $brand->findOrFail($id)
        ]);
    }

    public function update(Request $request, Brand $brand, $id)
    {
        $request->validate([
            'title' => 'required|string|max:150',
            'logo' => 'nullable|mimes:jpeg,jpg,png|max:10000',
        ]);
        
        try {
            $model = $brand->findOrFail($id);
            if (empty($model)) {
                return back()
                    ->with(['alert-type' => 'error', 'message' => 'Không tìm thấy dữ liệu']);
            }

            $params['title'] = Ultilities::clearXSS($request->title);

            $slug = Str::slug($params['title']);
            $str = date('dmHis', strtotime(now()));
            
            if ($request->hasFile('logo')) {
                $oldImage = $model->logo;
                $logo = $request->file('logo');
                $params['logo'] = $slug.'-'.($str + 1).'.'.$logo->getClientOriginalExtension();
            }

            $action = $model->update($params);

            if ($action) {
                if ($request->hasFile('logo')) {
                    if ($oldImage != "") {
                        $path = public_path() . '/uploads/brands/'. $oldImage;
                        if (file_exists($path)) {
                            File::delete($path);
                        }
                    }
                    $logo = $request->file('logo');
                    $path = public_path('uploads/brands');
                    $logo->move($path, $params['logo']);
                }
                return redirect()->route('brands')
                    ->with(['alert-type' => 'success', 'message' => 'Cập nhật thành công']);
            }
        } catch (\Exception $e) {
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }

    public function destroy(Brand $brand, $id)
    {
        $model = $brand->with(['products'])->findOrFail($id);

        if ((!empty($model->products) && count($model->products) > 0)) {
            return 1;
        } else {
            if ($model->logo != "") {
                $path = public_path() . '/uploads/brands/'. $model->logo;
                if (file_exists($path)) {
                    File::delete($path);
                }
            }
    
            $model->delete();
            return 0;
        }
    }
}
