<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Information;
use App\Libraries\Ultilities;
use Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;

class InformationController extends Controller
{
    public function index(Information $information)
    {
        return view('backend.informations.index', [
            'detail' => $information->first()
        ]);
    }

    public function update(Request $request, Information $information)
    {
        $request->validate([
            'name' => "required|string|max:150",
            'email' => "nullable|string|email|max:150",
            'slogan' => "nullable|string|max:255",
            'phone' => "nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:20",
            'zalo' => "nullable|max:255",
            'facebook' => "nullable|string|url|max:255",
            'meta_title' => 'required|string|max:150',
            'meta_keywords' => 'required|string|max:150',
            'meta_description' => 'required|string|max:400',
            'image_seo' => 'nullable|mimes:jpeg,jpg,png|max:10000',
            'shopping_guide' => 'nullable|max:10000',
            'payment_guide' => 'nullable|max:10000',
            'purchase_forms' => 'nullable|max:10000',
            'road_map' => 'nullable|max:10000',
            'delivery_policy' => 'nullable|max:10000',
            'exchange_policy' => 'nullable|max:10000',
            'warranty_policy' => 'nullable|max:10000',
            'recruitment' => 'nullable|max:10000',
            'privacy_policy' => 'nullable|max:10000',
        ]);
        try {
            $model = $information->first();

            $params['name'] = Ultilities::clearXSS($request->name);
            $params['phone'] = Ultilities::clearXSS($request->phone);
            $params['email'] = Ultilities::clearXSS($request->email);
            $params['zalo'] = Ultilities::clearXSS($request->zalo);
            $params['slogan'] = Ultilities::clearXSS($request->slogan);
            $params['facebook'] = Ultilities::clearXSS($request->facebook);
            $params['face_id'] = Ultilities::clearXSS($request->face_id);
            $params['fanpage'] = $request->fanpage;
            $params['map'] = $request->map;
            $params['meta_title'] = Ultilities::clearXSS($request->meta_title);
            $params['meta_keywords'] = Ultilities::clearXSS($request->meta_keywords);
            $params['meta_description'] = Ultilities::clearXSS($request->meta_description);
            
            $str = date('dmHis', strtotime(now()));
            
            if ($request->hasFile('image_seo')) {
                $image_seo = $request->file('image_seo');
                $params['seo_image'] = $str.'-'.$image_seo->getClientOriginalName();
            }
            if ($request->hasFile('shopping_guide')) {
                $image_seo = $request->file('shopping_guide');
                $params['shopping_guide'] = $str.'-'.$image_seo->getClientOriginalName();
            }
            if ($request->hasFile('payment_guide')) {
                $image_seo = $request->file('payment_guide');
                $params['payment_guide'] = $str.'-'.$image_seo->getClientOriginalName();
            }
            if ($request->hasFile('purchase_forms')) {
                $image_seo = $request->file('purchase_forms');
                $params['purchase_forms'] = $str.'-'.$image_seo->getClientOriginalName();
            }
            if ($request->hasFile('road_map')) {
                $image_seo = $request->file('road_map');
                $params['road_map'] = $str.'-'.$image_seo->getClientOriginalName();
            }
            if ($request->hasFile('delivery_policy')) {
                $image_seo = $request->file('delivery_policy');
                $params['delivery_policy'] = $str.'-'.$image_seo->getClientOriginalName();
            }
            if ($request->hasFile('exchange_policy')) {
                $image_seo = $request->file('exchange_policy');
                $params['exchange_policy'] = $str.'-'.$image_seo->getClientOriginalName();
            }
            if ($request->hasFile('warranty_policy')) {
                $image_seo = $request->file('warranty_policy');
                $params['warranty_policy'] = $str.'-'.$image_seo->getClientOriginalName();
            }
            if ($request->hasFile('privacy_policy')) {
                $image_seo = $request->file('privacy_policy');
                $params['privacy_policy'] = $str.'-'.$image_seo->getClientOriginalName();
            }
            if ($request->hasFile('recruitment')) {
                $image_seo = $request->file('recruitment');
                $params['recruitment'] = $str.'-'.$image_seo->getClientOriginalName();
            }

            if ($request->hasFile('logo')) {
                $logo = $request->file('logo');
                $params['logo'] = ($str+1).'-'.$logo->getClientOriginalName();
            }

            if (empty($model)) {
                $action = $information->create($params);
            } else {
                $action = $model->update($params);
            }

            if ($action) {
                if ($request->hasFile('image_seo')) {
                    $image_seo = $request->file('image_seo');
                    $path = public_path('uploads');
                    $image_seo->move($path, $params['seo_image']);
                }
                if ($request->hasFile('recruitment')) {
                    $recruitment = $request->file('recruitment');
                    $path = public_path('uploads');
                    $recruitment->move($path, $params['recruitment']);
                }
                if ($request->hasFile('shopping_guide')) {
                    $shopping_guide = $request->file('shopping_guide');
                    $path = public_path('uploads');
                    $shopping_guide->move($path, $params['shopping_guide']);
                }
                if ($request->hasFile('payment_guide')) {
                    $payment_guide = $request->file('payment_guide');
                    $path = public_path('uploads');
                    $payment_guide->move($path, $params['payment_guide']);
                }
                if ($request->hasFile('purchase_forms')) {
                    $purchase_forms = $request->file('purchase_forms');
                    $path = public_path('uploads');
                    $purchase_forms->move($path, $params['purchase_forms']);
                }
                if ($request->hasFile('road_map')) {
                    $road_map = $request->file('road_map');
                    $path = public_path('uploads');
                    $road_map->move($path, $params['road_map']);
                }
                if ($request->hasFile('delivery_policy')) {
                    $delivery_policy = $request->file('delivery_policy');
                    $path = public_path('uploads');
                    $delivery_policy->move($path, $params['delivery_policy']);
                }
                if ($request->hasFile('exchange_policy')) {
                    $exchange_policy = $request->file('exchange_policy');
                    $path = public_path('uploads');
                    $exchange_policy->move($path, $params['exchange_policy']);
                }
                if ($request->hasFile('warranty_policy')) {
                    $warranty_policy = $request->file('warranty_policy');
                    $path = public_path('uploads');
                    $warranty_policy->move($path, $params['warranty_policy']);
                }
                if ($request->hasFile('privacy_policy')) {
                    $privacy_policy = $request->file('privacy_policy');
                    $path = public_path('uploads');
                    $privacy_policy->move($path, $params['privacy_policy']);
                }
                if ($request->hasFile('logo')) {
                    $logo = $request->file('logo');
                    $path = public_path('uploads');
                    $logo->move($path, $params['logo']);
                }
                
                return back()
                    ->with(['alert-type' => 'success', 'message' => 'Cập nhật thành công']);
            }
        } catch (\Exception $e) {
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }
}
