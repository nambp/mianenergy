<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libraries\Ultilities;
use DataTables;
use Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use App\Models\Slider;

class SliderController extends Controller
{
    public function index(Request $request, Slider $slider)
    {
        if ($request->ajax()) {
            $data = $slider->orderBy('position', 'asc');
            if (!empty($request->get('status')) || $request->get('status') !="") {
                $data->where('status', $request->get('status'));
            }
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->editColumn('image', function ($data) {
                        return '<img src="'.asset('uploads/sliders/'.$data->image).'" width="120">';
                    })
                    ->addColumn('status', function ($data) {
                        return view('elements.backend.status', [
                            'model' => $data,
                            'url' => route('sliders.st', ['id' => $data->id, 'value' => $data->status])
                        ]);
                    })
                    ->editColumn('position', function ($data) {
                        return view('elements.backend.position', [
                            'model' => $data,
                            'url' => route('sliders.p', ['id' => $data->id]),
                        ]);
                    })
                    ->addColumn('action', function ($data) {
                        return view('elements.backend.action', [
                            'model' => $data,
                            'url_show' => null,
                            'url_edit' => route('sliders.e', $data->id),
                            'url_destroy' => route('sliders.r', $data->id)
                        ]);
                    })
                    ->rawColumns(['action', 'status', 'position', 'image'])
                    ->make(true);
        }
        return view('backend.sliders.index');
    }

    public function create()
    {
        return view('backend.sliders.add');
    }

    public function store(Request $request, Slider $slider)
    {
        $request->validate([
            'image' => 'required|mimes:jpeg,jpg,png|max:10000',
        ]);
        try {
            $maxPosition = $slider->max('position');

            $str = date('dmHis', strtotime(now()));

            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $params['image'] =$str.'-'.$image->getClientOriginalName();
            }

            $params['status'] = $request->status;
            $params['position'] = $maxPosition + 1;

            $action = $slider->create($params);

            if ($action) {
                if ($request->hasFile('image')) {
                    $image = $request->file('image');
                    $path = public_path('uploads/sliders');
                    $image->move($path, $params['image']);
                }
                return redirect()->route('sliders')
                    ->with(['alert-type' => 'success', 'message' => 'Thêm mới thành công']);
            }
        } catch (\Exception $e) {
            // dd($e);
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }
    
    public function edit(Slider $slider, $id)
    {
        return view('backend.sliders.edit', [
            'detail' => $slider->findOrFail($id)
        ]);
    }

    public function update(Request $request, Slider $slider, $id)
    {
        $request->validate([
            'image' => 'nullable|mimes:jpeg,jpg,png|max:10000',
        ]);
        
        try {
            $model = $slider->findOrFail($id);
            if (empty($model)) {
                return back()
                    ->with(['alert-type' => 'error', 'message' => 'Không tìm thấy dữ liệu']);
            }

            $params['status'] = Ultilities::clearXSS($request->status);

            $str = date('dmHis', strtotime(now()));
            
            if ($request->hasFile('image')) {
                $oldImage = $model->image;
                $image = $request->file('image');
                $params['image'] = $str.'-'.$image->getClientOriginalName();
            }

            $action = $model->update($params);

            if ($action) {
                if ($request->hasFile('image')) {
                    if ($oldImage != "") {
                        $path = public_path() . '/uploads/sliders/'. $oldImage;
                        if (file_exists($path)) {
                            File::delete($path);
                        }
                    }
                    $image = $request->file('image');
                    $path = public_path('uploads/sliders');
                    $image->move($path, $params['image']);
                }
                return redirect()->route('sliders')
                    ->with(['alert-type' => 'success', 'message' => 'Cập nhật thành công']);
            }
        } catch (\Exception $e) {
            // dd($e);
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }

    public function destroy(Slider $slider, $id)
    {
        $model = $slider->findOrFail($id);
        
        if ($model->image != "") {
            $path = public_path() . '/uploads/sliders/'. $model->image;
            if (file_exists($path)) {
                File::delete($path);
            }
        }

        $model->delete();
        return 0;
    }

    public function changeStatus(Request $request, Slider $slider, $id, $value)
    {
        $id = $request->id;
        $value = $request->value;
        $update = $slider->changeStatus($id, $value);
        
        if ($update) {
            return 1;
        }

        return 0;
    }

    public function changePosition(Request $request, Slider $slider, $id)
    {
        $type = $request->type;

        $data = $slider->findOrFail($id);
        $position = $data->position;

        if ($type == 'up') {
            $model = $slider->getSmallerPosition($position);
        } else {
            $model = $slider->getGreaterPosition($position);
        }
        if (!empty($model)) {
            $newPosition = $model->position;
            $data->update(['position' => $newPosition]);
            $model->update(['position' => $position]);
            return [1, $type];
        } else {
            return [0, $type];
        }
    }
}
