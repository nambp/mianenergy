<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libraries\Ultilities;
use DataTables;
use Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use App\Models\Agency;

class AgencyController extends Controller
{
    public function index(Request $request, Agency $agency)
    {
        if ($request->ajax()) {
            $data = $agency->orderBy('id', 'desc');
            if (!empty($request->get('search'))) {
                $keyword = $request->get('search');
                $data->where('title', 'LIKE', '%'.$keyword.'%')->orWhere('content', 'LIKE', '%'.$keyword.'%');
            }
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function ($data) {
                        return view('elements.backend.action', [
                            'model' => $data,
                            'url_show' => null,
                            'url_edit' => route('agencies.e', $data->id),
                            'url_destroy' => route('agencies.r', $data->id)
                        ]);
                    })
                    ->rawColumns(['action', 'logo'])
                    ->make(true);
        }
        return view('backend.agencies.index');
    }

    public function create()
    {
        return view('backend.agencies.add');
    }

    public function store(Request $request, Agency $agency)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'content' => 'required|string|max:400',
        ]);
        try {
            $params['title'] = Ultilities::clearXSS($request->title);
            $params['content'] = Ultilities::clearXSS($request->content);

            $action = $agency->create($params);

            if ($action) {
                return redirect()->route('agencies')
                    ->with(['alert-type' => 'success', 'message' => 'Thêm mới thành công']);
            }
        } catch (\Exception $e) {
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }
    
    public function edit(Agency $agency, $id)
    {
        return view('backend.agencies.edit', [
            'detail' => $agency->findOrFail($id)
        ]);
    }

    public function update(Request $request, Agency $agency, $id)
    {
        $request->validate([
            'title' => 'required|string|max:150',
            'content' => 'required|string|max:400',
        ]);
        
        try {
            $model = $agency->findOrFail($id);
            if (empty($model)) {
                return back()
                    ->with(['alert-type' => 'error', 'message' => 'Không tìm thấy dữ liệu']);
            }

            $params['title'] = Ultilities::clearXSS($request->title);
            $params['content'] = Ultilities::clearXSS($request->content);

            $action = $model->update($params);

            if ($action) {
                return redirect()->route('agencies')
                    ->with(['alert-type' => 'success', 'message' => 'Cập nhật thành công']);
            }
        } catch (\Exception $e) {
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }

    public function destroy(Agency $agency, $id)
    {
        $model = $agency->findOrFail($id);

        $model->delete();
        return 0;
    }
}
