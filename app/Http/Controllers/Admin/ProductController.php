<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libraries\Ultilities;
use DataTables;
use Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use App\Models\Product;
use App\Models\Category;
use App\Models\Brand;

class ProductController extends Controller
{
    public function index(Request $request, Product $product, Category $category)
    {
        $categories = $category->orderBy('title', 'asc')->get();
        if ($request->ajax()) {
            $data = $product->with(['category', 'brand']);
            if (!empty($request->get('search'))) {
                $keyword = $request->get('search');
                $data->where('title', 'LIKE', '%'.$keyword.'%')->orWhere('code', 'LIKE', '%'.$keyword.'%');
            }
            
            if (!empty($request->get('category_id')) || $request->get('category_id') !="") {
                $data->where('category_id', '=', $request->get('category_id'));
            }

            if (!empty($request->get('status')) || $request->get('status') !="") {
                $data->where('status', '=', $request->get('status'));
            }

            if (!empty($request->get('special')) || $request->get('special') !="") {
                $data->where('spec', '=', $request->get('special'));
            }
            $data->latest();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status', function ($data) {
                        return view('elements.backend.status', [
                            'model' => $data,
                            'url' => route('products.st', ['id' => $data->id, 'value' => $data->status])
                        ]);
                    })
                    ->addColumn('category', function ($data) {
                        return !empty($data->category) ? $data->category->title : '-';
                    })
                    ->addColumn('brand', function ($data) {
                        return !empty($data->brand) ? $data->brand->title : '-';
                    })
                    ->addColumn('action', function ($data) {
                        return view('elements.backend.action', [
                            'model' => $data,
                            'url_show' => route('products.d', $data->id),
                            'url_edit' => route('products.e', $data->id),
                            'url_destroy' => route('products.r', $data->id)
                        ]);
                    })
                    ->rawColumns(['action', 'status', 'position'])
                    ->make(true);
        }
        return view('backend.products.index', [
            'categories' => $categories
        ]);
    }

    public function show(Product $product, $id)
    {
        return view('backend.products.show', [
            'detail' => $product->with(['category', 'brand', 'galleries', 'technicals'])->findOrFail($id)
        ]);
    }

    public function create(Category $category, Brand $brand)
    {
        $categories = $category->orderBy('title', 'asc')->get();
        $brands = $brand->orderBy('title', 'asc')->get();
        return view('backend.products.add', compact('categories', 'brands'));
    }

    public function store(Request $request, Product $product)
    {
        $request->validate([
            'title' => 'required|string|max:150',
            'slug' => 'required|string|max:150|unique:products,slug,NULL,id,deleted_at,NULL',
            'meta_title' => 'required|string|max:150',
            'meta_keywords' => 'required|string|max:150',
            'meta_description' => 'required|string|max:400',
            'image_seo' => 'required|mimes:jpeg,jpg,png|max:10000',
            'image' => 'required|mimes:jpeg,jpg,png|max:10000',
            'category_id' => 'required|numeric|min:1',
            'brand_id' => 'required|numeric|min:1',
            'code' => 'required|string|min:3|max:12',
            'price' => 'nullable|numeric',
        ]);
        try {
            $params['title'] = Ultilities::clearXSS($request->title);
            $params['slug'] = Str::slug(Ultilities::clearXSS($request->slug));
            $params['meta_title'] = Ultilities::clearXSS($request->meta_title);
            $params['meta_keywords'] = Ultilities::clearXSS($request->meta_keywords);
            $params['meta_description'] = Ultilities::clearXSS($request->meta_description);
            $params['status'] = Ultilities::clearXSS($request->status);
            $params['content'] = $request->content;
            $params['code'] = Ultilities::clearXSS($request->code);
            $params['price'] = ($request->price) ? Ultilities::clearXSS($request->price) : 0;
            $params['category_id'] = Ultilities::clearXSS($request->category_id);
            $params['brand_id'] = Ultilities::clearXSS($request->brand_id);
            $params['spec'] = Ultilities::clearXSS($request->spec);
            
            $slug = $params['slug'];
            $str = date('dmHis', strtotime(now()));

            if ($request->hasFile('image_seo')) {
                $image_seo = $request->file('image_seo');
                $params['seo_image'] = $slug.'-'.$str.'.'.$image_seo->getClientOriginalExtension();
            }

            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $params['image'] = $slug.'-'.($str + 1).'.'.$image->getClientOriginalExtension();
            }

            $action = $product->create($params);

            if ($action) {
                if ($request->hasFile('image_seo')) {
                    $image_seo = $request->file('image_seo');
                    $path = public_path('uploads/products');
                    $image_seo->move($path, $params['seo_image']);
                }
                if ($request->hasFile('image')) {
                    $image = $request->file('image');
                    $path = public_path('uploads/products');
                    $image->move($path, $params['image']);
                }
                return redirect()->route('products')
                    ->with(['alert-type' => 'success', 'message' => 'Thêm mới thành công']);
            }
        } catch (\Exception $e) {
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }
    
    public function edit(Product $product, Category $category, Brand $brand, $id)
    {
        $categories = $category->orderBy('title', 'asc')->get();
        $brands = $brand->orderBy('title', 'asc')->get();
        return view('backend.products.edit', [
            'detail' => $product->findOrFail($id),
            'categories' => $categories,
            'brands' => $brands,
        ]);
    }

    public function update(Request $request, Product $product, $id)
    {
        $request->validate([
            'title' => 'required|string|max:150',
            'slug' => "required|string|max:150|unique:products,slug,{$id},id,deleted_at,NULL",
            'meta_title' => 'required|string|max:150',
            'meta_keywords' => 'required|string|max:150',
            'meta_description' => 'required|string|max:400',
            'image_seo' => 'nullable|mimes:jpeg,jpg,png|max:10000',
            'image' => 'nullable|mimes:jpeg,jpg,png|max:10000',
            'category_id' => 'required|numeric|min:1',
            'brand_id' => 'required|numeric|min:1',
            'code' => 'required|string|min:5|max:12',
            'price' => 'nullable|numeric',
        ]);
        
        try {
            $model = $product->findOrFail($id);
            if (empty($model)) {
                return back()
                    ->with(['alert-type' => 'error', 'message' => 'Không tìm thấy dữ liệu']);
            }

            $params['title'] = Ultilities::clearXSS($request->title);
            $params['slug'] = Str::slug(Ultilities::clearXSS($request->slug));
            $params['meta_title'] = Ultilities::clearXSS($request->meta_title);
            $params['meta_keywords'] = Ultilities::clearXSS($request->meta_keywords);
            $params['meta_description'] = Ultilities::clearXSS($request->meta_description);
            $params['status'] = Ultilities::clearXSS($request->status);
            $params['content'] = $request->content;
            $params['code'] = Ultilities::clearXSS($request->code);
            $params['price'] = ($request->price) ? Ultilities::clearXSS($request->price) : 0;
            $params['category_id'] = Ultilities::clearXSS($request->category_id);
            $params['brand_id'] = Ultilities::clearXSS($request->brand_id);
            $params['spec'] = Ultilities::clearXSS($request->spec);

            $slug = $params['slug'];
            $str = date('dmHis', strtotime(now()));
            
            if ($request->hasFile('image_seo')) {
                $image_seo = $request->file('image_seo');
                $params['seo_image'] = $slug.'-'.$str.'.'.$image_seo->getClientOriginalExtension();
            }
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $params['image'] = $slug.'-'.($str + 1).'.'.$image->getClientOriginalExtension();
            }

            $action = $model->update($params);

            if ($action) {
                if ($request->hasFile('image_seo')) {
                    $image_seo = $request->file('image_seo');
                    $path = public_path('uploads/products');
                    $image_seo->move($path, $params['seo_image']);
                }
                if ($request->hasFile('image')) {
                    $image = $request->file('image');
                    $path = public_path('uploads/products');
                    $image->move($path, $params['image']);
                }
                return redirect()->route('products')
                    ->with(['alert-type' => 'success', 'message' => 'Cập nhật thành công']);
            }
        } catch (\Exception $e) {
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }

    public function destroy(Product $product, $id)
    {
        $model = $product->with(['orders', 'rates'])->findOrFail($id);

        if ((!empty($model->orders) && count($model->orders) > 0) || (!empty($model->rates) && count($model->rates) > 0)) {
            return 1;
        } else {
            if ($model->seo_image != "") {
                $path = public_path() . '/uploads/products/'. $model->seo_image;
                if (file_exists($path)) {
                    File::delete($path);
                }
            }
            if ($model->image != "") {
                $path = public_path() . '/uploads/products/'. $model->image;
                if (file_exists($path)) {
                    File::delete($path);
                }
            }
    
            $model->delete();
            return 0;
        }
    }

    public function changeStatus(Request $request, Product $product, $id, $value)
    {
        $id = $request->id;
        $value = $request->value;
        $update = $product->changeStatus($id, $value);
        
        if ($update) {
            return 1;
        }

        return 0;
    }
}
