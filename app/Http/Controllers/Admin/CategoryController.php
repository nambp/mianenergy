<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libraries\Ultilities;
use DataTables;
use Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index(Request $request, Category $category)
    {
        if ($request->ajax()) {
            $data = $category->whereNull('parent_id')->orderBy('position', 'asc');
            if (!empty($request->get('search'))) {
                $keyword = $request->get('search');
                $data->where('title', 'LIKE', '%'.$keyword.'%');
            }
            
            if (!empty($request->get('status')) || $request->get('status') !="") {
                $data->where('status', '=', $request->get('status'));
            }
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status', function ($data) {
                        return view('elements.backend.status', [
                            'model' => $data,
                            'url' => route('categories.st', ['id' => $data->id, 'value' => $data->status])
                        ]);
                    })
                    ->editColumn('position', function ($data) {
                        return view('elements.backend.position', [
                            'model' => $data,
                            'url' => route('categories.p', ['id' => $data->id]),
                        ]);
                    })
                    ->addColumn('action', function ($data) {
                        return view('elements.backend.action', [
                            'model' => $data,
                            'url_show' => route('sub-categories', $data->id),
                            'url_edit' => route('categories.e', $data->id),
                            'url_destroy' => route('categories.r', $data->id)
                        ]);
                    })
                    ->rawColumns(['action', 'status', 'position'])
                    ->make(true);
        }
        return view('backend.categories.index');
    }

    public function create()
    {
        return view('backend.categories.add');
    }

    public function store(Request $request, Category $category)
    {
        $request->validate([
            'title' => 'required|string|max:150',
            'slug' => 'required|string|max:150|unique:categories,slug,NULL,id,deleted_at,NULL',
            'meta_title' => 'required|string|max:150',
            'meta_keywords' => 'required|string|max:150',
            'meta_description' => 'required|string|max:400',
            'image_seo' => 'required|mimes:jpeg,jpg,png|max:10000',
            'banner' => 'required|mimes:jpeg,jpg,png|max:10000',
        ]);
        try {
            // get max position
            $maxPosition = $category->whereNull('parent_id')->max('position');

            $params['title'] = Ultilities::clearXSS($request->title);
            $params['slug'] = Str::slug(Ultilities::clearXSS($request->slug));
            $params['meta_title'] = Ultilities::clearXSS($request->meta_title);
            $params['meta_keywords'] = Ultilities::clearXSS($request->meta_keywords);
            $params['meta_description'] = Ultilities::clearXSS($request->meta_description);
            $params['status'] = Ultilities::clearXSS($request->status);
            $params['content'] = $request->content;
            $params['position'] = $maxPosition + 1;

            $slug = $params['slug'];
            $str = date('dmHis', strtotime(now()));

            if ($request->hasFile('image_seo')) {
                $image_seo = $request->file('image_seo');
                $params['seo_image'] = $slug.'-'.$str.'.'.$image_seo->getClientOriginalExtension();
            }

            if ($request->hasFile('banner')) {
                $banner = $request->file('banner');
                $params['banner'] = $slug.'-'.($str + 1).'.'.$banner->getClientOriginalExtension();
            }

            $action = $category->create($params);

            if ($action) {
                if ($request->hasFile('image_seo')) {
                    $image_seo = $request->file('image_seo');
                    $path = public_path('uploads/categories');
                    $image_seo->move($path, $params['seo_image']);
                }
                if ($request->hasFile('banner')) {
                    $banner = $request->file('banner');
                    $path = public_path('uploads/categories');
                    $banner->move($path, $params['banner']);
                }
                return redirect()->route('categories')
                    ->with(['alert-type' => 'success', 'message' => 'Thêm danh mục thành công']);
            }
        } catch (\Exception $e) {
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }
    
    public function edit(Category $category, $id)
    {
        return view('backend.categories.edit', [
            'detail' => $category->findOrFail($id)
        ]);
    }

    public function update(Request $request, Category $category, $id)
    {
        $request->validate([
            'title' => 'required|string|max:150',
            'slug' => "required|string|max:150|unique:categories,slug,{$id},id,deleted_at,NULL",
            'meta_title' => 'required|string|max:150',
            'meta_keywords' => 'required|string|max:150',
            'meta_description' => 'required|string|max:400',
            'image_seo' => 'nullable|mimes:jpeg,jpg,png|max:10000',
            'banner' => 'nullable|mimes:jpeg,jpg,png|max:10000',
        ]);
        
        try {
            $model = $category->findOrFail($id);
            if (empty($model)) {
                return back()
                    ->with(['alert-type' => 'error', 'message' => 'Không tìm thấy dữ liệu']);
            }

            $params['title'] = Ultilities::clearXSS($request->title);
            $params['slug'] = Str::slug(Ultilities::clearXSS($request->slug));
            $params['meta_title'] = Ultilities::clearXSS($request->meta_title);
            $params['meta_keywords'] = Ultilities::clearXSS($request->meta_keywords);
            $params['meta_description'] = Ultilities::clearXSS($request->meta_description);
            $params['status'] = Ultilities::clearXSS($request->status);
            $params['content'] = $request->content;

            $slug = $params['slug'];
            $str = date('dmHis', strtotime(now()));
            
            if ($request->hasFile('image_seo')) {
                $image_seo = $request->file('image_seo');
                $params['seo_image'] = $slug.'-'.$str.'.'.$image_seo->getClientOriginalExtension();
            }
            if ($request->hasFile('banner')) {
                $banner = $request->file('banner');
                $params['banner'] = $slug.'-'.($str + 1).'.'.$banner->getClientOriginalExtension();
            }

            $action = $model->update($params);

            if ($action) {
                if ($request->hasFile('image_seo')) {
                    $image_seo = $request->file('image_seo');
                    $path = public_path('uploads/categories');
                    $image_seo->move($path, $params['seo_image']);
                }
                if ($request->hasFile('banner')) {
                    $banner = $request->file('banner');
                    $path = public_path('uploads/categories');
                    $banner->move($path, $params['banner']);
                }
                return redirect()->route('categories')
                    ->with(['alert-type' => 'success', 'message' => 'Cập nhật danh mục thành công']);
            }
        } catch (\Exception $e) {
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }

    public function destroy(Category $category, $id)
    {
        $model = $category->with(['children', 'products'])->findOrFail($id);

        if ((!empty($model->children) && count($model->children) > 0) || (!empty($model->products) && count($model->products) > 0)) {
            return 1;
        } else {
            if ($model->seo_image != "") {
                $path = public_path() . '/uploads/categories/'. $model->seo_image;
                if (file_exists($path)) {
                    File::delete($path);
                }
            }
            if ($model->banner != "") {
                $path = public_path() . '/uploads/categories/'. $model->banner;
                if (file_exists($path)) {
                    File::delete($path);
                }
            }
    
            $model->delete();
            return 0;
        }
    }

    public function changeStatus(Request $request, Category $category, $id, $value)
    {
        $id = $request->id;
        $value = $request->value;
        $update = $category->changeStatus($id, $value);
        
        if ($update) {
            return 1;
        }

        return 0;
    }

    public function changePosition(Request $request, Category $category, $id)
    {
        $type = $request->type;

        $data = $category->findOrFail($id);
        $position = $data->position;

        if ($type == 'up') {
            $model = $category->getSmallerPosition($position);
        } else {
            $model = $category->getGreaterPosition($position);
        }
        if (!empty($model)) {
            $newPosition = $model->position;
            $data->update(['position' => $newPosition]);
            $model->update(['position' => $position]);
            return [1, $type];
        } else {
            return [0, $type];
        }
    }

    public function list(Request $request, Category $category, $id)
    {
        $detail = $category->findOrFail($id);
        if ($request->ajax()) {
            $data = $category->where('parent_id', $id)->orderBy('position', 'asc');
            if (!empty($request->get('search'))) {
                $keyword = $request->get('search');
                $data->where('title', 'LIKE', '%'.$keyword.'%');
            }
            
            if (!empty($request->get('status')) || $request->get('status') !="") {
                $data->where('status', '=', $request->get('status'));
            }
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status', function ($data) {
                        return view('elements.backend.status', [
                            'model' => $data,
                            'url' => route('categories.st', ['id' => $data->id, 'value' => $data->status])
                        ]);
                    })
                    ->editColumn('position', function ($data) {
                        return view('elements.backend.position', [
                            'model' => $data,
                            'url' => route('sub-categories.p', ['id' => $data->parent_id, 'cate' => $data->id]),
                        ]);
                    })
                    ->addColumn('action', function ($data) {
                        return view('elements.backend.action', [
                            'model' => $data,
                            'url_show' => null,
                            'url_edit' => route('sub-categories.e', [$data->parent_id, $data->id]),
                            'url_destroy' => route('categories.r', $data->id)
                        ]);
                    })
                    ->rawColumns(['action', 'status', 'position'])
                    ->make(true);
        }
        return view('backend.sub-categories.index', compact('detail'));
    }

    public function add(Request $request, Category $category, $id)
    {
        return view('backend.sub-categories.add', [
            'detail' => $category->findOrFail($id)
        ]);
    }

    public function saveCate(Request $request, Category $category, $id)
    {
        $request->validate([
            'title' => 'required|string|max:150',
            'short_title' => 'required|string|max:100',
            'slug' => 'required|string|max:150|unique:categories,slug,NULL,id,deleted_at,NULL',
            'meta_title' => 'required|string|max:150',
            'meta_keywords' => 'required|string|max:150',
            'meta_description' => 'required|string|max:400',
            'image_seo' => 'required|mimes:jpeg,jpg,png|max:10000',
        ]);
        try {
            // get max position
            $maxPosition = $category->where('parent_id', $id)->max('position');

            $params['title'] = Ultilities::clearXSS($request->title);
            $params['short_title'] = Ultilities::clearXSS($request->short_title);
            $params['slug'] = Str::slug(Ultilities::clearXSS($request->slug));
            $params['meta_title'] = Ultilities::clearXSS($request->meta_title);
            $params['meta_keywords'] = Ultilities::clearXSS($request->meta_keywords);
            $params['meta_description'] = Ultilities::clearXSS($request->meta_description);
            $params['status'] = Ultilities::clearXSS($request->status);
            $params['content'] = $request->content;
            $params['parent_id'] = $request->parent_id;
            $params['position'] = $maxPosition + 1;

            $slug = $params['slug'];
            $str = date('dmHis', strtotime(now()));

            if ($request->hasFile('image_seo')) {
                $image_seo = $request->file('image_seo');
                $params['seo_image'] = $slug.'-'.$str.'.'.$image_seo->getClientOriginalExtension();
            }

            $action = $category->create($params);

            if ($action) {
                if ($request->hasFile('image_seo')) {
                    $image_seo = $request->file('image_seo');
                    $path = public_path('uploads/categories');
                    $image_seo->move($path, $params['seo_image']);
                }
                return redirect()->route('sub-categories', $id)
                    ->with(['alert-type' => 'success', 'message' => 'Thêm danh mục thành công']);
            }
        } catch (\Exception $e) {
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }

    public function change(Category $category, $id, $cate)
    {
        $detail = $category->findOrFail($id);
        $model = $category->findOrFail($cate);
        $categories = $category->whereNull('parent_id')->select('id', 'title')->get();
        return view('backend.sub-categories.edit', [
            'detail' => $detail,
            'model' => $model,
            'categories' => $categories,
        ]);
    }

    public function saveChange(Request $request, Category $category, $id)
    {
        $request->validate([
            'title' => 'required|string|max:150',
            'short_title' => 'required|string|max:100',
            'slug' => "required|string|max:150|unique:categories,slug,{$id},id,deleted_at,NULL",
            'meta_title' => 'required|string|max:150',
            'meta_keywords' => 'required|string|max:150',
            'meta_description' => 'required|string|max:400',
            'image_seo' => 'nullable|mimes:jpeg,jpg,png|max:10000',
            'parent_id' => 'required|numeric|min:1',
        ]);
        
        try {
            $model = $category->findOrFail($id);
            $parent_id  = $model->parent_id;
            if (empty($model)) {
                return back()
                    ->with(['alert-type' => 'error', 'message' => 'Không tìm thấy dữ liệu']);
            }

            $params['title'] = Ultilities::clearXSS($request->title);
            $params['short_title'] = Ultilities::clearXSS($request->short_title);
            $params['slug'] = Str::slug(Ultilities::clearXSS($request->slug));
            $params['meta_title'] = Ultilities::clearXSS($request->meta_title);
            $params['meta_keywords'] = Ultilities::clearXSS($request->meta_keywords);
            $params['meta_description'] = Ultilities::clearXSS($request->meta_description);
            $params['status'] = Ultilities::clearXSS($request->status);
            $params['content'] = $request->content;
            $params['parent_id'] = $request->parent_id;

            $slug = $params['slug'];
            $str = date('dmHis', strtotime(now()));
            
            if ($request->hasFile('image_seo')) {
                $image_seo = $request->file('image_seo');
                $params['seo_image'] = $slug.'-'.$str.'.'.$image_seo->getClientOriginalExtension();
            }

            $action = $model->update($params);

            if ($action) {
                if ($request->hasFile('image_seo')) {
                    $image_seo = $request->file('image_seo');
                    $path = public_path('uploads/categories');
                    $image_seo->move($path, $params['seo_image']);
                }
                
                return redirect()->route('sub-categories', $parent_id)
                    ->with(['alert-type' => 'success', 'message' => 'Cập nhật danh mục thành công']);
            }
        } catch (\Exception $e) {
            return back()
                    ->with('exception', $e->getMessage())
                    ->with([
                        'alert-type' => 'error',
                        'message' => 'Đã có lỗi xảy ra, vui lòng thử lại'
                    ])->withInput();
        }
    }

    public function updatePosition(Request $request, Category $category, $id, $cate)
    {
        $type = $request->type;

        $data = $category->findOrFail($cate);
        $position = $data->position;

        if ($type == 'up') {
            $model = $category->getSmallerPositionSub($position, $id);
        } else {
            $model = $category->getGreaterPositionSub($position, $id);
        }
        if (!empty($model)) {
            $newPosition = $model->position;
            $data->update(['position' => $newPosition]);
            $model->update(['position' => $position]);
            return [1, $type];
        } else {
            return [0, $type];
        }
    }
}
