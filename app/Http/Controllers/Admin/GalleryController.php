<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Gallery;
use Str;
use Carbon\Carbon;

class GalleryController extends Controller
{
    public function save(Request $request, Gallery $gallery, Product $product)
    {
        $this->validate($request, [
            'album' => 'required',
            'album.*' => 'mimes:jpg,png,jpeg'
        ]);
        $albums = array();
        
        if ($request->has('album') && $request->hasFile('album')) {
            try {
                $data = $product->findOrFail($request->product_id);
                $slug = $data->slug;
                foreach ($request->file('album') as $key => $file) {
                    $name = $slug.'-'.($key + 1).'-'.time().'.'.$file->extension();
                    $file->move(public_path().'/uploads/galleries/', $name);
                    array_push($albums, [
                        'product_id' => $request->product_id,
                        'image' => $name
                    ]);
                }
                
                $album = $gallery->insert($albums);

                if (!empty($album)) {
                        return back()->with(['alert-type' => 'success', 'message' => __('Upload thành công'),'images_error' => []]);
                } else {
                        return back()->with(['alert-type' => 'error', 'message' => __('Upload thất bại')]);
                }
            } catch (\Exception $e) {
                foreach ($albums as $media) {
                    if (file_exists(public_path().'/uploads/galleries/'.$media['image'])) {
                        \unlink(public_path().'/uploads/galleries/'.$media['image']);
                    }
                }
                return back()
                    ->with(['alert-type' => 'error', 'message' => __('Upload thất bại')]);
            }
        }
    }

    public function destroy(Request $request, Gallery $gallery)
    {
        $album = $gallery->findOrFail($request->id);
        $path = public_path().'/uploads/galleries/'.$album->image;

        if (file_exists($path)) {
            unlink($path);
        }

        $album->delete();

        return back()->with(['alert-type' => 'success', 'message' => __('Xóa thành công')]);
    }
}
