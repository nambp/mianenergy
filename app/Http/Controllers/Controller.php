<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Models\Information;
use App\Models\Agency;
use App\Models\Category;
use App\Models\Brand;
use View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $logo = Brand::get();
        $info = Information::first();
        $agencies = Agency::get();
        $menu = Category::with(['children'])->whereStatus(1)->orderBy('position', 'asc')->get();
        View::share('info', $info);
        View::share('agencies', $agencies);
        View::share('menu', $menu);
        View::share('logo',$logo);
    }

    /**
     * resize image
     * @param file file
     * @param int type = 0 mealkit, =1 market
     */
    public function thumbnail(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'file' => 'required',
            'width' => 'sometimes|nullable|numeric',
            'height' => 'sometimes|nullable|numeric',
            'quatity' => 'sometimes|nullable|numeric',
        ]);

        if ($validator->fails()) {
            abort(404, 'Not found image');
        }
        $file = public_path($request->path. $request->file);

        $width = (!empty($request->width)) ? $request->width : null;
        $height = (!empty($request->height)) ? $request->height : null;

        if (empty($width) || empty($height)) {
            $sizeImage = getimagesize($file);
            $width = $sizeImage[0]/4;
            $height = $sizeImage[1]/4;
        }

        // Resize
        $img = Image::cache(function ($image) use ($file, $width, $height) {
            $image->make($file)->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
        }, 24*30*60, true);
        // create response and add encoded image data
        $response = Response::make($img->encode($img->extension));
        // set content-type
        $response->header('Content-Type', $img->mime);
        // output
        return $response;
        // <img src="{{ route('image.thumbnail',['file'=>$item->avatar, 'width'=>config('constant.RESIZE.MEALKIT_AVAYAR.WIDTH'), 'height'=>config('constant.RESIZE.MEALKIT_AVAYAR.HEIGHT'), 'type'=>0])}}" alt="" class="img-fluid">
    }
}
