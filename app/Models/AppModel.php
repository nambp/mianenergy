<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppModel extends Model
{
    use HasFactory;

    public function changePosition($id, $position)
    {
        return $this->where($this->primaryKey, $id)->update(['position' => $position]);
    }

    public function changeStatus($id, $value)
    {
        return $this->where($this->primaryKey, $id)->update(['status' => $value]);
    }
}
