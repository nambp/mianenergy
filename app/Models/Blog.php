<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\AppModel;

class Blog extends AppModel
{
    use HasFactory;
    protected $table = 'blogs';
    protected $fillable = [
        'title', 'slug', 'summary', 'image', 'content', 'status',
        'meta_title', 'meta_keywords', 'meta_description', 'seo_image'
    ];

    const ACTIVE = 1;
    const BLOCK = 2 ;

    public function getListBlog()
    {
        return $this->where('status',self::ACTIVE)->paginate(10);
    }
}
