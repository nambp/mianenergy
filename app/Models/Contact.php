<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Libraries\Ultilities;
use App\Models\AppModel;

class Contact extends AppModel
{
    use HasFactory;
    protected $table = 'contacts';
    protected $fillable = [
        'name', 'phone', 'email', 'address', 'message', 'status'
    ];

    const ACTIVE = 1;
    const BLOCK = 2;

    public function rule()
    {
        return [
            'name'=> "required|min:3|max:100",
            'phone'=>'required|min:8|numeric',
            'email'=>'required|email|max:100',
            'address'=>'nullable|max:255',
            'message'=> 'nullable|max:500'
        ];
    }

    public function addContact($request)
    {
        $params = [
            'name'=> Ultilities::clearXSS($request->name),
            'phone'=> Ultilities::clearXSS($request->phone),
            'email'=> Ultilities::clearXSS($request->email),
            'address'=> Ultilities::clearXSS($request->address),
            'message'=> Ultilities::clearXSS($request->message),
            'status'=> self::ACTIVE,
        ];
        $contact = $this->create($params);
        return $contact;
    }
}
