<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\AppModel;

class Project extends AppModel
{
    use HasFactory;
    protected $table = 'projects';
    protected $fillable = [
        'title', 'slug', 'summary', 'content', 'image', 'status',
        'meta_title', 'meta_keywords', 'meta_description', 'seo_image'
    ];

    const ACTIVE = 1;
    const BLOCK = 2 ;

    public function getListProject()
    {
        return $this->where('status',self::ACTIVE)->paginate(10);
    }
}
