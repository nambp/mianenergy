<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Libraries\Ultilities;
use App\Models\AppModel;

class Order extends AppModel
{
    use HasFactory;
    protected $table = 'orders';
    protected $fillable = [
        'product_id', 'name', 'email', 'phone', 'quantity', 'message', 'status'
    ];

    const NEW = 1;
    const CHECKED = 2;

    public function saveAdvisory($request)
    {
        $params = [
            'product_id' => Ultilities::clearXSS($request->product_id),
            'name' => Ultilities::clearXSS($request->name),
            'email' => Ultilities::clearXSS($request->email),
            'phone' => Ultilities::clearXSS($request->phone),
            'quantity' => ($request->quantity) ? Ultilities::clearXSS($request->quantity) : 1,
            'message' => Ultilities::clearXSS($request->message),
            'status'=> self::NEW,
        ];
        
        $create =  $this->create($params);
        return $create;
    }

}
