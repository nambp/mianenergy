<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\AppModel;

class Slider extends AppModel
{
    use HasFactory;
    protected $table = 'sliders';
    protected $fillable = [
        'image', 'position', 'status'
    ];

    public function getSmallerPosition($position)
    {
        return $this->where('position', '<', $position)->orderBy('position', 'DESC')->first();
    }

    // Get data smaller by position
    public function getGreaterPosition($position)
    {
        return $this->where('position', '>', $position)->orderBy('position', 'ASC')->first();
    }

    public function getList()
    {
        return $this->where('status',1)->orderBy('position','asc')->get();
    }
}
