<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\AppModel;

class Product extends AppModel
{
    use HasFactory, SoftDeletes;
    protected $table = 'products';
    protected $fillable = [
        'code', 'category_id', 'brand_id', 'title', 'slug', 'price', 'content', 'image',
        'status', 'spec', 'meta_title', 'meta_keywords', 'meta_description', 'seo_image'
    ];

    const ACTIVE  = 1;
    const BLOCK = 2;

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    public function galleries()
    {
        return $this->hasMany(Gallery::class);
    }

    public function technicals()
    {
        return $this->hasMany(Technical::class);
    }

    public function rates()
    {
        return $this->hasMany(Rate::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function getDetail($slug)
    {
        return $this->where('slug',$slug)->with(['technicals','galleries','category'])->first();
    }

    public function getList()
    {
        return $this->where('status',self::ACTIVE)->orderBy('id','desc')->paginate(20);
    }

    public function filter($request)
    {
        $products = $this->where('status',self::ACTIVE);
        if(!empty($request->cate_id)){
            $products->where('category_id',$request->cate_id);
        }
        if(!empty($request->sort)){
            // dd($request->sort);
            if($request->sort == 1){
                $products->orderBy('id','desc');
            }else{
                $products->orderBy('id','asc');
            }
        }
        if(!empty($request->brand)){
            $products->whereIn('brand_id',$request->brand);
        }
        return $products->paginate(20);
    }
}
