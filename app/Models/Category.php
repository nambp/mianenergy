<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\AppModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends AppModel
{
    use HasFactory, SoftDeletes;
    protected $table = 'categories';
    protected $fillable = [
        'parent_id', 'title', 'slug', 'short_title', 'position', 'status',
        'content', 'banner', 'meta_title', 'meta_keywords', 'meta_description', 'seo_image'
    ];

    public function categories()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id')->with('categories');
    }

    public function parent()
    {
        return $this->belongsto(Category::class, 'parent_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    // Get data smaller by position
    public function getSmallerPosition($position)
    {
        return $this->whereNull('parent_id')->where('position', '<', $position)->orderBy('position', 'DESC')->first();
    }

    // Get data smaller by position
    public function getGreaterPosition($position)
    {
        return $this->whereNull('parent_id')->where('position', '>', $position)->orderBy('position', 'ASC')->first();
    }

    // Get data smaller by position
    public function getSmallerPositionSub($position, $parent)
    {
        return $this->where('parent_id', $parent)
            ->where('position', '<', $position)->orderBy('position', 'DESC')->first();
    }

    // Get data smaller by position
    public function getGreaterPositionSub($position, $parent)
    {
        return $this->where('parent_id', $parent)
            ->where('position', '>', $position)->orderBy('position', 'ASC')->first();
    }

    public function getList()
    {
        return $this->with(['children','products','parent'])->where('status',1)->get();
    }

    public function getCateBySlug($slug)
    {
        return $this->with(['children','products','parent'])->where('slug',$slug)->first();
    }
}
