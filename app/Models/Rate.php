<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\AppModel;
use App\Libraries\Ultilities;

class Rate extends AppModel
{
    use HasFactory;
    protected $table = 'rates';
    protected $fillable = [
        'product_id', 'star', 'message', 'name', 'email', 'status'
    ];

    const ACTIVE = 1;
    const BLOCK = 2;

    public function products()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function saveRate($request)
    {
        $params = [
            'product_id'=> Ultilities::clearXSS($request->product_id),
            'star'=> Ultilities::clearXSS($request->star),
            'message'=> Ultilities::clearXSS($request->message),
            'name'=> Ultilities::clearXSS($request->name),
            'status'=>  self::ACTIVE,
        ];
    }
}
