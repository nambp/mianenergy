<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    use HasFactory;
    protected $table = 'informations';
    protected $fillable = [
        'name', 'logo', 'phone', 'email', 'slogan', 'zalo', 'facebook', 'fanpage', 'face_id',
        'shopping_guide','payment_guide','purchase_forms','road_map','delivery_policy','exchange_policy','warranty_policy','privacy_policy','recruitment',
        'map', 'meta_title', 'meta_keywords', 'meta_description', 'seo_image'
    ];
}
