<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Mật khẩu của bạn đã được thay đổi!',
    'sent' => 'Chúng tôi đã gửi cho bạn email có chưa link đặt lại mật khẩu!',
    'throttled' => 'Vui lòng chờ trước khi thử lại.',
    'token' => 'Token đặt lại mật khẩu của bạn không chính xác.',
    'user' => "Chúng tôi không thể tìm thấy người dùng có địa chỉ email tương ứng.",

];
