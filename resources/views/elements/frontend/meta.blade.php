<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="title" content="@yield('title_all')">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="description" content="@yield('description')">
    <meta name="author" content="{{ $info->name }}">
    <link rel="canonical" href="{{ \Request::url() }}">
    <meta property="og:title" content="@yield('og_title')">
    <meta property="og:description" content="@yield('og_description')">
    <meta property="og:url" content="{{ \Request::url() }}">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="{{ $info->name }}">
    <meta property="og:image" content="@yield('og_image')">
    <meta property="og:image:secure_url" content="@yield('og_secure_url')">
    <meta property="og:image:height" content="300">
    <meta property="og:image:width" content="300">
    <meta name="twitter:title" content="@yield('t_title')">
    <meta name="twitter:description" content="@yield('t_description')">
    <meta property="fb:app_id" content="{{ $info->face_id }}">
    <meta name="robots" content="noodp">
    <link rel="icon" href="{{ asset('images/icon/favicon.png')}}" type="image/gif" sizes="192x192">
    <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,700;1,700&display=swa">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">
    <style>
        .call-contact:hover{
            color: #ee1a26 !important;
        }
    </style>
</head>