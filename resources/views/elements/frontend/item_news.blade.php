@php
    $lists = \App\Libraries\Ultilities::News();
@endphp
@if(count(@$lists) > 0)
    @foreach (@$lists as $item)
    <li class="blog-detail">
        <div class="image">
            <img src="{{ asset('uploads/blogs/'.$item->image) }}" alt="" srcset="">
        </div>
        <div class="blog-des">
            <a href="{{ route('news.detail',$item->slug) }}">{{ $item->title }}</a>
        </div>
    </li>
    @endforeach
@endif