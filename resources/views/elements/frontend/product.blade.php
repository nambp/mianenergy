@if(count(@$products) > 0)
    @foreach ($products as $item)
        <div class="item">
            <div class="product">
                <div class="image">
                    <a href="{{ route('product.detail',$item->slug) }}"><img class="img-fluid" src="{{ asset('uploads/products/'.$item->image) }}" alt="" srcset=""></a>
                </div>
                <div class="title-product">
                    <a href="{{ route('product.detail',$item->slug) }}">
                        {{ $item->title }}
                    </a>
                    <p>Giá : Liên hệ</p>
                </div>
            </div>
        </div>
    @endforeach
@else
    <p>Không có sản phẩm nào !</p>
@endif