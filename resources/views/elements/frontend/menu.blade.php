<header class="header">
    <div class="bg--main d-none" id="bg--main"></div>
    <div class="header-top">
        <div class="container-customer">
            
            <div class="row">
                <div class="col-lg-0 col-md-12 d-md-none">
                    <div class="logo ">
                        <div class="open-menu" id="open-menu">
                            <i class="fa fa-bars"></i>
                        </div>
                        <a href="{{ route('home')}}">
                            <img class="img-fluid img-logo" src="{{ asset('uploads/'.$info ->logo)}}" alt="" srcset="">
                        </a>
                        <div class="open-menu2" id="open-menu2">
                            <i class="fa fa-bars"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <ul class="open-menu nav nav-left rwd-menu" id="menu-mobile">
                        <div class="logo_mobile">
                            <div class="close" id="close-sidebar">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                        @php
                            $lists = \App\Libraries\Ultilities::getListCate();
                        @endphp
                        @foreach ($lists as $item)
                            @if ($item->parent_id == null)
                                <li class="nav-item menu-hasdropdown menu-hasflyout">
                                    <a class="nav-link nav-cate" href="javascript:void(0);">{{ $item->title}}<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                    <ul class="rwd-submenu">
                                        <li><a href="javascript:void(0);" class="back-btn"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp; Back</a></li>
                                        <li><a href="{{ route('category',$item->slug) }}">{{ $item->title}}</a></li>
                                        @foreach ($item->children as $value)
                                            <li><a href="{{ route('category',$value->slug) }}">{{ $value->title }}</a></li>
                                        @endforeach
                                        
                                    </ul>
                                </li>                                               
                            @endif
                        @endforeach
                       
                    </ul>
                    <div class="navigation" id="menu-mobile2">
                        <ul class="nav">
                            <div class="logo_mobile d-md-none">
                                <div class="close" id="close-sidebar2">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                            <li class="nav-item"><a href="{{ route('home') }}" class="nav-link">TRANG CHỦ </a></li>
                            <li class="nav-item"><a href="{{ route('introduce') }}" class="nav-link">GIỚI THIỆU </a></li>
                            <li class="nav-item"><a href="{{ route('product') }}" class="nav-link">SẢN PHẨM </a></li>
                            <li class="nav-item"><a href="{{ route('project') }}" class="nav-link">DỰ ÁN </a></li>
                            <li class="nav-item"><a href="{{ route('news') }}" class="nav-link">TIN TỨC </a></li>
                            <li class="nav-item"><a href="{{ route('contact') }}" class="nav-link">LIÊN HỆ </a></li>
                        </ul>
                    </div>
                </div>
{{--                 
                <div class="col-lg-4">
                    
                </div> --}}
            </div>
        </div>
    </div>
    <div class="header-main d-lg-none">
        <div class="container-customer">
            <div class="header-main-box">
                <div class="row">
                    <div class="col-lg-3 d-lg-none">
                        <div class="logo">
                            <a href="{{ route('home')}}">
                                <img class="img-fluid img-logo" src="{{ asset('uploads/'.$info ->logo)}}" alt="" srcset="">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="search">
                            <div class="tag">
                                <p><i class="fa fa-tag"></i> Quạt thông gió, quạt hút, quat treo tường, quat đứng, quat sàn, điều hoà không khí</p>
                            </div>
                            <div class="form-search">
                                <form action="{{ route('search') }}" method="POST">
                                    @csrf
                                    <div class="form-group d-flex">
                                        <input type="text" placeholder="Tìm kiếm" name="search" class="form-control border-radius">
                                        <button type="submit" class="btn border-radius btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="location d-flex">
                            {{-- <div class="icon-location">
                                <i class="fa fa-map-marker mr-2" aria-hidden="true"></i>
                            </div> --}}
                            <div class="title-location">
                                <div class="icon-location">
                                   <i class="call-contact fa fa-phone mr-2" aria-hidden="true"></i>
                                </div>
                                <p><a class="call-contact" style="color: #000;text-decoration: none;" href="tel:{{@$info->phone}}">{{@$info->phone}}</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                
            </div>
        </div>
    </div>
</header>
<section class="wide-nav">
    <div class="container-customer">
        <div class="wide-box">
            <div class="mega-menu d-lg-none">
                <div class="mega-menu-title">
                    <a href="{{ route('product') }}" class="text-white" style="text-decoration: none"><i class="fa fa-bars mr-2" aria-hidden="true"></i> DANH MỤC SẢN PHẨM</a>
                </div>
                
            </div>
            <div class="slogan">
                <div class="icon-left">
                    <i class="fa fa-volume-control-phone mr-2" aria-hidden="true"></i>
                </div>
                <div class="slogan-title">
                    <marquee width="100%" direction="left">
                        {{ $info->slogan}}
                    </marquee>
                </div>
                <div class="icon-right">
                    <p class="badge badge-danger">HOT</p>
                </div>
            </div>
        </div>

    </div>
</section>