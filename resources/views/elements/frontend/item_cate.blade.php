@php
    $lists = \App\Libraries\Ultilities::getListCate();
@endphp
@foreach ($lists as $keys => $item)
    @if ($item->parent_id == null)
        @php
            $check = $item->children;
            foreach ($check as $key => $value) {
                
                if($value->slug == @$slug){
                    $checks = 1;
                }else {
                    $checks = 0;
                }
            }
        @endphp
        {{-- {{  dd(($value->slug) ) }} --}}
        
        <a href="#submenu{{$keys+1}}" data-toggle="collapse" aria-expanded="{{ ($checks == 1) ? 'true' : 'false'}}" class="bg-white list-group-items list-group-item-action flex-column align-items-start category-item">
            <div class="d-flex w-100 justify-content-start align-items-center">
                <span class="menu-collapsed">{{ $item->title }}</span>
                <span class="submenu-icon ml-auto"></span>
            </div>
        </a>
        <div id='submenu{{$keys+1}}' class="collapse sidebar-submenu sub_menu {{ ($checks == 1) ? 'show' : ''}}" >
            @foreach ($item->children as $k => $value)
                <a href="{{ route('category',$value->slug) }}" class="list-group-item list-group-item-action text-dark" style="color: {{ ($value->slug == @$slug) ? '#ee1a26 !important' : ''}}">
                    <span class="menu-collapsed">{{ $value->title }}</span>
                </a>
            @endforeach
            {{-- <a href="#" class="list-group-item list-group-item-action text-dark active">
                <span class="menu-collapsed">Reports</span>
            </a>
            <a href="#" class="list-group-item list-group-item-action text-dark">
                <span class="menu-collapsed">Tables</span>
            </a> --}}
        </div>
        @php
            $checks = 0;
        @endphp
    @endif
@endforeach