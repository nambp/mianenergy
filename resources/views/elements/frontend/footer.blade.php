<section class="slogan-primary">
    <div class="container-customer">
        <div class="slogan-box">
            <h3>MIANENERGY GROUP - HÂN HẠNH ĐƯỢC PHỤC VỤ QUÝ KHÁCH HÀNG</h3>
        </div>
    </div>
</section>
<section class="company">
    <div class="container-customer">
        <div class="owl-carousel owl-logo owl-theme">
            @if(count($logo) > 0)
                @foreach ($logo as $item)
                    <div class="item">
                        <img class="img-fluid" src="{{ asset('uploads/brands/'.$item->logo) }}" alt="" srcset="">
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</section>
<section class="policy">
    <div class="container-customer">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="policy-box">
                    <ul>VỀ CHÚNG TÔI
                        <li><span class="text-danger">♦</span><a href="{{ route('introduce')}}" class="btn--bottom"> Giới thiệu công ty </a></li>
                        <li><span class="text-danger">♦</span><a href="{{!empty($info->recruitment) ? asset('uploads/'.$info->recruitment) : '#'}}" class="btn--bottom"> Tuyển dụng </a></li>
                        <li><span class="text-danger">♦</span><a href="{{ route('news')}}" class="btn--bottom"> Tin tức </a></li>
                        <li><span class="text-danger">♦</span><a href="{{ route('contact')}}" class="btn--bottom"> Liên hệ </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="policy-box">
                    <ul>HỖ TRỢ KHÁCH HÀNG
                        <li><span class="text-danger">♦</span><a href="{{!empty($info->shopping_guide) ? asset('uploads/'.$info->shopping_guide) : '#'}}" class="btn--bottom"> Hướng dẫn mua hàng </a></li>
                        <li><span class="text-danger">♦</span><a href="{{!empty($info->payment_guide) ? asset('uploads/'.$info->payment_guide) : '#'}}" class="btn--bottom"> Hướng dẫn thanh toán </a></li>
                        <li><span class="text-danger">♦</span><a href="{{!empty($info->purchase_forms) ? asset('uploads/'.$info->purchase_forms) : '#'}}" class="btn--bottom"> Các hình thức mua hàng </a></li>
                        <li><span class="text-danger">♦</span><a href="{{!empty($info->road_map) ? asset('uploads/'.$info->road_map) : '#'}}" class="btn--bottom"> Sơ đồ đường đi </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="policy-box">
                    <ul>CHÍNH SÁCH BÁN HÀNG
                        <li><span class="text-danger">♦</span><a href="{{!empty($info->delivery_policy) ? asset('uploads/'.$info->delivery_policy) : '#'}}" class="btn--bottom"> Chính sách giao hàng </a></li>
                        <li><span class="text-danger">♦</span><a href="{{!empty($info->exchange_policy) ? asset('uploads/'.$info->exchange_policy) : '#'}}" class="btn--bottom"> Chính sách đổi sản phẩm </a></li>
                        <li><span class="text-danger">♦</span><a href="{{!empty($info->warranty_policy) ? asset('uploads/'.$info->warranty_policy) : '#'}}" class="btn--bottom"> Chính sách bảo hành </a></li>
                        <li><span class="text-danger">♦</span><a href="{{!empty($info->privacy_policy) ? asset('uploads/'.$info->privacy_policy) : '#'}}" class="btn--bottom"> Chính sách bảo mật </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="contact-footer">
    <div class="container-customer">
        <div class="row">
            <div class="col-lg-4">
                <div class="name">
                    <h4>CÔNG TY TNHH NĂNG LƯỢNG MIAN</h4>
                    <p>{{ @$info->slogan}}</p>
                    <p><span>Địa chỉ:</span> Số 20 LK1 khu đô thị Lideco, Thị Trấn Trạm Trôi, Huyện Hoài Đức, Thành phố Hà Nội</p>
                    <p><span>Hotline:</span> {{ $info->phone }} - Zalo: {{ $info->zalo }}</p>
                    <p><span>Email:</span> {{ $info->email }}</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="name">
                    <h4>Văn phòng</h4>
                    @if (count($agencies) > 0)
                        @foreach ($agencies as $item)
                            <p><span>{{ $item->title}}:</span> {{ $item->content}}</p>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="col-lg-4">
                <div class="name">
                    <h4>BẢN ĐỒ</h4>
                    <p>{!! $info->map !!}</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="text-footer">
    <h5>Nhà sản xuất và phân phối quạt công nghiệp lớn nhất tại Việt Nam - MIANENERGY</h5>
</section>
{{-- <script src="https://images.dmca.com/Badges/DMCABadgeHelper.min.js"> </script> --}}
<style type="text/css">
	.footervantheweb-contact{
		    background: unset;
    position: fixed !important;
    left: 18px;
    bottom: 18px;
    z-index: 1;
}
	.footervantheweb-contact ul li {
       list-style: none!important;
       padding: 5px 0;
}
	</style>
<div class="footervantheweb-contact">
	<ul>
		<li class="mobile-vanthe"><a target="_blank" class="btn_popup_vantheweb-mobile" href="tel:{{$info->phone}}"><img src="https://tampanelcachnhiet.com/wp-content/uploads/2020/06/widget_icon_click_to_call.png"></a></li>		
		<li><a target="_blank" class="btn_popup_vantheweb2" href="https://zalo.me/{{ $info->zalo }}"><img src="https://tampanelcachnhiet.com/wp-content/uploads/2020/06/widget_icon_zalo.png"></a></li>
		<li><a target="_blank" class="btn_popup_vantheweb3" href="{{ $info->facebook }}"><img src="https://tampanelcachnhiet.com/wp-content/uploads/2020/06/widget_icon_messenger.png"></a></li>
	</ul>
</div>