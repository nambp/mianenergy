@php
    $lists = \App\Libraries\Ultilities::productNew();
@endphp
@if (count(@$lists) > 0)
    @foreach (@$lists as $item)
        <li class="product-detail">
            <div class="image">
                <img src="{{ asset('uploads/products/'.$item->image) }}" alt="" srcset="">
            </div>
            <div class="blog-des">
                <a href="{{ route('product.detail',$item->slug) }}">{{ $item->title }}</a>
                <p>Giá: Liên hệ</p>
            </div>
        </li>
    @endforeach
@endif

    
                           