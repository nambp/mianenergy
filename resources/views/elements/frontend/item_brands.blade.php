
@php
    $lists = \App\Libraries\Ultilities::brands();
@endphp
{{-- <div class="form-check">
    <input class="form-check-input" name="brand" type="checkbox" value="" id="flexCheckDefault" checked/>
    <label class="form-check-label" for="flexCheckDefault">
        Tất cả
    </label>
</div> --}}
@if (count($lists) > 0)
    @foreach ($lists as $key => $item)
        <div class="form-check">
            <input class="form-check-input" name="brands" type="checkbox" value="{{ $item->id }}" id="flexCheckDefault{{$key}}" />
            <label class="form-check-label" for="flexCheckDefault{{$key}}">
                 {{ $item->title}}
            </label>
        </div>
    @endforeach
@endif
