@if ($url_show != null)
<a href="{{$url_show}}" class="btn btn-primary waves-effect waves-light btn-sm" title="Chi tiết"><i class="fas fa-eye"></i></a>
@endif
@if ($url_edit != null)
<a href="{{$url_edit}}" class="btn btn-info waves-effect waves-light btn-sm" title="Cập nhật"><i class="fas fa-edit"></i></a>
@endif
@if ($url_destroy != null)
<a href="{{$url_destroy}}" class="btn btn-danger waves-effect waves-light btn-sm btn-delete" title="Xóa"><i class="fas fa-trash-alt"></i></a>
@endif
