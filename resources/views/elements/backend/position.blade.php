<p class="mb-0">
    <a href="javascript:void(0)" data-link="{{ $url }}" class="move" data-type="up" data-id="{{ $model->id }}" data-position="{{ $model->position }}"><i class="mdi mdi-arrow-up"></i></a>
    <a href="javascript:void(0)" data-link="{{ $url }}" class="move" data-type="down" data-id="{{ $model->id }}" data-position="{{ $model->position }}"><i class="mdi mdi-arrow-down"></i></a>
</p>