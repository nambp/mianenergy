<label class="switch mb-0" data-url="{{ $url }}">
    <input type="checkbox" {{ ($model->status == 1) ? 'checked' : '' }} data-id="{{ $model->id }}" data-status="{{ ($model->status == 1) ? 2 : 1 }}">
    <span class="slider round"></span>
</label>