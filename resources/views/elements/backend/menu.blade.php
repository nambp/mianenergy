<aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item"> 
                            <a class="sidebar-link waves-effect waves-dark" href="{{ route('categories') }}">
                                <i class="mdi mdi-tune-vertical"></i><span class="hide-menu"> Danh mục</span>
                            </a>
                        </li>
                        <li class="sidebar-item"> 
                            <a class="sidebar-link waves-effect waves-dark" href="{{ route('brands') }}">
                                <i class="mdi mdi-tune-vertical"></i><span class="hide-menu"> Thương hiệu</span>
                            </a>
                        </li>
                        <li class="sidebar-item"> 
                            <a class="sidebar-link hwaves-effect waves-dark" href="{{ route('products') }}">
                                <i class="mdi mdi-widgets"></i><span class="hide-menu">Sản phẩm</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark" href="{{ route('orders') }}">
                                <i class="mdi mdi-credit-card-multiple"></i><span class="hide-menu">Đơn đặt hàng</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark" href="{{ route('rates') }}">
                                <i class="mdi mdi-credit-card-multiple"></i><span class="hide-menu">ý kiến đánh giá</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark" href="{{ route('blogs') }}">
                                <i class="mdi mdi-credit-card-multiple"></i><span class="hide-menu">Blogs</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark" href="{{ route('projects') }}">
                                <i class="mdi mdi-credit-card-multiple"></i><span class="hide-menu">Dự án</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark" href="{{ route('contacts') }}">
                                <i class="mdi mdi-credit-card-multiple"></i><span class="hide-menu">Phản hồi</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark" href="{{ route('sliders') }}">
                                <i class="mdi mdi-credit-card-multiple"></i><span class="hide-menu">Ảnh banner</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark" href="{{ route('introduces') }}">
                                <i class="mdi mdi-credit-card-multiple"></i><span class="hide-menu">Bài viết giới thiệu</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark" href="{{ route('informations') }}">
                                <i class="mdi mdi-credit-card-multiple"></i><span class="hide-menu">Thông tin chung</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark" href="{{ route('agencies') }}">
                                <i class="mdi mdi-credit-card-multiple"></i><span class="hide-menu">Trụ sở, văn phòng</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>