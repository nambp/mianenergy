@extends('layouts.client')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css" integrity="sha512-UTNP5BXLIptsaj5WdKFrkFov94lDx+eBvbKyoe1YAfjeRPC+gT5kyZ10kOHCfNZqEui1sxmqvodNUx3KbuYI/A==" crossorigin="anonymous" />
@endsection
@section('keywords', $info->meta_keywords)
@section('description', $info->meta_description)
@section('og_description', $info->meta_description)
@section('t_title', $info->meta_title)
@section('title_all', $info->meta_title)
@section('t_description', $info->meta_description)
@section('og_secure_url', asset('uploads/'.$info->seo_image))
@section('og_image', asset('uploads/'.$info->seo_image))
@section('title', 'Mian Energy')
@section('content')
    <section class="slider">
        <div class="container-customer customer-slider">
            <div class="slider-content">
                <div class="nav-menu d-lg-none">
                    <ul class="nav">
                        @foreach($categories as $key => $value)
                            @if($value->parent_id == null)
                            @php
                                $category_id = $value->children->pluck('id')->all();
                                $products = \App\Libraries\Ultilities::productCategory($category_id);
                            @endphp
                                @if(count($products) > 0)
                                    <li class="nav-item dropdown dropdown-submenu">
                                        <a class="nav-link nav-cate dropdown-toggle" href="{{ route('category',$value->slug) }}">{{ $value->title }}</a>
                                        <ul class="dropdown-menu menu--sub" >
                                            @foreach($value->children as $key => $item)
                                                <li class="nav-item ">
                                                    <a class="nav-link nav-cate" href="{{ route('category',$item->slug) }}">{{ $item->title }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endif
                            @endif
                        @endforeach
                        
                    </ul>
                    
                </div>
                <div class="slider-image">
                    <div id="carouselExampleControls" class="carousel slide carousel-fade" data-ride="carousel">
                        <div class="carousel-inner">
                          @if (@$slider)
                                @foreach ($slider as $key => $sli)
                                    <div class="carousel-item {{($key == 0) ? 'active' : ''}}">
                                        <img class="d-block w-100 img-fluid" src="{{ asset('uploads/sliders/'.$sli->image)}}" alt="First slide">
                                    </div>
                                @endforeach
                          @endif
                          
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                </div>
            </div>
            <section class="content-support d-lg-none">
                <div class="container-customer support-box">
                    <div class="support-box">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="support-item">
                                    <div class="image">
                                        <img src="{{ asset('images/icon/thanhtoa_636447942490379026.png')}}" alt="" srcset="">
                                    </div>
                                    <div class="content">
                                        <p>Thanh toán</p>
                                        <h3>NHANH NHẤT</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="support-item">
                                    <div class="image">
                                        <img src="{{ asset('images/icon/phanho_636447888736106953.png')}}" alt="" srcset="">
                                    </div>
                                    <div class="content">
                                        <p>Phản hồi khách hàng</p>
                                        <h3>HÀI LÒNG</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="support-item">
                                    <div class="image">
                                        <img src="{{ asset('images/icon/giaohan_636447888552194637.png')}}" alt="" srcset="">
                                    </div>
                                    <div class="content">
                                        <p>Giao hàng</p>
                                        <h3>24/7</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="support-item">
                                    <div class="image">
                                        <img src="{{ asset('images/icon/banbuo_636447961226397820.png')}}" alt="" srcset="">
                                    </div>
                                    <div class="content">
                                        <p>Nhà sản xuất</p>
                                        <h3>CHÍNH HÃNG</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
    <section class="slg-title">
        <div class="container-customer">
            <h1 class="titlt-web">
                <span>Nhà sản xuất và phân phối quạt công nghiệp hàng đầu tại việt nam</span>
            </h1>
        </div>
    </section>
    <section class="new-product">
        <div class="container-customer">
            <div class="product-tab">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="new-product" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Sản phẩm mới</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="hot-product" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Sản phẩm hot</a>
                    </li>
                  </ul>
                  <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="owl-carousel owl-product owl-theme">
                            @if (count($productList) > 0)
                                @foreach ($productList as $product)
                                    <div class="item">
                                        <div class="product">
                                            <div class="image">
                                                <a href="{{ route('product.detail',$product->slug) }}"><img class="img-fluid" src="{{ asset('uploads/products/'.$product->image) }}" alt="" srcset=""></a>
                                            </div>
                                            <div class="title-product">
                                                <a href="{{ route('product.detail',$product->slug) }}">
                                                    {{ $product->title }}
                                                </a>
                                                <p>Giá : Liên hệ</p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                
                            @endif
                        </div>
                    </div>
                  
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="owl-carousel owl-product owl-theme">
                            @if (count($productList) > 0)
                                @foreach ($productList->where('spec',2) as $product)
                                    <div class="item">
                                        <div class="product">
                                            <div class="image">
                                                <a href="{{ route('product.detail',$product->slug) }}"><img class="img-fluid" src="{{ asset('uploads/products/'.$product->image) }}" alt="" srcset=""></a>
                                            </div>
                                            <div class="title-product">
                                                <a href="{{ route('product.detail',$product->slug) }}">
                                                    {{ $product->title }}
                                                </a>
                                                <p>Giá : Liên hệ</p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                
                            @endif
                        </div>
                    </div>
                  </div>
            </div>
        </div>
    </section>
    @foreach ($categories as $item)
        @if($item->parent_id == null)
            @php
                $category_id = $item->children->pluck('id')->all();
                $products = \App\Libraries\Ultilities::productCategory($category_id);
            @endphp
            @if(count($products) > 0)
                <section class="list-product">
                    <div class="container-customer">
                        <div class="product-box">
                            <div class="list-category">
                                <div class="item-category">
                                    <a href="{{ route('category',$item->slug) }}">
                                        {{ $item->title}}
                                    </a>
                                </div>
                            
                                <div class="list-sub-cate d-lg-none">
                                    <ul class="nav nav-category">
                                        @foreach($item->children as $key => $value)
                                            @if($key < 4)
                                                <li class="nav-item"><a class="nav-link" href="{{ route('category',$value->slug) }}">{{ $value->short_title}}</a></li>
                                            @endif
                                        @endforeach
                                        <li class="nav-item"><a class="nav-link" href="{{ route('category',$item->slug) }}" href="#">Xem tất cả >></a></li>
                                    </ul>
                                </div>
                            </div>
                            @php
                                $category_id = $item->children->pluck('id')->all();
                                $products = \App\Libraries\Ultilities::productCategory($category_id);
                            @endphp
                            <div class="product-detail">
                                <div class="banner-product d-lg-none">
                                    <img src="{{ asset('uploads/categories/'.$item->banner) }}" alt="" srcset="">
                                </div>
                                
                                <div class="product">
                                    @if (count($products) > 0)
                                        @foreach ($products as $product)
                                            <div class="item">
                                                <div class="product">
                                                    <div class="image">
                                                        <a href="{{ route('product.detail',$product->slug) }}"><img class="img-fluid" src="{{ asset('uploads/products/'.$product->image) }}" alt="" srcset=""></a>
                                                    </div>
                                                    <div class="title-product">
                                                        <a href="{{ route('product.detail',$product->slug) }}">
                                                            {{ $product->title }}
                                                        </a>
                                                        <p>Giá : Liên hệ</p>
                                                    </div>
                                                </div>
                                            </div>  
                                            
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            @endif
        @endif
    @endforeach
    
@endsection
@push('scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            $('.owl-product').owlCarousel({
                loop:true,
                margin:10,
                responsiveClass:true,
                dots: false ,
                nav: false,
                navText:false,
                autoplay:true,
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                responsive:{
                    0:{
                        items:1,
                        nav:true
                    },
                    600:{
                        items:3,
                        nav:false
                    },
                    1000:{
                        items:3,
                        nav:true,
                        loop:false
                    },
                    1200:{
                        items:5,
                        nav:true,
                        loop:false
                    }
                }
            });
        });
    </script>
@endpush
    