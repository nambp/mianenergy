@extends('layouts.client')
@section('css')
{{-- <link href="{{ asset('xtreme/dist/css/style.min.css') }}" rel="stylesheet"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css" integrity="sha512-UTNP5BXLIptsaj5WdKFrkFov94lDx+eBvbKyoe1YAfjeRPC+gT5kyZ10kOHCfNZqEui1sxmqvodNUx3KbuYI/A==" crossorigin="anonymous" />
@endsection
@section('title', 'Dự án')
@section('keywords', $detail->meta_keywords)
@section('description', $detail->meta_description)
@section('og_description', $detail->meta_description)
@section('t_title', $detail->meta_title)
@section('title_all', $detail->meta_title)
@section('t_description', $detail->meta_description)
@section('og_secure_url', asset('uploads/'.$detail->seo_image))
@section('og_image', asset('uploads/'.$detail->seo_image))
@section('content')
    <section class="breacum">
        <div class="container-customer">
            <div class="row justify-content-center align-items-center">
                <h1 class="mt-3 title-web">CATEGORY ARCHIVES: DỰ ÁN</h1>
            </div>
        </div>
    </section>
    <section class="list-product">
        <div class="container-customer">
            <div class="product-box">
                <div class="list-categories">
                    <div class="list-sub-cate">
                        <div class="container-default container-left">
                            <div class="detail-news">
                                <div class="dt-news__body">
                                    <div class="dt-news__sapo">
                                        <span class="name-news">TIN TỨC</span>
                                        <h2>{{ $detail->title }}</h2>
                                      </div>
                                      <div class="dt-news__content">
                                          {!! $detail->content !!}
                                      </div>
                                </div>
                            </div>                     
                        </div>
                    </div>
                    <div class="item-category">
                        <div class="category">
                            <a href="javascript:void(0)">
                                TIN NỔI BẬT
                            </a>
                        </div>
                        <ul class="list-blogs">
                            @include('elements.frontend.item_news')
                            @endfor
                        </ul>
                        <div class="category mt-5">
                            <a href="javascript:void(0)">
                                SẢN PHẨM
                            </a>
                        </div>
                        <ul class="list-product-new">
                            @include('elements.frontend.item_product')
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
// Hide submenus
$('#body-row .collapse').collapse('hide'); 

// Collapse/Expand icon
$('#collapse-icon').addClass('fa-angle-double-left'); 

// Collapse click
$('[data-toggle=sidebar-colapse]').click(function() {
    SidebarCollapse();
});

function SidebarCollapse () {
    $('.menu-collapsed').toggleClass('d-none');
    $('.sidebar-submenu').toggleClass('d-none');
    $('.submenu-icon').toggleClass('d-none');
    $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');
    
    // Collapse/Expand icon
    $('#collapse-icon').toggleClass('fa-angle-double-left fa-angle-double-right');
}
});
</script>
    
@endpush
    