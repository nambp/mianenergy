@extends('layouts.client')
@section('css')
{{-- <link href="{{ asset('xtreme/dist/css/style.min.css') }}" rel="stylesheet"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css" integrity="sha512-UTNP5BXLIptsaj5WdKFrkFov94lDx+eBvbKyoe1YAfjeRPC+gT5kyZ10kOHCfNZqEui1sxmqvodNUx3KbuYI/A==" crossorigin="anonymous" />
@endsection
@section('keywords', $info->meta_keywords)
@section('description', $info->meta_description)
@section('og_description', $info->meta_description)
@section('t_title', $info->meta_title)
@section('title_all', $info->meta_title)
@section('t_description', $info->meta_description)
@section('og_secure_url', asset('uploads/'.$info->seo_image))
@section('og_image', asset('uploads/'.$info->seo_image))
@section('title', 'Liên hệ')
@section('content')
    <section class="contact">
        <div class="container-customer">
            <div class="row mt-5">
                <div class="col-lg-6">
                    <div class="contact-detail">
                        <h1>CÔNG TY TNHH NĂNG LƯỢNG MIAN</h1>
                        <p>Số ĐKKD: 1014868593 cấp bởi Sở Kế Hoạch và Đầu Tư Hà Nội.</p>
                        <div class="cl">
                            <div class="cl2">
                                @if (count($agencies) > 0)
                                    @foreach ($agencies as $item)
                                        <p><b><i class="wp-svg-home-2 home-2"></i> {{ $item->title}}:</b>{{ $item->content}}</p>
                                        
                                    @endforeach
                                    
                                @endif
                            </div>
                        </div>
                        <div class="cl">
                            <div class="cl2">
                                <p><b><i class="wp-svg-phone phone"></i> Hotline:</b> <span style="color: #ff0000;">{{ $info->phone }}</span> – <b><i class="wp-svg-phone phone"></i> Zalo:</b> <span style="color: #ff0000;">{{ $info->zalo }}</span></p>
                            </div>
                        </div>
                        <div class="cl">
                        <div class="cl2">
                        <p><b><i class="wp-svg-mail-2 mail-2"></i> Email: </b><span style="color: #ff0000;">{{ $info->email }}</span></p>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-contact">
                        <h2>ĐĂNG KÝ TƯ VẤN </h2>
                        <form action="{{ route('send.contact') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Họ tên" id="">
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="number" name="phone" value="{{ old('phone') }}" class="form-control" placeholder="Điện thoại" id="">
                                @if ($errors->has('phone'))
                                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email" id="">
                                @if ($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="form-group"> 
                                <input type="text" name="address" class="form-control" value="{{ old('address')}}" placeholder="Địa chỉ" id="">
                                @if ($errors->has('address'))
                                    <span class="text-danger">{{ $errors->first('address') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <textarea name="message" placeholder="Nội dung tư vấn" id="" class="form-control" cols="30" rows="4">{{ old('message')}}</textarea>
                                @if ($errors->has('message'))
                                    <span class="text-danger">{{ $errors->first('message') }}</span>
                                @endif
                            </div>
                            <input type="submit" value="Gửi đi" class="btn btn-primary">
                        </form>
                    </div>
                </div>
            </div>
            <div class="mt-5">
                <p>{!! $info->map !!}</p>
            </div>
        </div>
    </section>
    
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
// Hide submenus
$('#body-row .collapse').collapse('hide'); 

// Collapse/Expand icon
$('#collapse-icon').addClass('fa-angle-double-left'); 

// Collapse click
$('[data-toggle=sidebar-colapse]').click(function() {
    SidebarCollapse();
});

function SidebarCollapse () {
    $('.menu-collapsed').toggleClass('d-none');
    $('.sidebar-submenu').toggleClass('d-none');
    $('.submenu-icon').toggleClass('d-none');
    $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');
    
    // Collapse/Expand icon
    $('#collapse-icon').toggleClass('fa-angle-double-left fa-angle-double-right');
}
});
</script>
    
@endpush
    