@extends('layouts.client')
@section('css')
{{-- <link href="{{ asset('xtreme/dist/css/style.min.css') }}" rel="stylesheet"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css" integrity="sha512-UTNP5BXLIptsaj5WdKFrkFov94lDx+eBvbKyoe1YAfjeRPC+gT5kyZ10kOHCfNZqEui1sxmqvodNUx3KbuYI/A==" crossorigin="anonymous" />
@endsection
@section('keywords', $introduce->meta_keywords)
@section('description', $introduce->meta_description)
@section('og_description', $introduce->meta_description)
@section('t_title', $introduce->meta_title)
@section('title_all', $introduce->meta_title)
@section('t_description', $introduce->meta_description)
@section('og_secure_url', asset('uploads/'.$introduce->seo_image))
@section('og_image', asset('uploads/'.$introduce->seo_image))
@section('title', 'Giới thiệu')
@section('content')
    
    <section class="list-product">
        <div class="container-customer">
            {!! $introduce->content !!}
        </div>
    </section>
@endsection
    