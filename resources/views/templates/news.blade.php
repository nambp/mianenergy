@extends('layouts.client')
@section('css')
{{-- <link href="{{ asset('xtreme/dist/css/style.min.css') }}" rel="stylesheet"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css" integrity="sha512-UTNP5BXLIptsaj5WdKFrkFov94lDx+eBvbKyoe1YAfjeRPC+gT5kyZ10kOHCfNZqEui1sxmqvodNUx3KbuYI/A==" crossorigin="anonymous" />
@endsection
@section('title', 'Tin tức')
@section('keywords', $info->meta_keywords)
@section('description', $info->meta_description)
@section('og_description', $info->meta_description)
@section('t_title', $info->meta_title)
@section('title_all', $info->meta_title)
@section('t_description', $info->meta_description)
@section('og_secure_url', asset('uploads/'.$info->seo_image))
@section('og_image', asset('uploads/'.$info->seo_image))
@section('content')
    <section class="breacum">
        <div class="container-customer">
            <div class="row justify-content-center align-items-center">
                <h1 class="mt-3 title-web">CATEGORY ARCHIVES: TIN TỨC</h1>
            </div>
        </div>
    </section>
    <section class="list-product">
        <div class="container-customer">
            <div class="product-box">
                <div class="list-categories">
                   
                    <div class="list-sub-cate">
                        <div class="container-default container-left">
                            @foreach ($blogs as $key => $item)
                                <div class="news-item  {{ ($key > 0) ? 'mt-4' : ''}}">
                                    <div class="image">
                                        <a href="{{ route('news.detail',$item->slug) }}"><img src="{{ asset('uploads/blogs/'.$item->image) }}" alt="" srcset=""></a>
                                    </div>
                                    <div class="news-detail">
                                        <a href="{{ route('news.detail',$item->slug) }}">{{ $item->title}}</a>
                                        <p>{{ $item->summary }}</p>
                                    </div>
                                    <div class="calendar">
                                        <span>{{ date('d',strtotime($item->created_at))}}</span>
                                        <p>Th{{ date('n',strtotime($item->created_at))}}</p>
                                    </div>
                                </div>    
                            @endforeach
                        </div>
                        <div class="d-flex justify-content-center mt-2">
                            {{ $blogs->links('vendor.pagination.bootstrap-4')  }}
                        </div>
                        {{-- {!! $blogs->links() !!} --}}
                    </div>
                    
                    <div class="item-category">
                        <div class="category">
                            <a href="javascript:void(0)">
                                TIN NỔI BẬT
                            </a>
                        </div>
                        <ul class="list-blogs">
                            @include('elements.frontend.item_news')
                        </ul>
                        <div class="category mt-5">
                            <a href="javascript:void(0)">
                                SẢN PHẨM
                            </a>
                        </div>
                        <ul class="list-product-new">
                            @include('elements.frontend.item_product')
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
// Hide submenus
$('#body-row .collapse').collapse('hide'); 

// Collapse/Expand icon
$('#collapse-icon').addClass('fa-angle-double-left'); 

// Collapse click
$('[data-toggle=sidebar-colapse]').click(function() {
    SidebarCollapse();
});

function SidebarCollapse () {
    $('.menu-collapsed').toggleClass('d-none');
    $('.sidebar-submenu').toggleClass('d-none');
    $('.submenu-icon').toggleClass('d-none');
    $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');
    
    // Collapse/Expand icon
    $('#collapse-icon').toggleClass('fa-angle-double-left fa-angle-double-right');
}
});
</script>
    
@endpush
    