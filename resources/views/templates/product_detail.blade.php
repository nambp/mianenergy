@extends('layouts.client')
@section('css')
{{-- <link href="{{ asset('xtreme/dist/css/style.min.css') }}" rel="stylesheet"> --}}
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('xtreme/assets/libs/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css" integrity="sha512-UTNP5BXLIptsaj5WdKFrkFov94lDx+eBvbKyoe1YAfjeRPC+gT5kyZ10kOHCfNZqEui1sxmqvodNUx3KbuYI/A==" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css" integrity="sha512-yJHCxhu8pTR7P2UgXFrHvLMniOAL5ET1f5Cj+/dzl+JIlGTh5Cz+IeklcXzMavKvXP8vXqKMQyZjscjf3ZDfGA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
@section('keywords', $detail->meta_keywords)
@section('title_all', $detail->meta_title)
@section('description', $detail->meta_description)
@section('og_title', $detail->meta_title)
@section('og_description', $detail->meta_description)
@section('t_title', $detail->meta_title)
@section('t_description', $detail->meta_description)
@section('og_secure_url', asset('uploads/'.$detail->seo_image))
@section('og_image', asset('uploads/products/'.$detail->seo_image))
@section('title', $detail->title)
@section('content')
    <section class="breacum">
        <div class="container-customer">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-12">
                    <div class="breacum-box">
                        <h1 class="page-title">{{  $detail->title }}</h1>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Trang chủ</a></li>
                                    <li class="breadcrumb-item"><a href="{{ route('category',$detail->category->slug) }}">{{ $detail->category->title }}</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">{{  $detail->title }}</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-lg-6">
                   
                </div> --}}
            </div>
        </div>
    </section>

    <section class="prodct-info">
        <div class="container-customer">
            <div class="detai--product">
                <div class="row w-100 mr-0 ml-0">
                    <div class="col-lg-4" style="border: 1px solid #ededed">
                        
                        <div class="slider-product">
                            <ul id="vertical">
                                <li data-thumb="{{ asset('uploads/products/'.$detail->image) }}" style="max-height:380px" data-src="img/largeImage.jpg">
                                    <img class="img-fluid h-100" src="{{ asset('uploads/products/'.$detail->image) }}"/>
                                </li>
                                @if (count($detail->galleries) > 0)
                                    @foreach($detail->galleries as $key => $item)
                                        <li data-thumb="{{ asset('uploads/galleries/'.$item->image) }}" style="max-height:380px" data-src="img/largeImage{{$key+1}}.jpg">
                                            <img  class="img-fluid h-100" src="{{ asset('uploads/galleries/'.$item->image) }}" />
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8" style="border: 1px solid #ededed">
                        <div class="infomation_product">
                            <div class="infomation">
                                <div class="title">
                                    <h2>{{ $detail->title }}</h2>
                                </div>
                                <div class="advisory">
                                    <div class="box-image mr-2">
                                        <img class="img-fluid" src="https://quatcongnghiepviet.com/wp-content/uploads/2020/05/tuvan.jpg" alt="" srcset="">
                                    </div>
                                    <div class="box-text">
                                        <span>{{ @$info->phone }}</span>
                                        <p>Đặt hàng nhanh (24/24)</p>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                      
                        <div class="parameter">
                            <table width="100%">

                                <tbody>
                                    @if(count($detail->technicals))
                                        @foreach ($detail->technicals as $item)
                                            <tr>
                                                <td><strong>{{ $item->title }}</strong></td>
                                                <td>:</td>
                                                <td>{{ $item->content }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    <tr>
                                        <td><strong class="text-danger">Giá    :               Liên hệ</strong></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="row justify-content-center align-items-center mt-2">
                                <div class="col-lg-6 col-sm-12">
                                    <a href="#" class="btn btn-order" data-toggle="modal" data-target=".bd-example-modal-lg">Tư vấn</a>
                                </div>
                                <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                            <div class="icon-box featured-box icon-box-left text-left">
                                                <div class="icon-box-img">
                                                    <div class="icon">
                                                        <div class="icon-inner">
                                                            <img width="70" height="70" src="https://quatcongnghiepviet.com/wp-content/uploads/2020/12/icon-website_1-400x400.png" class="attachment-medium size-medium" alt="Icon2" loading="lazy">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="icon-box-text last-reset">
                                                    <h3>TƯ VẤN</h3>
                                                    <p>Chúng tôi sẽ gọi lại cho bạn !</p>
                                                </div>
                                            </div>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                          </div>
                                          <div class="modal-body">
                                            <form action="{{ route('add.advisory') }}" method="POST" id="advisory">
                                                @csrf
                                              <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Họ tên</label>
                                                <input type="text" class="form-control" name="name" id="name"  value="{{ old('name')}}">
                                                @if($errors->has('name'))
                                                    <div class="error text-danger" style="font-size: 12px">{{ $errors->first('name') }}</div>
                                                @endif
                                                <span class="text-danger pt-2" id="error_name"></span>
                                              </div>
                                              <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Số điện thoại</label>
                                                <input type="number" class="form-control" id="phone" name="phone"  value="{{ old('phone')}}">
                                                @if($errors->has('phone'))
                                                    <div class="error text-danger" style="font-size: 12px">{{ $errors->first('phone') }}</div>
                                                @endif
                                                <span class="text-danger pt-2" id="error_phone"></span>
                                              </div>
                                              <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Email</label>
                                                <input type="email" class="form-control" id="email" name="email"  value="{{ old('email')}}">
                                                @if($errors->has('email'))
                                                    <div class="error text-danger" style="font-size: 12px">{{ $errors->first('email') }}</div>
                                                @endif
                                                <span class="text-danger pt-2" id="error_email"></span>
                                              </div>
                                              <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Địa chỉ</label>
                                                <input type="text" class="form-control" id="address" name="address" value="{{ old('address')}}">
                                                @if($errors->has('address'))
                                                    <div class="error text-danger" style="font-size: 12px">{{ $errors->first('address') }}</div>
                                                @endif
                                              </div>
                                              <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Số lượng</label>
                                                <input type="number" class="form-control" id="quantity" name="quantity" value="{{ old('quantity')}}">
                                                @if($errors->has('quantity'))
                                                    <div class="error text-danger" style="font-size: 12px">{{ $errors->first('quantity') }}</div>
                                                @endif
                                              </div>
                                              <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Nội dung tư vấn</label>
                                                <textarea type="text" class="form-control" id="product" rows="6" name="message">{{ old('message')}}</textarea>
                                                @if($errors->has('message'))
                                                    <div class="error text-danger" style="font-size: 12px">{{ $errors->first('message') }}</div>
                                                @endif
                                                <input type="hidden" name="product_id" class="form-control" id="product" value="{{$detail->id}}">
                                                <span class="text-danger pt-2" id="error_message"></span>
                                              </div>
                                              
                                            
                                          </div>
                                          <div class="modal-footer">
                                            {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button> --}}
                                            <button type="submit" id="btn_submit" class="btn btn-primary">Tư vấn</button>
                                          </div>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="chinhsach">
                                        <p><i class="_icon fa fa-shield _before"></i> 100% hàng chính hãng giá tốt nhất</p>
                                        <p><i class="_icon fa fa-refresh _before"></i> Chính sách đổi trả sản phẩm trong vòng 07 ngày</p>
                                        <p><i class="_icon fa fa-truck _before"></i> Giao hàng tận nơi, vận chuyển toàn quốc</p>
                                        <p><i class="_icon fa fa-credit-card-alt _before"></i> Thanh toán linh hoạt</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                
            </div>

        </div>
    </section>
    <section class="list-product">
        <div class="container-customer">
            <div class="product-box">
                <div class="list-categories">
                   
                    <div class="list-sub-cate">
                        <div class="container-default container-left">
                            <ul class="nav nav-tabs" id="myTab1" role="tablist">
                                <li class="nav-item">
                                  <a class="nav-link  {{ (\Session::get('rating') == 'fail') ? '' : 'active'}}" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Mô tả</a>
                                </li>
                                {{-- {{-- <li class="nav-item">
                                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
                                </li> --}}
                                <li class="nav-item">
                                  <a class="nav-link {{ (\Session::get('rating') == 'fail') ? 'active' : ''}}" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Đánh giá</a>
                                </li> 
                              </ul>
                              <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade {{ (\Session::get('rating') == 'fail') ? '' : 'show active'}} " id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="p-2">
                                        <div id="ftwp-postcontent"><h2 id="ftoc-heading-1" class="ftwp-heading">{{ $detail->title }}</h2>
                                            {!! $detail->content !!}
                                        </div>
                                    </div>
                                </div>
                                {{-- {{ dd(\Session::get('rating'))}} --}}
                                {{-- <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div> --}}
                                <div class="tab-pane fade {{ (\Session::get('rating') == 'fail') ? 'show active' : ''}}" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                    <div id="review_form" class="col-inner">
                                        <div class="review-form-inner has-border">
                                            <div id="respond" class="comment-respond">
                                                <h3 id="reply-title" class="comment-reply-title mt-3">Hãy là người đầu tiên nhận xét “ {{ $detail->title }} ” <small><a rel="nofollow" id="cancel-comment-reply-link" href="/quat-cong-nghiep-treo-ifan-nb-75-plus/#respond" style="display:none;">Hủy</a></small></h3>
                                                <form action="{{ route('send.rate')}}" method="post" id="commentform" class="comment-form">
                                                    @csrf
                                                    <div class="comment-form-rating">
                                                        <label for="rating">Đánh giá của bạn&nbsp;<span class="required text-danger">*</span></label><br>
                                                        <div class="rate">
                                                            <input type="radio" hidden id="star5" name="rate" value="5" />
                                                            <label for="star5" title="text">5 stars</label>
                                                            <input type="radio" hidden id="star4" name="rate" value="4" />
                                                            <label for="star4" title="text">4 stars</label>
                                                            <input type="radio" hidden id="star3" name="rate" value="3" />
                                                            <label for="star3" title="text">3 stars</label>
                                                            <input type="radio" hidden id="star2" name="rate" value="2" />
                                                            <label for="star2" title="text">2 stars</label>
                                                            <input type="radio" hidden id="star1" name="rate" value="1" />
                                                            <label for="star1" title="text">1 star</label>
                                                        </div>
                                                        
                                                    </div>
                                                    @if($errors->has('rate'))
                                                        <div class="error text-danger mt-5" style="font-size: 12px">{{ $errors->first('rate') }}</div>
                                                    @endif
                                                    <div class="form-group mt-5">
                                                        <input type="hidden" name="product_id" value="{{ $detail->id }}">
                                                        <label for="comment">Nhận xét của bạn&nbsp;<span class="required text-danger">*</span></label>
                                                        <textarea id="comment" class="form-control" name="message" cols="45" rows="8" >{{ old('message')}}</textarea>
                                                        @if($errors->has('message'))
                                                            <div class="error text-danger" style="font-size: 12px">{{ $errors->first('message') }}</div>
                                                        @endif
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="author">Tên&nbsp;<span class="required text-danger">*</span></label>
                                                                <input id="author" class="form-control" name="name" type="text" value="{{ old('name')}}" size="30" >
                                                                @if($errors->has('name'))
                                                                    <div class="error text-danger" style="font-size: 12px">{{ $errors->first('name') }}</div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label for="email">Email&nbsp;<span class="required text-danger">*</span></label>
                                                                <input id="email" name="email" class="form-control" type="email" value="" size="30" ></p>
                                                                @if($errors->has('email'))
                                                                    <div class="error text-danger" style="font-size: 12px">{{ $errors->first('email') }}</div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <input name="submit" type="submit" id="submit" class="submit btn btn-primary" value="Gửi đi">
                                                    </div>
                                                </form>
                                	        </div><!-- #respond -->
                                        </div>
                                    </div>
                                </div>
                              </div>
                                                 
                        </div>
                    </div>
                    <div class="item-category">
                        <div class="category">
                            <a href="javascript:void(0)">
                                DANH MỤC SẢN PHẨM
                            </a>
                        </div>   
                        <ul class="list-group">
                            @include('elements.frontend.item_cate')
                        </ul>
                        <div class="category mt-5">
                            <a href="javascript:void(0)">
                                SẢN PHẨM
                            </a>
                        </div>
                        <ul class="list-product-new">
                            @php
                                $list = \App\Libraries\Ultilities::productNew();
                            @endphp
                            @foreach($list as $key => $item)
                                <li class="product-detail">
                                    <div class="image">
                                        <img src="{{ asset('uploads/products/'.$item->image) }}" alt="" srcset="">
                                    </div>
                                    <div class="blog-des">
                                        <a href="{{ route('product.detail',$item->slug) }}">{{ $item->title }}</a>
                                        <p>Giá: Liên hệ</p>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
    </section>

    <section class="list-product">
        <div class="container-customer">
            @if (count($products))
                <h3 class="font-weight-bold">SẢN PHẨM TƯƠNG TỰ</h3>
                <div class="product w-100 mt-1">
                    @foreach($products as $key => $value)
                        <div class="item" style="padding: 1% 0">
                            <div class="product">
                                <div class="image w-100">
                                    <a href="#"><img class="img-fluid" src="{{ asset('uploads/products/'.$value->image) }}" alt="" srcset=""></a>
                                </div>
                                <div class="title-product">
                                    <a href="{{ route('product.detail',$item->slug) }}">
                                        {{ $value->title}}
                                    </a>
                                    <p>Giá : Liên hệ</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </section>
@endsection
@push('scripts')
<script src="{{ asset('js/splide.min.js') }}"></script>
<script src="{{ asset('xtreme/assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('xtreme/assets/libs/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('xtreme/dist/js/pages/forms/select2/select2.init.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js" integrity="sha512-Gfrxsz93rxFuB7KSYlln3wFqBaXUc1jtt3dGCp+2jTb563qYvnUBM/GP2ZUtRC27STN/zUamFtVFAIsRFoT6/w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(document).ready(function() {
        $('#vertical').lightSlider({
        // gallery:true,
        // item:1,
        // vertical:true,
        // verticalHeight:295,
        // vThumbWidth:50,
        // thumbItem:8,
        // thumbMargin:4,
        // slideMargin:0,
        // enableDrag: false,
        // currentPagerPosition:'left',
        auto:true,
        gallery:true,
        item:1,
        loop:true,
        thumbItem:6,
        slideMargin:0,
        enableDrag: false,
        currentPagerPosition:'left',
       
        });  
    });

</script>
<script>
    $(document).ready(function(){
// Hide submenus
$('#body-row .collapse').collapse('hide'); 

// Collapse/Expand icon
$('#collapse-icon').addClass('fa-angle-double-left'); 

// Collapse click
$('[data-toggle=sidebar-colapse]').click(function() {
    SidebarCollapse();
});

function SidebarCollapse () {
    $('.menu-collapsed').toggleClass('d-none');
    $('.sidebar-submenu').toggleClass('d-none');
    $('.submenu-icon').toggleClass('d-none');
    $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');
    
    // Collapse/Expand icon
    $('#collapse-icon').toggleClass('fa-angle-double-left fa-angle-double-right');
}
});
</script>
<script>
    function convertMsg(msg) {
        msg = msg.toLowerCase();
        msg = msg.charAt(0).toUpperCase() + msg.slice(1);
        return msg;
    }
    
    $('body').on('click', '#btn_submit', function(){
        var fullname = $('#name').val();
        var email = $('#email').val();
        var message = $('#phone').val();
        if(fullname == '' && email == '' && message == ''){
            $('#error_name').html('Trường họ tên không được bỏ trống.');
            $('#error_email').html('Trường email không được bỏ trống.');
            $('#error_phone').html('Trường số điện thoại không được bỏ trống.');
            return false;
        }else{
            $('#error_name').html('');
            $('#error_phone').html('');
            $('#error_email').html('');
        }
        if(fullname == '' && email == ''){
            $('#error_name').html('Trường họ tên không được bỏ trống.');
            $('#error_email').html('Trường email không được bỏ trống.');
            return false;
        }else{
            $('#error_name').html('');
            $('#error_email').html('');
        }
        if(fullname == '' && message == ''){
            $('#error_name').html('Trường họ tên không được bỏ trống.');
            $('#error_phone').html('Trường số điện thoại không được bỏ trống.');
            return false;
        }else{
            $('#error_name').html('');
            $('#error_phone').html('');
        }
        if(email == '' && message == ''){
            $('#error_email').html('Trường email không được bỏ trống.');
            $('#error_phone').html('Trường số điện thoại không được bỏ trống.');
            return false;
        }else{
            $('#error_phone').html('');
            $('#error_email').html('');
        }
        if(fullname == ''){
            $('#error_name').html('Trường họ tên không được bỏ trống.');
            return false;
        }else{
            $('#error_name').html('');
        }

        if(fullname.length >= 150){
            $('#error_name').html('The full name must not be greater than 150 characters.');
            return false;
        }else{
            $('#error_name').html('');
        }
        if(email == ''){
            $('#error_email').html('Trường email không được bỏ trống.');
            return false;
        }else{
            $('#error_email').html('');
        }
        if(email.length >= 100){
            $('#error_email').html('The email must not be greater than 100 characters.');
            return false;
        }else{
            $('#error_email').html('');
        }
        if(message == ''){
            $('#error_phone').html('Trường số điện thoại không được bỏ trống.');
            return false;
        }else{
            $('#error_phone').html('');
        }
        if(message.length >= 300){
            $('#error_phone').html('The message must not be greater than 300 characters.');
            return false;
        }else{
            $('#error_phone').html('');
        }


    });


    </script>
    
@endpush
    