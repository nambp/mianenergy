@extends('layouts.client')
@section('css')
{{-- <link href="{{ asset('xtreme/dist/css/style.min.css') }}" rel="stylesheet"> --}}
<link rel="stylesheet" type="text/css" href="{{ asset('xtreme/assets/libs/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css" integrity="sha512-UTNP5BXLIptsaj5WdKFrkFov94lDx+eBvbKyoe1YAfjeRPC+gT5kyZ10kOHCfNZqEui1sxmqvodNUx3KbuYI/A==" crossorigin="anonymous" />
@endsection
@section('title', 'Danh mục')

@section('keywords', $category->meta_keywords)
@section('title_all', $category->meta_title)
@section('description', $category->meta_description)
@section('og_title', $category->meta_title)
@section('og_description', $category->meta_description)
@section('t_title', $category->meta_title)
@section('t_description', $category->meta_description)
@section('og_secure_url', asset('uploads/categories/'.$category->seo_image))
@section('og_image', asset('uploads/categories/'.$category->seo_image))
@section('content')
    <section class="breacum">
        <div class="container-customer">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-6">
                    <div class="breacum-box">
                        <h1 class="page-title">Quạt treo tường công nghiệp</h1>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Trang chủ</a></li>
                                    @if($category->parent)
                                        <li class="breadcrumb-item"><a href="{{ route('category',$category->parent->slug) }}">{{ $category->parent->title }}</a></li>
                                    @endif
                                    <li class="breadcrumb-item active" aria-current="page">{{ $category->title }}</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="category_id" id="cate_id" value="{{ $category->id}}" />
                <div class="col-lg-6">
                    <div class="result">
                        {{-- <h3>Hiển thị 1–24 của 59 kết quả</h3> --}}
                        <h3></h3>
                        <select name="sort" class="form-control" id="sort">
                            <option value="1">Thứ tự mới nhất</option>
                            <option value="2">Thứ tự cũ nhất</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="list-product">
        <div class="container-customer">
            <div class="product-box">
                <div class="list-categories">
                    <div class="item-category">
                        <div class="category">
                            <a href="javascript:void(0)">
                                DANH MỤC SẢN PHẨM
                            </a>
                        </div>   
                        <ul class="list-group">
                            @include('elements.frontend.item_cate')
                        </ul>
                        <div class="category mt-5">
                            <a href="javascript:void(0)">
                                THƯƠNG HIỆU
                            </a>
                        </div>
                        <div class="filter-brands">
                            @include('elements.frontend.item_brands')
                        </div>
                        <div class="category mt-5">
                            <a href="javascript:void(0)">
                                TIN NỔI BẬT
                            </a>
                        </div>
                        <ul class="list-blogs">
                            @include('elements.frontend.item_news')
                        </ul>
                    </div>
                    <div class="list-sub-cate">
                        <div class="container-default">
                           
                            {!! $category->content !!}
                            <div class="product w-100 mt-5" id="product">
                                @if($category->parent_id == null)
                                    @php
                                        $category_id = $category->children->pluck('id')->all();
                                        
                                        $productList = \App\Libraries\Ultilities::productAll($category_id);
                                    @endphp
                                    @if(count($productList) > 0)
                                        @foreach ($productList as $item)
                                            <div class="item">
                                                <div class="product">
                                                    <div class="image">
                                                        <a href="{{ route('product.detail',$item->slug) }}"><img class="img-fluid" src="{{ asset('uploads/products/'.$item->image) }}" alt="" srcset=""></a>
                                                    </div>
                                                    <div class="title-product">
                                                        <a href="{{ route('product.detail',$item->slug) }}">
                                                            {{ $item->title}}
                                                        </a>
                                                        <p>Giá : Liên hệ</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        Không có sản phẩm nào !
                                    @endif
                                @elseif(count($category->products) > 0)
                                    @php
                                        $productList = \App\Libraries\Ultilities::productAlls($category->id);
                                    @endphp
                                    @foreach ($productList as $item)
                                        <div class="item">
                                            <div class="product">
                                                <div class="image">
                                                    <a href="{{ route('product.detail',$item->slug) }}"><img class="img-fluid" src="{{ asset('uploads/products/'.$item->image) }}" alt="" srcset=""></a>
                                                </div>
                                                <div class="title-product">
                                                    <a href="{{ route('product.detail',$item->slug) }}">
                                                        {{ $item->title}}
                                                    </a>
                                                    <p>Giá : Liên hệ</p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    Không có sản phẩm nào !
                                @endif
                                
                            </div>
                            @if(!empty($productList))
                                <div class="d-flex justify-content-center mt-2">
                                    {{ $productList->links('vendor.pagination.bootstrap-4')  }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
@endsection
@push('scripts')
<script src="{{ asset('xtreme/assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('xtreme/assets/libs/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('xtreme/dist/js/pages/forms/select2/select2.init.js') }}"></script>
<script>
    $(document).ready(function(){
// Hide submenus
$('#body-row .collapse').collapse('hide'); 

// Collapse/Expand icon
$('#collapse-icon').addClass('fa-angle-double-left'); 

// Collapse click
$('[data-toggle=sidebar-colapse]').click(function() {
    SidebarCollapse();
});

function SidebarCollapse () {
    $('.menu-collapsed').toggleClass('d-none');
    $('.sidebar-submenu').toggleClass('d-none');
    $('.submenu-icon').toggleClass('d-none');
    $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');
    
    // Collapse/Expand icon
    $('#collapse-icon').toggleClass('fa-angle-double-left fa-angle-double-right');
}
});
</script>
<script>
    let brand = [];
    let cate_id = $('#cate_id').val();
    $('body').on('change',"input[name='brands']",function(){
        if($(this).is(":checked")) { 
            let value = $(this).val();
            brand.push(value);
        } else {
            let value = $(this).val();
            for( var i = 0; i < brand.length; i++){ 
                if ( brand[i] === value) { 
                    brand.splice(i, 1); 
                }
            }
        }
        let sort = $("#sort").val();
        $.ajax({
            type : "POST",
            data : {"_token": "{{ csrf_token() }}",brand:brand,sort:sort ,cate_id:cate_id},
            url : "{{ route('filter.product') }}",
            success : function(result){
                $("#product").html(result);
            },
            error : function(result){

            }
        })
    });
    $('body').on('change',"#sort",function(){
        let sort = $(this).val();
        $.ajax({
            type : "POST",
            data : {"_token": "{{ csrf_token() }}",brand:brand,sort:sort,cate_id:cate_id},
            url : "{{ route('filter.product') }}",
            success : function(result){
                $("#product").html(result);
            },
            error : function(result){

            }
        })
    })
</script>
    
@endpush
    