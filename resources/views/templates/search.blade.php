@extends('layouts.client')
@section('css')
{{-- <link href="{{ asset('xtreme/dist/css/style.min.css') }}" rel="stylesheet"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css" integrity="sha512-UTNP5BXLIptsaj5WdKFrkFov94lDx+eBvbKyoe1YAfjeRPC+gT5kyZ10kOHCfNZqEui1sxmqvodNUx3KbuYI/A==" crossorigin="anonymous" />
@endsection
@section('title', 'Tìm kiếm')
@section('keywords', $info->meta_keywords)
@section('description', $info->meta_description)
@section('og_description', $info->meta_description)
@section('t_title', $info->meta_title)
@section('title_all', $info->meta_title)
@section('t_description', $info->meta_description)
@section('og_secure_url', asset('uploads/'.$info->seo_image))
@section('og_image', asset('uploads/'.$info->seo_image))
@section('content')
    <section class="breacum">
        <div class="container-customer">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-12">
                    <div class="breacum-box">
                        <h1 class="page-title">Cửa hàng</h1>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Trang chủ</a></li>
                                    <li class="breadcrumb-item"><a href="{{ route('product') }}">Sản phẩm</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">KẾT QUẢ TÌM KIẾM CHO “{{ $search }}”</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-lg-6">
                    <div class="result">
                        <h3>Hiển thị 1–24 của 59 kết quả</h3>
                        <select class="form-control">
                            <option value="">Thứ tự mặc định</option>
                            <option value="">Thứ tự mặc định</option>
                        </select>
                    </div>
                </div> --}}
            </div>
        </div>
    </section>
    <section class="list-product">
        <div class="container-customer">
            <div class="product-box">
                <div class="list-categories">
                    <div class="item-category">
                        <div class="category">
                            <a href="javascript:void(0)">
                                DANH MỤC SẢN PHẨM
                            </a>
                        </div>   
                        <ul class="list-group">
                            @include('elements.frontend.item_cate')
                        </ul>
                        {{-- <div class="category mt-5">
                            <a href="javascript:void(0)">
                                THƯƠNG HIỆU
                            </a>
                        </div>
                        <div class="filter-brands">
                            @include('elements.frontend.item_brands')
                        </div> --}}
                        <div class="category mt-5">
                            <a href="javascript:void(0)">
                                TIN NỔI BẬT
                            </a>
                        </div>
                        <ul class="list-blogs">
                            @include('elements.frontend.item_news')
                        </ul>
                    </div>
                    <div class="list-sub-cate">
                        <div class="container-default">
                            <div class="product w-100" id="product">
                                @if(count($products) > 0)
                                    @foreach($products as $key => $value)
                                        <div class="item">
                                            <div class="product">
                                                <div class="image">
                                                    <a href="{{ route('product.detail',$value->slug) }}"><img class="img-fluid" src="{{ asset('uploads/products/'.$value->image) }}" alt="" srcset=""></a>
                                                </div>
                                                <div class="title-product">
                                                    <a href="{{ route('product.detail',$value->slug) }}">
                                                         {{ $value->title }}
                                                    </a>
                                                    <p>Giá : Liên hệ</p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <p>Không có sản phẩm nào !</p>
                                @endif
                            </div>
                            {{-- <div class="d-flex justify-content-center mt-2">
                                {{ $products->links('vendor.pagination.bootstrap-4')  }}
                            </div> --}}
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
// Hide submenus
$('#body-row .collapse').collapse('hide'); 

// Collapse/Expand icon
$('#collapse-icon').addClass('fa-angle-double-left'); 

// Collapse click
$('[data-toggle=sidebar-colapse]').click(function() {
    SidebarCollapse();
});

function SidebarCollapse () {
    $('.menu-collapsed').toggleClass('d-none');
    $('.sidebar-submenu').toggleClass('d-none');
    $('.submenu-icon').toggleClass('d-none');
    $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');
    
    // Collapse/Expand icon
    $('#collapse-icon').toggleClass('fa-angle-double-left fa-angle-double-right');
}
});
</script>
<script>
    let brand = [];
    $('body').on('change',"input[name='brands']",function(){
        if($(this).is(":checked")) { 
            let value = $(this).val();
            brand.push(value);
        } else {
            let value = $(this).val();
            for( var i = 0; i < brand.length; i++){ 
                if ( brand[i] === value) { 
                    brand.splice(i, 1); 
                }
            }
        }
        let sort = $("#sort").val();
        $.ajax({
            type : "POST",
            data : {"_token": "{{ csrf_token() }}",brand:brand},
            url : "{{ route('filter.product') }}",
            success : function(result){
                $("#product").html(result);
            },
            error : function(result){

            }
        })
    });
    
</script>
    
@endpush
    