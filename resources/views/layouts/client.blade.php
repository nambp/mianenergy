<!DOCTYPE html>
<html lang="en">
    @include('elements.frontend.meta')
    @yield('seo_meta')
    @yield('css')
<body>
    <div class="wrap">
        @include('elements.frontend.menu')
        @yield('content')
        @include('elements.frontend.footer')
        @include('elements.frontend.scripts')
    </div>
</body>
</html>