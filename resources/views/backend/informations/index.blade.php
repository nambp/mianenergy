@extends('layouts.backend')
@section('title', 'Thông tin chung')
@section('bread')
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title">Thông tin chung</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dash') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Thông tin chung</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="heading d-flex">
                <h4 class="mb-3 w-50">Thông tin chung</h4>
              </div>
              <div class="row my-3">
                <div class="col-12">
                  <form action="{{ route('informations.u') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Tên công ty <span class="text-danger">*</span></label>
                          <input type="text" id="name" name="name" value="{{ old('name', !empty($detail) ? $detail->name : '') }}" class="form-control">
                          @if ($errors->has('name'))
                              <span class="text-danger">{{ $errors->first('name') }}</span>
                          @endif
                        </div>
                        <div class="form-group">
                          <label for="">Số điện thoại</label>
                          <input type="text" id="phone" name="phone" value="{{ old('phone', !empty($detail) ? $detail->phone : '') }}" class="form-control">
                          @if ($errors->has('phone'))
                              <span class="text-danger">{{ $errors->first('phone') }}</span>
                          @endif
                        </div>
                        <div class="form-group">
                          <label for="">Email</label>
                          <input type="text" id="email" name="email" value="{{ old('email', !empty($detail) ? $detail->email : '') }}" class="form-control">
                          @if ($errors->has('email'))
                              <span class="text-danger">{{ $errors->first('email') }}</span>
                          @endif
                        </div>
                        
                      </div>
                      <div class="col-lg-6">
                        <label for="image">Logo</label> <br>
                        <input type="file" name="logo" id="logo" accept="image/*">
                        <div class="preview-image">
                          <img src="{{!empty($detail) ? asset('uploads/'.$detail->logo) : '#'}}" alt="" class="img-fluid mt-2" id="preview-logo" width="150">
                        </div>
                        <p class="mb-0 error-logo"></p>
                        @if ($errors->has('logo'))
                            <span class="text-danger pt-2 er-logo">{{ $errors->first('logo') }}</span>
                        @endif
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Facebook</label>
                          <input type="text" id="facebook" name="facebook" value="{{ old('facebook', !empty($detail) ? $detail->facebook : '') }}" class="form-control">
                          @if ($errors->has('facebook'))
                              <span class="text-danger">{{ $errors->first('facebook') }}</span>
                          @endif
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Fanpage ID</label>
                          <input type="text" id="face_id" name="face_id" value="{{ old('face_id', !empty($detail) ? $detail->face_id : '') }}" class="form-control">
                          @if ($errors->has('face_id'))
                              <span class="text-danger">{{ $errors->first('face_id') }}</span>
                          @endif
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Zalo</label>
                          <input type="text" id="zalo" name="zalo" value="{{ old('zalo', !empty($detail) ? $detail->zalo : '') }}" class="form-control">
                          @if ($errors->has('zalo'))
                              <span class="text-danger">{{ $errors->first('zalo') }}</span>
                          @endif
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <label for="image">File tuyển dụng</label> <br>
                        <input type="file" name="recruitment" id="recruitment" accept="image/*">
                        <div class="preview-image mb-2">
                          <a href="{{!empty($detail) ? asset('uploads/'.$detail->recruitment) : '#'}}">Show file</a>
                        </div>
                        <p class="mb-0 error-logo"></p>
                        @if ($errors->has('recruitment'))
                            <span class="text-danger pt-2 er-logo">{{ $errors->first('recruitment') }}</span>
                        @endif
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-6">
                        <label for="image">File HD mua hàng</label> <br>
                        <input type="file" name="shopping_guide" id="shopping_guide" accept="image/*">
                        <div class="preview-image mb-2">
                          <a href="{{!empty($detail) ? asset('uploads/'.$detail->shopping_guide) : '#'}}">Show file</a>
                        </div>
                        <p class="mb-0 error-logo"></p>
                        @if ($errors->has('shopping_guide'))
                            <span class="text-danger pt-2 er-logo">{{ $errors->first('shopping_guide') }}</span>
                        @endif
                      </div>
                      <div class="col-lg-6">
                        <label for="image">File HD thanh toán</label> <br>
                        <input type="file" name="payment_guide" id="payment_guide" accept="image/*">
                        <div class="preview-image mb-2">
                          <a href="{{!empty($detail) ? asset('uploads/'.$detail->payment_guide) : '#'}}">Show file</a>
                        </div>
                        <p class="mb-0 error-logo"></p>
                        @if ($errors->has('payment_guide'))
                            <span class="text-danger pt-2 er-logo">{{ $errors->first('payment_guide') }}</span>
                        @endif
                      </div>
                      <div class="col-lg-6">
                        <label for="image">File các hình thức mua hàng</label> <br>
                        <input type="file" name="purchase_forms" id="purchase_forms" accept="image/*">
                        <div class="preview-image mb-2">
                          <a href="{{!empty($detail) ? asset('uploads/'.$detail->purchase_forms) : '#'}}">Show file</a>
                        </div>
                        <p class="mb-0 error-logo"></p>
                        @if ($errors->has('purchase_forms'))
                            <span class="text-danger pt-2 er-logo">{{ $errors->first('purchase_forms') }}</span>
                        @endif
                      </div>
                      <div class="col-lg-6">
                        <label for="image">File sơ đồ đường đi</label> <br>
                        <input type="file" name="road_map" id="road_map" accept="image/*">
                        <div class="preview-image mb-2">
                          <a href="{{!empty($detail) ? asset('uploads/'.$detail->road_map) : '#'}}">Show file</a>
                        </div>
                        <p class="mb-0 error-logo"></p>
                        @if ($errors->has('road_map'))
                            <span class="text-danger pt-2 er-logo">{{ $errors->first('road_map') }}</span>
                        @endif
                      </div>
                      <div class="col-lg-6">
                        <label for="image">File chính sách giao hàng</label> <br>
                        <input type="file" name="delivery_policy" id="delivery_policy" accept="image/*">
                        <div class="preview-image mb-2">
                          <a href="{{!empty($detail) ? asset('uploads/'.$detail->delivery_policy) : '#'}}">Show file</a>
                        </div>
                        <p class="mb-0 error-logo"></p>
                        @if ($errors->has('delivery_policy'))
                            <span class="text-danger pt-2 er-logo">{{ $errors->first('delivery_policy') }}</span>
                        @endif
                      </div>
                      <div class="col-lg-6">
                        <label for="image">File chính sách đổi sản phẩm</label> <br>
                        <input type="file" name="exchange_policy" id="exchange_policy" accept="image/*">
                        <div class="preview-image mb-2">
                          <a href="{{!empty($detail) ? asset('uploads/'.$detail->exchange_policy) : '#'}}">Show file</a>
                        </div>
                        <p class="mb-0 error-logo"></p>
                        @if ($errors->has('exchange_policy'))
                            <span class="text-danger pt-2 er-logo">{{ $errors->first('exchange_policy') }}</span>
                        @endif
                      </div>
                      <div class="col-lg-6">
                        <label for="image">File chính sách bảo hành</label> <br>
                        <input type="file" name="warranty_policy" id="warranty_policy" accept="image/*">
                        <div class="preview-image mb-2">
                          <a href="{{!empty($detail) ? asset('uploads/'.$detail->warranty_policy) : '#'}}">Show file</a>
                        </div>
                        <p class="mb-0 error-logo"></p>
                        @if ($errors->has('warranty_policy'))
                            <span class="text-danger pt-2 er-logo">{{ $errors->first('warranty_policy') }}</span>
                        @endif
                      </div>
                      <div class="col-lg-6">
                        <label for="image">File chính sách bảo mật</label> <br>
                        <input type="file" name="privacy_policy" id="privacy_policy" accept="image/*">
                        <div class="preview-image mb-2">
                          <a href="{{!empty($detail) ? asset('uploads/'.$detail->privacy_policy) : '#'}}">Show file</a>
                        </div>
                        <p class="mb-0 error-logo"></p>
                        @if ($errors->has('privacy_policy'))
                            <span class="text-danger pt-2 er-logo">{{ $errors->first('privacy_policy') }}</span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="">Fanpage</label>
                      <textarea id="fanpage" name="fanpage" rows="5" class="form-control">{{ old('fanpage', !empty($detail) ? $detail->fanpage : '') }}</textarea>
                      @if ($errors->has('fanpage'))
                          <span class="text-danger">{{ $errors->first('fanpage') }}</span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="">Slogan</label>
                      <textarea id="slogan" name="slogan" rows="5" class="form-control">{{ old('slogan', !empty($detail) ? $detail->slogan : '') }}</textarea>
                      @if ($errors->has('slogan'))
                          <span class="text-danger">{{ $errors->first('slogan') }}</span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="">Map</label>
                      <textarea name="map" id="map" class="form-control" rows="5">{!! old('map', !empty($detail) ? $detail->map : '') !!}</textarea>
                      @if ($errors->has('map'))
                          <span class="text-danger">{{ $errors->first('map') }}</span>
                      @endif
                    </div>
                    <hr>
                    <h5 class="text-info">Thông tin SEO</h5>
                    <hr>
                    <div class="row mt-4">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Tên tab <span class="text-danger">*</span></label>
                          <input type="text" id="meta_title" name="meta_title" value="{{ old('meta_title', !empty($detail) ? $detail->meta_title : '') }}" class="form-control">
                          @if ($errors->has('meta_title'))
                              <span class="text-danger">{{ $errors->first('meta_title') }}</span>
                          @endif
                        </div>
                        <div class="form-group">
                          <label for="">Từ khóa <span class="text-danger">*</span></label>
                          <input type="text" id="meta_keywords" name="meta_keywords" value="{{ old('meta_keywords', !empty($detail) ? $detail->meta_keywords : '') }}" class="form-control">
                          @if ($errors->has('meta_keywords'))
                              <span class="text-danger">{{ $errors->first('meta_keywords') }}</span>
                          @endif
                        </div>
                        <div class="form-group">
                          <label for="">Mô tả SEO <span class="text-danger">*</span></label>
                          <textarea name="meta_description" id="meta_description" rows="5" class="form-control">{{ old('meta_description', !empty($detail) ? $detail->meta_description : '') }}</textarea>
                          @if ($errors->has('meta_description'))
                              <span class="text-danger">{{ $errors->first('meta_description') }}</span>
                          @endif
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <label for="image">Ảnh SEO(1200 x 680)</label> <br>
                        <input type="file" name="image_seo" id="image_seo" accept="image/*">
                        <div class="preview-image">
                          <img src="{{!empty($detail) ? asset('uploads/'.$detail->seo_image) : '#'}}" alt="" class="img-fluid mt-2" id="preview-seo" width="150">
                        </div>
                        <p class="mb-0 error-seo"></p>
                        @if ($errors->has('image_seo'))
                            <span class="text-danger pt-2 er-seo">{{ $errors->first('image_seo') }}</span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group text-center mt-3">
                      <a href="{{ route('categories') }}" class="btn waves-effect waves-light btn-dark mr-2">Quay lại</a>
                      <button type="submit" class="btn btn-success">Lưu thông tin</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
  $(document).ready(function () {
      $('#image_seo').bind('change', function() {
        var flag1 = 1, check1 = 1;
        var picsize = (this.files[0].size);
            var ext = $(this).val().split('.').pop().toLowerCase();
        if (picsize > 0) {
          if ($.inArray(ext, ['png','jpg','jpeg']) == -1){
            $('.error-seo').html('<span class="text-danger">Ảnh chỉ được phép: jpeg, png, jpg.</span>');
            $('.er-seo').hide();
            $("#image_seo").val("");
            check1 = 0;
          } else {
            check1 = 1;
          }
          if (picsize >= 10000000){
            $('.error-seo').html('<span class="text-danger">Ảnh không được lớn hơn 10Mb.</span>');
            $('.er-seo').hide();
            $("#image_seo").val("");
            flag1 = 0;
          }else{
            flag1 = 1;
          }
          if (flag1 == 1 && check1 == 1){
            $('.error-image').html('');
            $('.er-seo').html('');
            var imgControlName = "#preview-seo";
            readURL(this, imgControlName);
          }
        }
      });
      $('#logo').bind('change', function() {
        var flag1 = 1, check1 = 1;
        var picsize = (this.files[0].size);
            var ext = $(this).val().split('.').pop().toLowerCase();
        if (picsize > 0) {
          if ($.inArray(ext, ['png','jpg','jpeg']) == -1){
            $('.error-logo').html('<span class="text-danger">Ảnh chỉ được phép: jpeg, png, jpg.</span>');
            $('.er-logo').hide();
            $("#logo").val("");
            check1 = 0;
          } else {
            check1 = 1;
          }
          if (picsize >= 10000000){
            $('.error-logo').html('<span class="text-danger">Ảnh không được lớn hơn 10Mb.</span>');
            $('.er-logo').hide();
            $("#logo").val("");
            flag1 = 0;
          }else{
            flag1 = 1;
          }
          if (flag1 == 1 && check1 == 1){
            $('.error-logo').html('');
            $('.er-logo').html('');
            var imgControlName = "#preview-logo";
            readURL(this, imgControlName);
          }
        }
      });
  });
</script>
@endpush