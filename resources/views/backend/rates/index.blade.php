@extends('layouts.backend')
@section('title', 'Đánh giá')
@section('bread')
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title">Đánh giá</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dash') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Danh sách</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="heading d-flex">
                <h4 class="mb-3 w-50">Đánh giá</h4>
              </div>
              <div class="row mb-3">
                <div class="col-md-3">
                  <input type="text" name="filter_search" id="filter_search" value="{{ old('filter_search') }}"
                    class="form-control" placeholder="Tìm kiếm ...">
                </div>
                <div class="col-md-3">
                  <select class="form-control private select2" id="product_id" style="width: 100%; height:36px;">
                    <option value="">Sản phẩm</option>
                    @foreach ($products as $item)
                    <option value="{{$item->id}}">{{$item->title}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-2">
                  <select class="form-control private cb" id="status" style="width: 100%; height:36px;">
                    <option value="">Trạng thái</option>
                    <option value="1">Đang mở</option>
                    <option value="2">Đang khóa</option>
                  </select>
                </div>
              </div>
              <div class="table-responsive hide-search-datatable custom-table">
                <table id="datatable" class="table table-striped display table-hover" style="width:100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Họ tên</th>
                      <th>Email</th>
                      <th>Sản phẩm</th>
                      <th>Đánh giá</th>
                      <th>Trạng thái</th>
                      <th></th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@push('scripts')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
  $(function () {
    var table = $('#datatable').DataTable({
        processing: true,
        responsive : true,
        serverSide: true,
        stateSave: true,
        ajax : {
					url: "{{ route('rates') }}",
					data : function(d){
						d.search 		= $('#filter_search').val(),
						d.product_id 	= $('#product_id').val(),
						d.status 	= $('#status').val()
					}
				},
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', width: '6%', orderable: false, searchable: false, class: 'align-middle'},
            {data: 'name', name: 'name', width: '20%', orderable: false, searchable: false, class: 'align-middle'},
            {data: 'email', name: 'email', orderable: false, searchable: false,class: 'align-middle'},
            {data: 'product', name: 'product', orderable: false, searchable: false,class: 'align-middle'},
            {data: 'star', name: 'star', width: '10%', orderable: false, searchable: false,class: 'align-middle'},
            {data: 'status', name: 'status', width: '10%', orderable: false, searchable: false, class: 'align-middle'},
            {data: 'action', name: 'action', width: '8%', orderable: false, searchable: false, class: 'align-middle'},
        ]
    });

    $('#filter_search').on('keyup', function(){
      table.draw();
    });
    $('#product_id, #status').on('change', function(){
      table.draw();
    });
  });

  $(document).ajaxError(function(event, jqxhr, settings, exception) {
    if (exception == 'Unauthorized') {
      // Prompt user if they'd like to be redirected to the login page
      bootbox.confirm("Hết phiên làm việc, bạn cần phải đăng nhập để tiếp tục thao tác?", function(result) {
          if (result) {
              window.location = '/login';
          }
      });
    }
  });

// disable datatables error prompt
$.fn.dataTable.ext.errMode = 'none';
  // Change status
  $('body').on('change', '.switch', function(e) {
		e.preventDefault();
		var me = $(this),
       
			url = me.data('url');
        var id = $(this).find('input').data('id');
        var value = $(this).find('input').data('status');

		$.ajax({
			url: url,
			type: 'GET',
			data: {
				'id': id,
                'value': value
			},
			success: function(response){
				// console.log(response);
				$('#datatable').DataTable().ajax.reload(null, false);
				toastr.options.positionClass = 'toast-bottom-right';
				if (response == 1) {
					toastr.success("Cập nhật trạng thái thành công");
				} else {
          toastr.error("Cập nhật trạng thái thất bại");
        }
			},
			error: function(xhr){
				// console.log(xhr);
				toastr.options.positionClass = 'toast-bottom-right';
				toastr.error("Cập nhật trạng thái thất bại");
			}
		});
  });
  $('body').on('click', '.btn-delete', function(e){
      e.preventDefault();

      var me = $(this),
        url = me.attr('href'),
        // title = me.attr('title'),
        csrf_token = $('meta[name="csrf-token"]').attr('content');

      swal({
          title: '',
          text: 'Bạn có chắc muốn thông tin này không ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#f15e5d',
          cancelButtonColor: '#c1c1c1',
          confirmButtonText: 'Có',
          cancelButtonText: 'Không'
      }).then((result) => {
					if (result.value) {
						$.ajax({
							url: url,
							type: 'POST',
							data: {
								'_method': 'DELETE',
								'_token' : csrf_token,
							},
							success: function(response){
                $('#datatable').DataTable().ajax.reload(null, false);
                swal({
                  type: 'success',
                  title: '',
                  text: 'Xóa thành công!'
                });
							},
							error: function(xhr){
								console.log(xhr);
								swal({
									type: 'warning',
									title: '',
									text: 'Xóa thất bại'
								});
							}
						});
					}
				});
  });
</script>
@endpush