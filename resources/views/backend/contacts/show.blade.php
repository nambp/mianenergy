@extends('layouts.backend')
@section('title', 'Chi tiết phản hồi')
@section('bread')
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title">Chi tiết phản hồi</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dash') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('contacts') }}">Danh sách</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Chi tiết</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="heading d-flex">
                <h4 class="mb-3 w-50">Chi tiết phản hồi</h4>
              </div>
              <div class="row my-3">
                <div class="col-12">
                  @if (!empty($contact))
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Họ tên</label>
                                <div class="py-2 px-2" style="background: #f1f1f1;">{!! $contact->name !!}</div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Số điện thoại</label>
                                <div class="py-2 px-2" style="background: #f1f1f1;">{!! $contact->phone !!}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Email</label>
                            <div class="py-2 px-2" style="background: #f1f1f1;">{!! $contact->email !!}</div>
                        </div>
                        </div>
                        <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Địa chỉ</label>
                            <div class="py-2 px-2" style="background: #f1f1f1;">{!! ($contact->address) ? $contact->address : '-' !!}</div>
                        </div>
                        </div>
                    </div>  
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label for="">Trạng thái</label>
                              <div class="py-2 px-2 text-danger" style="background: #f1f1f1;">{!! $contact->status == 1 ? 'Mới' : 'Đã xem' !!}</div>
                            </div>
                          </div>
                          
                        </div>
                      </div>
                    </div>
                    <div class="form-group mt-3">
                      <label for="">Nội dung</label>
                      <div class="py-3 px-2" style="background: #f1f1f1;">{!! ($contact->message) ? $contact->message : '-' !!}</div>
                    </div>
                    
                  @else
                      <p class="text-center">Không tìm thấy dữ liệu</p>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('.btn-delete').click(function(e) {
            e.preventDefault();

            var me = $(this),
              url = me.attr('href'),
              csrf_token = $('meta[name="csrf-token"]').attr('content');

            swal.fire({
                title: '',
                text: 'Bạn có chắc muốn xóa dữ liệu không?',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có',
                cancelButtonText: 'Không'
            }).then((result) => {
                if (result.value) {
                    $(this).parent().parent().remove();
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                          '_method' : 'DELETE',
                          '_token' : csrf_token,
                        },
                        success: function (data) {
                          toastr.success("Xóa ảnh thành công");
                        },
                        error: function(xhr){
                          toastr.error("Xóa ảnh thất bại");
                        }
                    });
                }
            });
        });
    	});
</script>
@endpush