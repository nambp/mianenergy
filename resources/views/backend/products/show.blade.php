@extends('layouts.backend')
@section('title', 'Chi tiết sản phẩm')
@section('bread')
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title">Chi tiết sản phẩm</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dash') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('products') }}">Danh sách</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Chi tiết</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="heading d-flex">
                <h4 class="mb-3 w-50">Chi tiết sản phẩm</h4>
              </div>
              <div class="row my-3">
                <div class="col-12">
                  @if (!empty($detail))
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Tên sản phẩm</label>
                          <div class="py-2 px-2" style="background: #f1f1f1;">{!! $detail->title !!}</div>
                        </div>
                        <div class="form-group">
                          <label for="">Slug</label>
                          <div class="py-2 px-2" style="background: #f1f1f1;">{!! $detail->slug !!}</div>
                        </div>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label for="">Mã sản phẩm</label>
                              <div class="py-2 px-2" style="background: #f1f1f1;">{!! $detail->code !!}</div>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label for="">Giá</label>
                              <div class="py-2 px-2" style="background: #f1f1f1;">{!! ($detail->price > 0) ? number_format($detail->price).' vnđ' : 'Liên hệ' !!}</div>
                            </div>
                          </div>
                        </div>  
                        <div class="form-group">
                          <label for="image">Ảnh</label> <br>
                          <div class="preview-image">
                            <img src="{{asset('uploads/products/'.$detail->image)}}" alt="" class="img-fluid mt-2" id="preview-banner" width="150">
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="">Danh mục</label>
                          <div class="py-2 px-2" style="background: #f1f1f1;">{!! $detail->category->title !!}</div>
                        </div>
                        <div class="form-group">
                          <label for="">Thương hiệu</label>
                          <div class="py-2 px-2" style="background: #f1f1f1;">{!! $detail->brand->title !!}</div>
                        </div>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label for="">Trạng thái</label>
                              <div class="py-2 px-2 text-danger" style="background: #f1f1f1;">{!! $detail->status == 1 ? 'Đang mở' : 'Đang khóa' !!}</div>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label for="">Loại sản phẩm</label>
                              <div class="py-2 px-2 text-danger" style="background: #f1f1f1;">{!! $detail->spec == 1 ? 'Thông thường' : 'Nổi bật' !!}</div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <div class="d-flex justify-content-between">
                              <label for="" class="w-50 mb-0 align-self-center">Thông số sản phẩm</label>
                              <button type="button" id="add" class="btn btn-sm btn-outline-secondary">Cập nhật</button>
                          </div>
                          <div class="tech">
                            <form action="{{ route('technicals.s')}}" method="POST" id="saveTech">
                              @csrf
                              <input type="hidden" name="prod_id" value="{{ $detail->id}}">
                              @if (!empty($detail->technicals) && count($detail->technicals) > 0)
                                  @foreach ($detail->technicals as $key => $item)
                                      <div class="row">
                                        <div class="col-lg-5">
                                          <div class="form-group">
                                            <label for="">Tiêu đề</label>
                                            <input type="text" name="title[]" id="" class="form-control txt-title" required value="{{old('title.'.$key, $item->title)}}">
                                            <p class="err mb-0 text-danger"></p>
                                          </div>
                                        </div>
                                        <div class="col-lg-6">
                                          <div class="form-group">
                                            <label for="">Nội dung</label>
                                            <input type="text" name="content[]" id="" class="form-control txt-title" required value="{{old('content.'.$key, $item->content)}}">
                                            <p class="err mb-0 text-danger"></p>
                                          </div>
                                        </div>
                                        <div class="col-lg-1 align-self-center">
                                          <i class="fa fa-trash icon-remove text-secondary"></i>
                                        </div>
                                      </div>
                                  @endforeach
                              @else
                                  <div class="row">
                                    <div class="col-lg-5">
                                      <div class="form-group">
                                        <label for="">Tiêu đề</label>
                                        <input type="text" name="title[]" id="" class="form-control txt-title" required>
                                        <p class="err mb-0 text-danger"></p>
                                      </div>
                                    </div>
                                    <div class="col-lg-6">
                                      <div class="form-group">
                                        <label for="">Nội dung</label>
                                        <input type="text" name="content[]" id="" class="form-control txt-title" required>
                                        <p class="err mb-0 text-danger"></p>
                                      </div>
                                    </div>
                                    <div class="col-lg-1 align-self-center">
                                      <i class="fa fa-trash icon-remove text-secondary"></i>
                                    </div>
                                  </div>
                              @endif
                            </form>
                          </div>
                          <div class="add-more">
                            <p class="mb-0"><a href="javascript:void(0)" id="plus"><i class="fa fa-plus"></i> Thêm</a></p>
                          </div>
                        </div>
                        <div class="form-group mt-5">
                          <form id="upload-form" action="{{ route('galleries.s') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="d-flex justify-content-between">
                              <label for="" class="w-50">Thư viện ảnh</label>
                              <label for="image" class="d-block mb-0 cursor-pointer text-info"><i class="fas fa-upload"></i> Tải lên</label>
                            </div>
                            <input type="file" class="d-none" name="album[]" id="image" multiple accept="image/*">
                            <input type="hidden" name="product_id" value="{{$detail->id}}">
                          </form>
                          @if(!empty($detail->galleries) && count($detail->galleries) > 0)
                              <div class="row el-element-overlay mt-3" id="list-album">
                                @foreach($detail->galleries as $value)
                                    <div class="col-lg-3 col-md-6 item">
                                        <div class="card">
                                            <img src="{{ asset('uploads/galleries/'. $value->image) }}" class="img-fluid" alt="Image">
                                            <a title="Xóa ảnh" class="btn btn-danger btn-outline btn-delete" href="{{ route('galleries.r',['id'=>$value->id]) }}"><i class="fas fa-trash-alt"></i></a>
                                        </div>
                                    </div>
                                @endforeach
                              </div>
                          @else
                            <p class="text-center text-info">{{__('Chưa có dữ liệu')}}</p>
                          @endif
                        </div>
                      </div>
                    </div>
                    <div class="form-group mt-3">
                      <label for="">Nội dung</label>
                      <div class="py-3 px-2" style="background: #f1f1f1;">{!! $detail->content !!}</div>
                    </div>
                    <hr>
                    <h5 class="text-info">Thông tin SEO</h5>
                    <hr>
                    <div class="row mt-4">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Tên tab</label>
                          <div class="py-2 px-2" style="background: #f1f1f1;">{!! $detail->meta_title !!}</div>
                        </div>
                        <div class="form-group">
                          <label for="">Từ khóa</label>
                          <div class="py-2 px-2" style="background: #f1f1f1;">{!! $detail->meta_keywords !!}</div>
                        </div>
                        <div class="form-group">
                          <label for="">Mô tả SEO</label>
                          <div class="py-2 px-2" style="background: #f1f1f1;">{!! $detail->meta_description !!}</div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <label for="image">Ảnh SEO</label> <br>
                        <div class="preview-image">
                          <img src="{{ asset('uploads/products/'.$detail->seo_image) }}" alt="" class="img-fluid mt-2" id="preview-seo" width="150">
                        </div>
                      </div>
                    </div>
                  @else
                      <p class="text-center">Không tìm thấy dữ liệu</p>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){

    		function checkImage(filename, filesize) {
            var fileType = filename.split('.').pop().trim().toLowerCase();
            var validImageTypes = ["gif", "jpeg", "png", "jpg"];
            if ($.inArray(fileType, validImageTypes) < 0) {
                alert('Ảnh không định dạng JPG, JPEG, PNG or GIF.');
                return false;
            }
            if (filesize > 10000000) {
                return false;
            }
            return true;
        }

        $('#image').change(function(e) {
            var files = $('#image')[0].files;

            for (var i=0, f; f=files[i]; i++) {
                var filename = f.name;
                var filesize = f.size;
                if (!checkImage(filename, filesize)) {
                    $(this).val('');
                    swal({
                        type: 'warning',
                        title: 'Upload fail!',
                        text: 'Ảnh ' + filename + ' không được vượt quá 10MB.'
                    });
                    return false;
                }
            }

            $('#upload-form').submit();
        });

        $('.btn-delete').click(function(e) {
            e.preventDefault();

            var me = $(this),
              url = me.attr('href'),
              csrf_token = $('meta[name="csrf-token"]').attr('content');

            swal.fire({
                title: '',
                text: 'Bạn có chắc muốn xóa dữ liệu không?',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có',
                cancelButtonText: 'Không'
            }).then((result) => {
                if (result.value) {
                    $(this).parent().parent().remove();
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                          '_method' : 'DELETE',
                          '_token' : csrf_token,
                        },
                        success: function (data) {
                          toastr.success("Xóa ảnh thành công");
                        },
                        error: function(xhr){
                          toastr.error("Xóa ảnh thất bại");
                        }
                    });
                }
            });
        });

        //  Update technical
        var html = '<div class="row"><div class="col-lg-5"><div class="form-group"><label for="">Tiêu đề</label><input type="text" name="title[]" id="" class="form-control txt-title" value="">' 
                    + '<p class="err mb-0 text-danger"></p></div></div><div class="col-lg-6"><div class="form-group"><label for="">Nội dung</label> ' 
                      + '<input type="text" name="content[]" id="" class="form-control txt-title" value=""><p class="err mb-0 text-danger"></p></div> ' 
                      + '</div><div class="col-lg-1 align-self-center"><i class="fa fa-trash icon-remove text-secondary"></i></div></div>';
        $('#plus').click(function () {
          $('#saveTech').append(html);
        })

        $('body').on ('click', '.icon-remove', function () {
          if ($('#saveTech').find('.row').length > 1 ) {
            $(this).parent().parent().remove();
          }
        });

        $('#add').click(function (e) {
          e.preventDefault();
          var input = $('.txt-title').val();
          var flag = true;
          $(".txt-title").each(function() {
              if($(this).val() == "" && $(this).val().length < 1) {
                  $(this).parent().find('.err').html('Nội dung không được bỏ trống')
                  flag = false
              } else {
                  $(this).parent().find('.err').html('')
              }
          });  
          
          if (flag) {
            $('#saveTech').submit();
          }
        })
    	});
</script>
@endpush