@extends('layouts.backend')
@section('title', 'Thêm sản phẩm')
@section('bread')
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title">Thêm sản phẩm</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dash') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('products') }}">Danh sách</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Thêm mới</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="heading d-flex">
                <h4 class="mb-3 w-50">Thêm sản phẩm</h4>
              </div>
              <div class="row my-3">
                <div class="col-12">
                  <form action="{{ route('products.s') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Tiêu đề <span class="text-danger">*</span></label>
                          <input type="text" id="title" name="title" value="{{ old('title') }}" class="form-control" onkeyup="ChangeToSlug();">
                          @if ($errors->has('title'))
                              <span class="text-danger">{{ $errors->first('title') }}</span>
                          @endif
                        </div>
                        <div class="form-group">
                          <label for="">Slug <span class="text-danger">*</span></label>
                          <input type="text" id="slug" name="slug" value="{{ old('slug') }}" class="form-control">
                          @if ($errors->has('slug'))
                              <span class="text-danger">{{ $errors->first('slug') }}</span>
                          @endif
                        </div>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label for="">Mã sản phẩm <span class="text-danger">*</span></label>
                              <input type="text" id="code" name="code" value="{{ old('code') }}" class="form-control">
                              @if ($errors->has('code'))
                                  <span class="text-danger">{{ $errors->first('code') }}</span>
                              @endif
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label for="">Giá</label>
                              <input type="text" id="price" name="price" value="{{ old('price') }}" class="form-control">
                              @if ($errors->has('price'))
                                  <span class="text-danger">{{ $errors->first('price') }}</span>
                              @endif
                            </div>
                          </div>
                        </div>
                        
                      </div>
                      <div class="col-lg-6">
                        <label for="image">Ảnh <span class="text-danger">*</span></label> <br>
                        <input type="file" name="image" id="banner" accept="image/*">
                        <div class="preview-image">
                          <img src="" alt="" class="img-fluid mt-2" id="preview-banner" width="150">
                        </div>
                        <p class="mb-0 error-banner"></p>
                        @if ($errors->has('image'))
                            <span class="text-danger pt-2 er-banner">{{ $errors->first('image') }}</span>
                        @endif
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Danh mục <span class="text-danger">*</span></label>
                          <select name="category_id" id="category_id" class="form-control select2" style="width: 100%; height: 36px;">
                            <option value="">Chọn</option>
                            @foreach ($categories as $item)
                                <option value="{{$item->id}}" {{ old('category_id') == $item->id ? 'selected' : ''}}>{{$item->title}}</option>
                            @endforeach
                          </select>
                          @if ($errors->has('category_id'))
                              <span class="text-danger">{{ $errors->first('category_id') }}</span>
                          @endif
                        </div>
                        
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Thương hiệu <span class="text-danger">*</span></label>
                          <select name="brand_id" id="brand_id" class="form-control select2" style="width: 100%; height: 36px;">
                            <option value="">Chọn</option>
                            @foreach ($brands as $item)
                                <option value="{{$item->id}}" {{ old('brand_id') == $item->id ? 'selected' : ''}}>{{$item->title}}</option>
                            @endforeach
                          </select>
                          @if ($errors->has('brand_id'))
                              <span class="text-danger">{{ $errors->first('brand_id') }}</span>
                          @endif
                        </div>
                        
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Trạng thái</label>
                          <div class="d-flex">
                            <div class="custom-control custom-radio w-25">
                                <input type="radio" id="active" name="status" class="custom-control-input" value="1" {{ (empty(old('status')) || old('status') != 2) ? 'checked' : '' }}>
                                <label class="custom-control-label" for="active">Đang mở</label>
                            </div>
                            <div class="custom-control custom-radio w-25">
                                <input type="radio" id="block" name="status" class="custom-control-input" value="2" {{ old('status') == 2 ? 'checked' : ''}}>
                                <label class="custom-control-label" for="block">Đang khóa</label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Loại sản phẩm</label>
                          <div class="d-flex">
                            <div class="custom-control custom-radio w-25">
                                <input type="radio" id="common" name="spec" class="custom-control-input" value="1" {{ (empty(old('spec')) || old('spec') != 2) ? 'checked' : '' }}>
                                <label class="custom-control-label" for="common">Thông thường</label>
                            </div>
                            <div class="custom-control custom-radio w-25">
                                <input type="radio" id="special" name="spec" class="custom-control-input" value="2" {{ old('spec') == 2 ? 'checked' : ''}}>
                                <label class="custom-control-label" for="special">Nổi bật</label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group mt-3">
                      <label for="">Nội dung</label>
                      <textarea name="content" id="content" class="form-control">{!! old('content') !!}</textarea>
                    </div>
                    <hr>
                    <h5 class="text-info">Thông tin SEO</h5>
                    <hr>
                    <div class="row mt-4">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Tên tab <span class="text-danger">*</span></label>
                          <input type="text" id="meta_title" name="meta_title" value="{{ old('meta_title') }}" class="form-control">
                          @if ($errors->has('meta_title'))
                              <span class="text-danger">{{ $errors->first('meta_title') }}</span>
                          @endif
                        </div>
                        <div class="form-group">
                          <label for="">Từ khóa <span class="text-danger">*</span></label>
                          <input type="text" id="meta_keywords" name="meta_keywords" value="{{ old('meta_keywords') }}" class="form-control">
                          @if ($errors->has('meta_keywords'))
                              <span class="text-danger">{{ $errors->first('meta_keywords') }}</span>
                          @endif
                        </div>
                        <div class="form-group">
                          <label for="">Mô tả SEO <span class="text-danger">*</span></label>
                          <textarea name="meta_description" id="meta_description" rows="5" class="form-control">{{ old('meta_description') }}</textarea>
                          @if ($errors->has('meta_description'))
                              <span class="text-danger">{{ $errors->first('meta_description') }}</span>
                          @endif
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <label for="image">Ảnh SEO <span class="text-danger">*</span> (1200 x 680)</label> <br>
                        <input type="file" name="image_seo" id="image_seo" accept="image/*">
                        <div class="preview-image">
                          <img src="" alt="" class="img-fluid mt-2" id="preview-seo" width="150">
                        </div>
                        <p class="mb-0 error-seo"></p>
                        @if ($errors->has('image_seo'))
                            <span class="text-danger pt-2 er-seo">{{ $errors->first('image_seo') }}</span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group text-center mt-3">
                      <a href="{{ route('products') }}" class="btn waves-effect waves-light btn-dark mr-2">Quay lại</a>
                      <button type="submit" class="btn btn-success">Lưu thông tin</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
  CKEDITOR.replace('content', {
      filebrowserUploadUrl: "{{route('upload.file', ['_token' => csrf_token() ])}}",
      filebrowserUploadMethod: 'form'
  });
  CKEDITOR.config.allowedContent = true;
  CKEDITOR.config.extraAllowedContent = 'div(*)';

  $(document).ready(function () {
      $('#image_seo').bind('change', function() {
        var flag1 = 1, check1 = 1;
        var picsize = (this.files[0].size);
            var ext = $(this).val().split('.').pop().toLowerCase();
        if (picsize > 0) {
          if ($.inArray(ext, ['png','jpg','jpeg']) == -1){
            $('.error-seo').html('<span class="text-danger">Ảnh chỉ được phép: jpeg, png, jpg.</span>');
            $('.er-seo').hide();
            $("#image_seo").val("");
            check1 = 0;
          } else {
            check1 = 1;
          }
          if (picsize >= 10000000){
            $('.error-seo').html('<span class="text-danger">Ảnh không được lớn hơn 10Mb.</span>');
            $('.er-seo').hide();
            $("#image_seo").val("");
            flag1 = 0;
          }else{
            flag1 = 1;
          }
          if (flag1 == 1 && check1 == 1){
            $('.error-image').html('');
            $('.er-seo').html('');
            var imgControlName = "#preview-seo";
            readURL(this, imgControlName);
          }
        }
      });
      $('#banner').bind('change', function() {
        var flag1 = 1, check1 = 1;
        var picsize = (this.files[0].size);
            var ext = $(this).val().split('.').pop().toLowerCase();
        if (picsize > 0) {
          if ($.inArray(ext, ['png','jpg','jpeg']) == -1){
            $('.error-banner').html('<span class="text-danger">Ảnh chỉ được phép: jpeg, png, jpg.</span>');
            $('.er-banner').hide();
            $("#banner").val("");
            check1 = 0;
          } else {
            check1 = 1;
          }
          if (picsize >= 10000000){
            $('.error-banner').html('<span class="text-danger">Ảnh không được lớn hơn 10Mb.</span>');
            $('.er-banner').hide();
            $("#banner").val("");
            flag1 = 0;
          }else{
            flag1 = 1;
          }
          if (flag1 == 1 && check1 == 1){
            $('.error-banner').html('');
            $('.er-banner').html('');
            var imgControlName = "#preview-banner";
            readURL(this, imgControlName);
          }
        }
      });
  });
</script>
@endpush