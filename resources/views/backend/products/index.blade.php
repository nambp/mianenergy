@extends('layouts.backend')
@section('title', 'Sản phẩm')
@section('bread')
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title">Sản phẩm</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dash') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Danh sách</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="heading d-flex">
                <h4 class="mb-3 w-50">Sản phẩm</h4>
                <div class="text-right mb-3 w-50">
                  <a href="{{ route('products.c') }}" class="btn waves-effect waves-light btn-danger btn-add ml-2">
                    <i class="fas fa-plus"></i> Thêm mới
                  </a>
                </div>
              </div>
              <div class="row mb-3">
                <div class="col-md-3">
                  <input type="text" name="filter_search" id="filter_search" value="{{ old('filter_search') }}"
                    class="form-control" placeholder="Search">
                </div>
                <div class="col-md-3">
                  <select class="form-control private select2" id="category_id" style="width: 100%; height:36px;">
                    <option value="">Danh mục</option>
                    @foreach ($categories as $item)
                    <option value="{{$item->id}}">{{$item->title}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-2">
                  <select class="form-control private cb" id="status" style="width: 100%; height:36px;">
                    <option value="">Trạng thái</option>
                    <option value="1">Đang mở</option>
                    <option value="2">Đang khóa</option>
                  </select>
                </div>
                <div class="col-md-2">
                  <select class="form-control private cb" id="special" style="width: 100%; height:36px;">
                    <option value="">Loại</option>
                    <option value="1">Bình thường</option>
                    <option value="2">Nổi bật</option>
                  </select>
                </div>
              </div>
              <div class="table-responsive hide-search-datatable custom-table">
                <table id="datatable" class="table table-striped display table-hover" style="width:100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Mã SP</th>
                      <th>Tiêu đề</th>
                      <th>Danh mục</th>
                      <th>Thương hiệu</th>
                      <th>Trạng thái</th>
                      <th width="14%"></th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@push('scripts')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
  $(function () {
    var table = $('#datatable').DataTable({
        processing: true,
        responsive : true,
        serverSide: true,
        stateSave: true,
        ajax : {
					url: "{{ route('products') }}",
					data : function(d){
						d.search 		= $('#filter_search').val(),
						d.status 	= $('#status').val(),
						d.category_id 	= $('#category_id').val(),
						d.special 	= $('#special').val()
					}
				},
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', width: '6%', orderable: false, searchable: false, class: 'align-middle'},
            {data: 'code', name: 'code', width: '8%', orderable: false, searchable: false, class: 'align-middle'},
            {data: 'title', name: 'title', orderable: false, searchable: false, class: 'align-middle'},
            {data: 'category', name: 'category', orderable: false, searchable: false,class: 'align-middle'},
            {data: 'brand', name: 'brand',orderable: false, searchable: false,class: 'align-middle'},
            {data: 'status', name: 'status', width: '12%', orderable: false, searchable: false, class: 'align-middle'},
            {data: 'action', name: 'action', width: '14%', orderable: false, searchable: false, class: 'align-middle'},
        ]
    });

    $('#filter_search').on('keyup', function(){
      table.draw();
    });
    $('#category_id, #special, #status').on('change', function(){
      table.draw();
    });
  });

  $(document).ajaxError(function(event, jqxhr, settings, exception) {
    if (exception == 'Unauthorized') {
      // Prompt user if they'd like to be redirected to the login page
      bootbox.confirm("Hết phiên làm việc, bạn cần phải đăng nhập để tiếp tục thao tác?", function(result) {
          if (result) {
              window.location = '/login';
          }
      });
    }
  });

// disable datatables error prompt
$.fn.dataTable.ext.errMode = 'none';
  // Change status
  $('body').on('change', '.switch', function(e) {
		e.preventDefault();
		var me = $(this),
			url = me.data('url');
        var id = $(this).find('input').data('id');
        var value = $(this).find('input').data('status');

		$.ajax({
			url: url,
			type: 'GET',
			data: {
				'id': id,
        'value': value
			},
			success: function(response){
				// console.log(response);
				$('#datatable').DataTable().ajax.reload(null, false);
				toastr.options.positionClass = 'toast-bottom-right';
				if (response == 1) {
					toastr.success("Cập nhật trạng thái thành công");
				} else {
          toastr.error("Cập nhật trạng thái thất bại");
        }
			},
			error: function(xhr){
				// console.log(xhr);
				toastr.options.positionClass = 'toast-bottom-right';
				toastr.error("Cập nhật trạng thái thất bại");
			}
		});
  });
  $('body').on('click', '.btn-delete', function(e){
      e.preventDefault();

      var me = $(this),
        url = me.attr('href'),
        // title = me.attr('title'),
        csrf_token = $('meta[name="csrf-token"]').attr('content');

      swal({
          title: '',
          text: 'Bạn có chắc muốn thông tin này không ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#f15e5d',
          cancelButtonColor: '#c1c1c1',
          confirmButtonText: 'Có',
          cancelButtonText: 'Không'
      }).then((result) => {
					if (result.value) {
						$.ajax({
							url: url,
							type: 'POST',
							data: {
								'_method': 'DELETE',
								'_token' : csrf_token,
							},
							success: function(response){
                if (response == 1) {
                  swal({
                    type: 'warning',
                    title: '',
                    text: 'Danh mục đang có thông tin, không thể xóa'
                  });
                } else {
                  $('#datatable').DataTable().ajax.reload(null, false);
                  swal({
                    type: 'success',
                    title: '',
                    text: 'Xóa thành công!'
                  });
                }
							},
							error: function(xhr){
								console.log(xhr);
								swal({
									type: 'warning',
									title: '',
									text: 'Xóa thất bại'
								});
							}
						});
					}
				});
  });
</script>
@endpush