@extends('layouts.backend')
@section('title', 'Chi tiết đơn hàng')
@section('bread')
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title">Chi tiết đơn hàng</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dash') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('orders') }}">Danh sách</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Chi tiết</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="heading d-flex">
                <h4 class="mb-3 w-50">Chi tiết đơn hàng</h4>
              </div>
              <div class="row my-3">
                <div class="col-12">
                  @if (!empty($order))
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Họ tên</label>
                                <div class="py-2 px-2" style="background: #f1f1f1;">{!! $order->name !!}</div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Số điện thoại</label>
                                <div class="py-2 px-2" style="background: #f1f1f1;">{!! $order->phone !!}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Email</label>
                            <div class="py-2 px-2" style="background: #f1f1f1;">{!! $order->email !!}</div>
                        </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Địa chỉ</label>
                                <div class="py-2 px-2" style="background: #f1f1f1;">{!! ($order->address) ? $order->address : '-' !!}</div>
                            </div>
                        </div>
                    </div>  
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label for="">Trạng thái</label>
                              <div class="py-2 px-2 text-danger" style="background: #f1f1f1;">{!! $order->status == 1 ? 'Mới' : 'Đã xem' !!}</div>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Số lượng</label>
                                <div class="py-2 px-2" style="background: #f1f1f1;">{!! ($order->quantity) ? $order->quantity : '-' !!}</div>
                            </div>
                        </div>
                          
                        </div>
                      </div>
                    </div>
                    <div class="form-group mt-3">
                      <label for="">Nội dung</label>
                      <div class="py-3 px-2" style="background: #f1f1f1;">{!! ($order->message) ? $order->message : '-' !!}</div>
                    </div>
                    
                  @else
                      <p class="text-center">Không tìm thấy dữ liệu</p>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@push('scripts')

@endpush