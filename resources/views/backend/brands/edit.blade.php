@extends('layouts.backend')
@section('title', 'Cập nhật thương hiệu')
@section('bread')
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title">Cập nhật thương hiệu</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dash') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('brands') }}">Danh sách</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Cập nhật</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="heading d-flex">
                <h4 class="mb-3 w-50">Cập nhật thương hiệu</h4>
              </div>
              <div class="row my-3">
                <div class="col-12">
                  @if (!empty($detail))
                  <form action="{{ route('brands.u', $detail->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Tiêu đề <span class="text-danger">*</span></label>
                          <input type="text" id="title" name="title" value="{{ old('title', $detail->title) }}" class="form-control" onkeyup="ChangeToSlug();">
                          @if ($errors->has('title'))
                              <span class="text-danger">{{ $errors->first('title') }}</span>
                          @endif
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <label for="image">Ảnh logo <span class="text-danger">*</span></label> <br>
                        <input type="file" name="logo" id="logo" accept="image/*">
                        <div class="preview-image">
                          <img src="{{ asset('uploads/brands/'.$detail->logo) }}" alt="" class="img-fluid mt-2" id="preview-logo" width="150">
                        </div>
                        <p class="mb-0 error-logo"></p>
                        @if ($errors->has('logo'))
                            <span class="text-danger pt-2 er-logo">{{ $errors->first('logo') }}</span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group text-center mt-3">
                      <a href="{{ route('brands') }}" class="btn waves-effect waves-light btn-dark mr-2">Quay lại</a>
                      <button type="submit" class="btn btn-success">Lưu thông tin</button>
                    </div>
                  </form>
                  @else
                      <p class="text-center">{{__('Không tìm thấy dữ liệu')}}</p>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
  $(document).ready(function () {
      $('#logo').bind('change', function() {
        var flag1 = 1, check1 = 1;
        var picsize = (this.files[0].size);
            var ext = $(this).val().split('.').pop().toLowerCase();
        if (picsize > 0) {
          if ($.inArray(ext, ['png','jpg','jpeg']) == -1){
            $('.error-logo').html('<span class="text-danger">Ảnh chỉ được phép: jpeg, png, jpg.</span>');
            $('.er-logo').hide();
            $("#logo").val("");
            check1 = 0;
          } else {
            check1 = 1;
          }
          if (picsize >= 10000000){
            $('.error-logo').html('<span class="text-danger">Ảnh không được lớn hơn 10Mb.</span>');
            $('.er-logo').hide();
            $("#logo").val("");
            flag1 = 0;
          }else{
            flag1 = 1;
          }
          if (flag1 == 1 && check1 == 1){
            $('.error-logo').html('');
            $('.er-logo').html('');
            var imgControlName = "#preview-logo";
            readURL(this, imgControlName);
          }
        }
      });
  });
</script>
@endpush