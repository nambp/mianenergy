@extends('layouts.backend')
@section('title', 'Thương hiệu')
@section('bread')
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title">Thương hiệu</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dash') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Danh sách</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="heading d-flex">
                <h4 class="mb-3 w-50">Thương hiệu</h4>
                <div class="text-right mb-3 w-50">
                  <a href="{{ route('brands.c') }}" class="btn waves-effect waves-light btn-danger btn-add ml-2">
                    <i class="fas fa-plus"></i> Thêm mới
                  </a>
                </div>
              </div>
              <div class="row mb-3">
                <div class="col-md-3">
                  <input type="text" name="filter_search" id="filter_search" value="{{ old('filter_search') }}"
                    class="form-control" placeholder="Tiêu đề">
                </div>
              </div>
              <div class="table-responsive hide-search-datatable custom-table">
                <table id="datatable" class="table table-striped display table-hover" style="width:100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Ảnh</th>
                      <th>Tiêu đề</th>
                      <th width="14%"></th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@push('scripts')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
  $(function () {
    var table = $('#datatable').DataTable({
        processing: true,
        responsive : true,
        serverSide: true,
        stateSave: true,
        ajax : {
					url: "{{ route('brands') }}",
					data : function(d){
						d.search 		= $('#filter_search').val(),
						d.status 	= $('#status').val()
					}
				},
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', width: '6%', orderable: false, searchable: false, class: 'align-middle'},
            {data: 'logo', name: 'logo', width: '25%', orderable: false, searchable: false, class: 'align-middle'},
            {data: 'title', name: 'title', orderable: false, searchable: false,class: 'align-middle'},
            {data: 'action', name: 'action', width: '14%', orderable: false, searchable: false, class: 'align-middle'},
        ]
    });

    $('#filter_search').on('keyup', function(){
      table.draw();
    });
  });

  $(document).ajaxError(function(event, jqxhr, settings, exception) {
    if (exception == 'Unauthorized') {
      // Prompt user if they'd like to be redirected to the login page
      bootbox.confirm("Hết phiên làm việc, bạn cần phải đăng nhập để tiếp tục thao tác?", function(result) {
          if (result) {
              window.location = '/login';
          }
      });
    }
  });

// disable datatables error prompt
$.fn.dataTable.ext.errMode = 'none';
 
  $('body').on('click', '.btn-delete', function(e){
      e.preventDefault();

      var me = $(this),
        url = me.attr('href'),
        // title = me.attr('title'),
        csrf_token = $('meta[name="csrf-token"]').attr('content');

      swal({
          title: '',
          text: 'Bạn có chắc muốn thông tin này không ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#f15e5d',
          cancelButtonColor: '#c1c1c1',
          confirmButtonText: 'Có',
          cancelButtonText: 'Không'
      }).then((result) => {
					if (result.value) {
						$.ajax({
							url: url,
							type: 'POST',
							data: {
								'_method': 'DELETE',
								'_token' : csrf_token,
							},
							success: function(response){
                if (response == 1) {
                  swal({
                    type: 'warning',
                    title: '',
                    text: 'Danh mục đang có thông tin, không thể xóa'
                  });
                } else {
                  $('#datatable').DataTable().ajax.reload(null, false);
                  swal({
                    type: 'success',
                    title: '',
                    text: 'Xóa thành công!'
                  });
                }
							},
							error: function(xhr){
								console.log(xhr);
								swal({
									type: 'warning',
									title: '',
									text: 'Xóa thất bại'
								});
							}
						});
					}
				});
  });
 
</script>
@endpush