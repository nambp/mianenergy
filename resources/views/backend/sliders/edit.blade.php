@extends('layouts.backend')
@section('title', 'Cập nhật slider')
@section('bread')
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title">Cập nhật slider</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dash') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('sliders') }}">Danh sách</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Cập nhật</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="heading d-flex">
                <h4 class="mb-3 w-50">Cập nhật slider</h4>
              </div>
              <div class="row my-3">
                <div class="col-12">
                  @if (!empty($detail))                  
                  <form action="{{ route('sliders.u', $detail->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                      <div class="col-lg-6">
                        <label for="image">Ảnh banner <span class="text-danger">*</span> (950 x 500)</label> <br>
                        <input type="file" name="image" id="banner" accept="image/*">
                        <div class="preview-image">
                          <img src="{{ asset('uploads/sliders/'.$detail->image) }}" alt="" class="img-fluid mt-2" id="preview-banner" width="150">
                        </div>
                        <p class="mb-0 error-banner"></p>
                        @if ($errors->has('image'))
                            <span class="text-danger pt-2 er-banner">{{ $errors->first('image') }}</span>
                        @endif
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="">Trạng thái</label>
                          <div class="d-flex">
                            <div class="custom-control custom-radio w-25">
                                <input type="radio" id="active" name="status" class="custom-control-input" value="1" {{ (empty(old('status')) || old('status') != 2) ? 'checked' : ($detail->status == 1 ? 'checked' : '') }}>
                                <label class="custom-control-label" for="active">Đang mở</label>
                            </div>
                            <div class="custom-control custom-radio w-25">
                                <input type="radio" id="block" name="status" class="custom-control-input" value="2" {{ old('status') == 2 ? 'checked' : ($detail->status == 2 ? 'checked' : '')}}>
                                <label class="custom-control-label" for="block">Đang khóa</label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group text-center mt-3">
                      <a href="{{ route('sliders') }}" class="btn waves-effect waves-light btn-dark mr-2">Quay lại</a>
                      <button type="submit" class="btn btn-success">Lưu thông tin</button>
                    </div>
                  </form>
                  @else
                    <p class="text-center">Không tìm thấy dữ liệu</p>                      
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">

  $(document).ready(function () {
      $('#banner').bind('change', function() {
        var flag1 = 1, check1 = 1;
        var picsize = (this.files[0].size);
            var ext = $(this).val().split('.').pop().toLowerCase();
        if (picsize > 0) {
          if ($.inArray(ext, ['png','jpg','jpeg']) == -1){
            $('.error-banner').html('<span class="text-danger">Ảnh chỉ được phép: jpeg, png, jpg.</span>');
            $('.er-banner').hide();
            $("#banner").val("");
            check1 = 0;
          } else {
            check1 = 1;
          }
          if (picsize >= 10000000){
            $('.error-banner').html('<span class="text-danger">Ảnh không được lớn hơn 10Mb.</span>');
            $('.er-banner').hide();
            $("#banner").val("");
            flag1 = 0;
          }else{
            flag1 = 1;
          }
          if (flag1 == 1 && check1 == 1){
            $('.error-banner').html('');
            $('.er-banner').html('');
            var imgControlName = "#preview-banner";
            readURL(this, imgControlName);
          }
        }
      });
  });
</script>
@endpush